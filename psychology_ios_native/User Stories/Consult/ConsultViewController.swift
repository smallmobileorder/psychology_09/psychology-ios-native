//
//  ConsultViewController.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 23.02.2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit
import MessageUI

class ConsultViewController: UIViewController, Routable, MFMailComposeViewControllerDelegate, UITextFieldDelegate {
    
    var calendarCoordinator: (Coordinatable & CalendarCoordinatorOutput)?
    
    func setRootModule(_ module: Presentable?, hideBar: Bool) {
        
    }
    
    func dismissModule(animated: Bool, completion: Callback?) {
        
    }
    
    func popModule(animated: Bool) {
        
    }
    
    func pushReplacing(_ module: Presentable?, animated: Bool, completion: Callback?) {
        
    }
    
    func push(_ module: Presentable?, animated: Bool, completion: Callback?) {
        
    }
    
    func present(_ module: Presentable?, animated: Bool) {
        present(module!.toPresent!, animated: animated, completion: nil)
    }
    
    
    var rootModule: Presentable?
    
    var modules: [Presentable]?
    
    func popToRootModule(animated: Bool) {
        
    }
    
    func popToModule(_ module: Presentable?, animated: Bool) {
        
    }
    
    var onDateSelect: Callback?
    
    enum Format: String, CaseIterable {
        case ochno = "Очно в Санкт-Петербурге"
        case skype = "Скайп"
    }
    
    @IBOutlet private var nameTextField: UITextField!
    @IBOutlet private var contactTextField: UITextField!
    @IBOutlet private var questionTextField: UITextField!
    @IBOutlet private var dateTextField: UITextField!
    @IBOutlet private var contactButton: UIButton!
    @IBOutlet private var sendButton: UIButton!
    
    private var format: Format = .ochno {
        didSet {
            contactButton.setTitle(format.rawValue, for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for tf in [nameTextField, contactTextField, questionTextField, dateTextField] {
            tf?.addTarget(self, action: #selector(updateButton), for: .editingChanged)
            tf?.delegate = self
            tf?.returnKeyType = tf !== dateTextField ? .continue : .done
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField === dateTextField {
            textField.resignFirstResponder()
            return true
        } else {
            let index = [nameTextField, contactTextField, questionTextField, dateTextField].firstIndex(of: textField)!
            [nameTextField, contactTextField, questionTextField, dateTextField][index + 1]?.becomeFirstResponder()
            return false
        }
    }
    
    @objc private func updateButton() {
        sendButton.isEnabled =
            !(nameTextField.text?.isEmpty ?? true)
            &&
            !(contactTextField.text?.isEmpty ?? true)
            &&
            !(questionTextField.text?.isEmpty ?? true)
            &&
            !(dateTextField.text?.isEmpty ?? true)
    }
    
    @IBAction private func selectFormat() {
        let alert = UIAlertController(title: "Выберите формат консультации", message: nil, preferredStyle: .actionSheet)
        Format.allCases.forEach { format in
            alert.addAction(
                UIAlertAction(title: format.rawValue, style: .default) { [weak self] _ in
                    self?.format = format
                }
            )
        }
        present(alert, animated: true)
    }
    
    @IBAction private func send() {
        guard MFMailComposeViewController.canSendMail() else {
            showAlert(.error("Почтовый клиент не настроен"), onCompletion: {})
            return
        }
        
        let mailComposer = MFMailComposeViewController()
        mailComposer.setToRecipients(["psychotherapevt@yandex.ru"])
        mailComposer.setSubject("Запись на консультацию")
        mailComposer.setMessageBody(
"""
Как к вам обращаться: \(nameTextField.text ?? "")
Как связаться: \(contactTextField.text ?? "")
Вопрос: \(questionTextField.text ?? "")
Удобное время для консультации: \(dateTextField.text ?? "")
Формат консультации: \(format.rawValue)
""", isHTML: false)
        mailComposer.mailComposeDelegate = self
        present(mailComposer, animated: true)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }

}
