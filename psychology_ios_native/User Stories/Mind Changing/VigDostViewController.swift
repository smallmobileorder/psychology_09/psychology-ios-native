//
//  VigDostViewController.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 09.02.2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

var currentThought: String?

class VigDostViewController: UIViewController {
    
    var isVig = true
    var record: Record!
    
    var onContinue: Callback?
    var onAnother: Callback?
    
    private var currentPosition: Int?
    private var enabledEmotions: [EmotionType: Int] = [:]
    
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var sliderSectionView: UIView!
    @IBOutlet private var buttons: [UIButton]!
    @IBOutlet private var selectViews: [UIImageView]!
    @IBOutlet private var percentageLabels: [UILabel]!
    @IBOutlet private var emotionNameLabel: UILabel!
    @IBOutlet private var slider: UISlider!
    @IBOutlet private var emotionPercentageLabel: UILabel!
    @IBOutlet private var sliderContainer: UIView!
    
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var sitLabel: UILabel!
    @IBOutlet private var thLabel: UILabel!
    @IBOutlet private var thoughtLabel: UILabel!
    @IBOutlet private var thoughtAnswerTextView: UITextView!
    @IBOutlet private var sovetLabel: UILabel!
    @IBOutlet private var continueButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        view.setDismissKeyboardOnTap()
        
        setup()
        
        if isVig {
            thLabel.text = record.thought
        } else {
            thLabel.text = currentThought
        }
        
        sitLabel.text = record.situatuon
        
        thoughtAnswerTextView.delegate = self
        
        titleLabel.text = isVig ? "Выгодность" : "Достоверность"
        thoughtLabel.text = isVig ? "Мысль над которой работаем:" : "Новая мысль"
        sovetLabel.text = isVig ? "Проверьте выгодность этой мысли. Помогает ли она вам достигать своей цели? Помогает ли она вам стать спокойнее? Если мысль не выгодна, попробуйте придумать новую мысль.\nНапример, вместо мысли «я никогда не справлюсь с этой статьей», гораздо выгоднее будет подумать «Мне предстоит собрать ещё немного материала и дописать еще 10 абзацев. Я справлюсь»" : "Проверьте достоверность этой мысли. Пересмотрите факты. Насколько эта мысль доказуема, точна? Верны ли выводы, которые вы сделали? Если мысль недостоверна, попробуйте придумать новую мысль.\nНапример, вместо мысли «он вечно опаздывает», вернее будет записать «сегодня он пришел на 10 минут позднее обещанного времени. Он порой опаздывал и раньше»."
    }
    
    @IBAction private func `continue`() {
        currentThought = thoughtAnswerTextView.text
        onContinue?()
    }
    
    @IBAction private func another() {
        onAnother?()
    }

}

private extension VigDostViewController {
    
    func setup() {
        for ((button, label), emotionType) in zip(zip(buttons, percentageLabels), EmotionType.allCases) {
            let value = enabledEmotions[emotionType]
            button.setImage(UIImage(named: emotionType.imageName(enabled: value != nil)), for: .normal)
            label.isHidden = value == nil
            label.text = value?.description.appending("%")
        }
        
        for index in 0 ... 7 {
            buttons[index].addTarget(self, action: #selector(setEmotion(_:)), for: .touchUpInside)
        }
        
        sliderSectionView.isHidden = true
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissEmotionPicker))
        gestureRecognizer.delegate = self
        scrollView.addGestureRecognizer(gestureRecognizer)
        scrollView.bounces = false
        
        updateContinueButton()
    }
    
    @IBAction func updateCurrentValue(_ sender: Any) {
        guard let currentPosition = currentPosition else { return }
        
        let value = Int(slider.value)
        let percentFormattedValue = value.description.appending("%")
        emotionPercentageLabel.text = percentFormattedValue
        percentageLabels[currentPosition].text = percentFormattedValue
        
        let emotion = EmotionType.allCases[currentPosition]
        
        enabledEmotions[emotion] = value
    }
    
    @objc func setEmotion(_ sender: UIButton) {
        defer { updateContinueButton() }
        
        guard let position = buttons.firstIndex(of: sender) else { return }
        
        if let currentPosition = currentPosition {
            percentageLabels[currentPosition].isHidden = false
            selectViews[currentPosition].isHidden = true
        }
        
        let selectedEmotion = EmotionType.allCases[position]
        
        let percentageLabel = percentageLabels[position]
        let selectView = selectViews[position]
        
        if let _ = enabledEmotions.removeValue(forKey: selectedEmotion) {
            sender.setImage(UIImage(named: selectedEmotion.imageName(enabled: false)), for: .normal)
            percentageLabel.isHidden = true
            selectView.isHidden = true
            sliderSectionView.isHidden = true
            
            currentPosition = nil
            
            return
        } else if enabledEmotions.count > 0 {
            return
        }
        
        sender.setImage(UIImage(named: selectedEmotion.imageName(enabled: true)), for: .normal)
        percentageLabel.isHidden = true
        selectView.isHidden = false
        
        sliderSectionView.isHidden = false
        slider.value = 1
        emotionPercentageLabel.text = "1%"
        percentageLabel.text = "1%"
        emotionNameLabel.text = selectedEmotion.name
        
        enabledEmotions[selectedEmotion] = 1
        
        currentPosition = position
    }
    
    @objc func dismissEmotionPicker(tap: UITapGestureRecognizer) {
        guard !slider.bounds.contains(tap.location(in: slider)) else { return }
        
        sliderSectionView.isHidden = true
        
        if let currentPosition = currentPosition {
            percentageLabels[currentPosition].isHidden = false
            selectViews[currentPosition].isHidden = true
        }
    }
    
    func updateContinueButton() {
        continueButton.isEnabled = !enabledEmotions.isEmpty && !thoughtAnswerTextView.text.isEmpty
    }
    
}

extension VigDostViewController: UIGestureRecognizerDelegate, UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        updateContinueButton()
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}
