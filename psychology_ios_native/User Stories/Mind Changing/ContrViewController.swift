//
//  ContrViewController.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 02/04/2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

class ContrViewController: UIViewController {
    
    @IBOutlet private var textField: UITextField!
    @IBOutlet private var slider: UISlider!
    @IBOutlet private var percentLabel: UILabel!
    @IBOutlet private var addButton: UIButton!
    
    var onNext: ObjectCallback<Contr>?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        textField.addTarget(self, action: #selector(didChangeText), for: .editingChanged)
        view.setDismissKeyboardOnTap()
        
        addNavigationBackButton(selector: #selector(close))
    }
    
    @objc func close() {
        if textField.text?.isEmpty ?? true && slider.value == 0 {
            navigationController?.popViewController(animated: true)
            return
        }
        
        showAlertСontroller(
            title: "Вы действительно хотите выйти? Введённые данные не будут сохранены",
            actions: [
                (title: "Отмена", handler: {}),
                (title: "Да", handler: { [weak self] in self?.navigationController?.popViewController(animated: true) })
            ]
        )
    }
    
    @objc func didChangeText() {
        addButton.isEnabled = !(textField.text?.isEmpty ?? true)
    }
    
    @IBAction private func setSlide(_ slider: UISlider) {
        percentLabel.text = "\(Int(slider.value))%"
    }
    
    @IBAction private func next() {
        onNext?(Contr(rec: textField.text ?? "", percent: Int(slider.value)))
    }

}
