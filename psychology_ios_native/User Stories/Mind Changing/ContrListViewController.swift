//
//  ContrListViewController.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 29/03/2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit
import RealmSwift

struct Contr {
    let rec: String
    let percent: Int
}

class ContrListViewController: UIViewController {
    
    @IBOutlet private var thoughtLabel: UILabel!
    @IBOutlet private var thoughtPercentLabel: UILabel!
    @IBOutlet private var slider: UISlider!
    @IBOutlet private var tableView: UITableView!
    @IBOutlet private var anotherButton: UIButton!
    
    var thought: ThoughtRealm!
    
    var onAdd: Callback?
    var onMind: Callback?
    var onAnother: Callback?
    
    var contrs: [ContrRealm] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        thoughtLabel.text = thought.thought
        thoughtPercentLabel.text = "\(thought.percent)%"
        slider.value = Float(thought.percent)
        contrs = thought.contrs.map { $0 }

        tableView.registerNib(cell: ContrTableViewCell.self)
        tableView.dataSource = self
        
        anotherButton.isHidden = onAnother == nil
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        let realm = try! Realm()
        try! realm.write {
            thought.percent = Int(slider.value)
        }
    }
    
    @IBAction private func changedSlider() {
        thoughtPercentLabel.text = "\(Int(slider.value))%"
    }
    
    @IBAction private func addMore() {
        onAdd?()
    }
    
    @IBAction private func goToMid() {
        onMind?()
    }
    
    @IBAction private func chooseAnother() {
        onAnother?()
    }
    
    func add(contr: ContrRealm) {
        contrs.append(contr)
        tableView.reloadData()
        
        let realm = try! Realm()
        try! realm.write {
            realm.add(thought)
            thought.contrs.append(contr)
        }
    }
    
    func remove(contr: ContrRealm) {
        guard let index = contrs.firstIndex(of: contr) else { return }
        contrs.remove(at: index)
        tableView.reloadData()
    }

}

extension ContrListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        contrs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(ContrTableViewCell.self)!
        
        cell.set(contr: contrs[indexPath.row])
        
        return cell
    }
    
}
