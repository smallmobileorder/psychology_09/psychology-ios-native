//
//  DzViewController.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 22.02.2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit
import RealmSwift

enum Dz {
    case катастрофизация1
    case катастрофизация2
    case эмоцобоснов
    case чбмышление
    case обесцениваниепозитивного
    case минмакс
    case сверхобобщение
    case чтениемыслей1
    case чтениемыслей2
    case навешиваниеярлыков
    case персонализация
    case туннельноемышление
    case долженствование1
    case долженствование2
    case долженствование3
    
    var title: String {
        switch self {
            case .катастрофизация1, .катастрофизация2: return "Катастрофизация"
            case .эмоцобоснов: return "Эмоциональное обоснование"
            case .чбмышление: return "Черно-белое мышление"
            case .обесцениваниепозитивного: return "Обесценивание позитивного"
            case .минмакс: return "Максимализм / минимализм"
            case .сверхобобщение: return "Сверхобобщение"
            case .чтениемыслей1, .чтениемыслей2: return "Чтение мыслей"
            case .навешиваниеярлыков: return "Навешивание ярлыков"
            case .персонализация: return "Персонализация"
            case .туннельноемышление: return "Туннельное мышление"
            case .долженствование1, .долженствование2, .долженствование3: return "Долженствование"
        }
    }
    
    var next: Dz? {
        switch self {
            case .катастрофизация1: return .катастрофизация2
            case .чтениемыслей1: return .чтениемыслей2
            case .долженствование1: return .долженствование2
            case .долженствование2: return .долженствование3
            default: return nil
        }
    }
}

class DzViewController: UIViewController, UIGestureRecognizerDelegate {

    var onDone: Callback?
    var onNext: OptionalObjectCallback<CatastrhHomeWork>?
    var onAnother: Callback?
    
    var dz: Dz?
    var catastrhHomeWork: CatastrhHomeWork?
    
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var rootStackView: UIStackView!
    @IBOutlet private var doneButton: BrandButton!
    @IBOutlet private var button: BrandButton!
    @IBOutlet private var anotherButton: BrandButton!
    private var worstSlider: SliderView!
    private var bestSlider: SliderView!
    private var emoview: EmoView?
    private var textfields: [UITextField] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let dz = dz else { return }
        
        titleLabel.text = "Домашнее задание - \(dz.title)"
        
        let storedDz = (try! Realm()).objects(CatastrhHomeWork.self).first
        
        switch dz {
            case .катастрофизация1:
                createLabel(text: "Привыкайте к тому, что жизнь не дает гарантий. Придется положиться на закон вероятности. Прогнозируя какую-либо ситуацию, ответьте на следующие вопросы:")
                createTextField(title: "Какова вероятность, что ситуация завершится хорошо?", text: storedDz?.goodResult ?? "", placeholder: "Хороший исход")
                createTextField(title: "Что подумало или сделало бы подавляющее большинство людей в этой ситуации?", text: storedDz?.peopleThoughts ?? "", placeholder: "Мысли людей")
                createTextField(title: "Какой прогноз большинство людей сочли бы благоразумным?", text: storedDz?.reasonableResult ?? "", placeholder: "Благоразумный прогноз")
                createLabel(text: "И помните, то, что ещё не произошло, – лишь плод вашего воображения.")
            case .катастрофизация2:
                createTextField(title: "Выберите ситуацию. Опишите наихудший ожидаемый ущерб (негативные последствия) и отметьте его на шкале.", text: storedDz?.worstHarm ?? "", placeholder: "Наихудший ожидаемый ущерб")
                worstSlider = createSlider(text: "")
                worstSlider.slider.value = Float(storedDz?.worstHarmPercent ?? 1)
                worstSlider.slider.sendActions(for: .valueChanged)
                createTextField(title: "Опишите наилучший ожидаемый ущерб и отметьте его на шкале.", text: storedDz?.bestHarm ?? "", placeholder: "Наилучший ожидаемый ущерб")
                bestSlider = createSlider(text: "")
                bestSlider.slider.value = Float(storedDz?.bestHarmPercent ?? 1)
                bestSlider.slider.sendActions(for: .valueChanged)
                createTextField(title: "Завтра вернитесь к этому заданию и опишите реальный ущерб.", text: storedDz == nil ? " " : "", placeholder: "Реальный ущерб", enabled: storedDz != nil)
                createSlider(text: "")
                createLabel(text: "Посмотрите, какой из прогнозов оказался реалистичнее. Делайте это домашнее задание ежедневно в течение месяца, чтобы в вашей голове сформировалась привычка реалистичного оценивания ожидаемой перспективы.")
            
            case .эмоцобоснов:
                createTextField(title: "Критично отнеситесь к тому, на чем вы основываете свои выводы.\nРазделите их на реальные подтвержденные факты и домыслы (догадки, мнения, гипотезы).\nВпишите их в соответствующие разделы", placeholder: "Проверенные факты")
                createTextField(placeholder: "Домыслы")
                createLabel(text: "Теперь в окошке домыслов добавьте надпись – «подлежит проверке».")
                createTextField(title: "Вернитесь к реальным фактам, перечитайте их и сформулируйте на их основе новую мысль.", placeholder: "Новая мысль")
                createEmoView()
            
            case .чбмышление:
                createTextField(title: "Для преодоления черно-белого мышления вам предстоит развивать диалектичность своего мышления. Говоря простым языком, вашей задачей будет допускать существование внутренних противоречий для любой ситуации. Например, если я считаю, что человек может быть либо прав, либо не прав. То в случае принятия своей правоты, я буду утверждать, что другой человек точно не прав.\nА теперь попробуйте допустить существование противоречивости. Легче всего это сделать, используя слова \"и вместе с тем\", или \"и в тоже время\", или \"одновременно\".\nНапример, я права в том-то, и вместе с тем не права в том-то. Он прав в том-то, и вместе с тем не прав в том-то.", placeholder: "Диалектная мысль")
            
            case .обесцениваниепозитивного:
                createTextField(title: "Запишите результат, которого вы достигли, но по каким-то причинам вам стало казаться, что это не особо и значимый результат, или вам просто повезло, или у других бы лучше получилось и т.д.", placeholder: "Незначительный для вас результат")
                createTextField(title: "Запишите, какой результат вы планировали получить, когда приступали к этому делу (к этой проблеме, ситуации).", placeholder: "Запланированный результат")
                createLabel(text: "Ответьте на следующие вопросы:")
                createTextField(title: "Изначальный результат был возможен к осуществлению по силам, имеющимся навыкам, по времени, по совокупности требований и имевшихся на тот момент обстоятельств? Имело ли место завышение требований?", placeholder: "Введите ответ")
                createTextField(title: "Сколько времени реально вы лично потратили на выполнение этого задания?", placeholder: "Потраченное время")
                createTextField(title: "Если к достижению этого результата привлекались другие люди, то сколько времени реально было затрачено другими людьми?", placeholder: "Потраченное время других людей")
                createTextField(title: "Какой системой оценивания результата вы пользовались? Является ли она универсальной для всех?", placeholder: "Система оценивания")
                createTextField(title: "Как бы вы оценили точно такую же выполненную работу другим человеком?", placeholder: "Оценка работы другого человека")
                createTextField(title: "Достигали ли вы такого же результата ранее за такое же время? Освоили ли вы какие-то новые знания, новые навыки в процессе выполнения этой задачи? Перечислите их. Где они могут вам понадобиться в будущем? Стали ли вы более эффективны в этом вопросе по сравнению с самим собой в прошлом?", placeholder: "Введите ответ")
            
            case .минмакс:
                createTextField(title: "Пока вы стремитесь к совершенству и обесцениваете свои усилия при малейших промахах, ваша жизнь наполнена тревогой. Ещё одна негативная сторона этой ошибки мышления - частое откладывание и незавершение дел в связи с тем, что они не достигают высокой планки.\nПредлагаю вам сосредоточиться не на постоянном усовершенствовании будущего результата, а на завершении дела и достижении этого результата. Помочь в этом процессе может разбивание глобальной задачи на небольшие шаги. Каждый из этих шагов можно завершить и при этом приблизиться к своей цели.", placeholder: "Поэтапные шаги")
            
            case .сверхобобщение:
                createTextField(title: "Проведите детальный подсчет того, что вы обобщали. Будьте дотошны, как ученый. Старайтесь учитывать все входящие данные, вместо того, чтобы подтасовывать результаты своего исследования.\nНапример, если вы считаете, что «я толстая, поэтому никогда не выйду замуж», то учитывайте всех толстых женщин, которых вы встречаете, и составьте реальную статистику, сколько из них замужем, а сколько нет.\nЗапишите результаты своего исследования в процентах или в абсолютных данных. Например, 40% (24 толстых девушек) не замужем и 60% (36 толстых девушек) замужем.", placeholder: "Результаты исследования")
                createTextField(title: "Сформулируйте вывод по своему исследованию и свою новую мысль.", placeholder: "Результаты исследования")
                createSlider(text: "Какую эмоцию вызывает новая мысль? Насколько она выражена в %?")
                createEmoView()
            
            case .чтениемыслей1:
                createTextField(title: "Проверьте свои догадки о содержании мыслей другого человека. Задайте ему прямой вопрос. В большинстве случаев вы сразу же получите ответ – проверено многими моими клиентами и мной лично. Это задание может сначала показаться вам страшным. Помните о том, что чем быстрее решитесь, тем меньше будете бояться. И ещё о том, что, угадывая мысли других людей, вы испытываете негативные эмоции дольше, чем во время самого вопроса.\n\nЗапишите результат своего эксперимента, чтобы не забыть, что вы узнали, и как изменилась ваша эмоция после эксперимента. Соответствуют ли ваши догадки реальным мыслям человека?", placeholder: "Результат эксперимента")
            
            case .чтениемыслей2:
                createTextField(title: "Вам в голову пришла догадка о том, что подумал или почувствовал другой человек. Запишите как минимум 10 альтернативных мыслей и чувств другого человека. Как ещё он, она или они могли бы подумать в этой ситуации? Что еще могли бы чувствовать?", placeholder: "Альтернативные мысли")
                createLabel(text: "Прочтите альтернативные мысли, среди этих мыслей и чувств вы наверняка найдете более реалистичные, чем та мысль, что первой пришла в вашу голову.")
            
            case .навешиваниеярлыков:
                createTextField(title: "Находите для каждого ярлыка как можно больше нейтральных или даже позитивных варианта оценки. Например, альтернативой «зануде» могут стать «настойчивый», «интересующийся чем-то своим», «общительный», «активный в решении своего вопроса», «ищущий поддержки» и т.д.  ", placeholder: "Негативная оценка")
                createTextField(placeholder: "Нейтральная или позитивная оценка")
            
            case .персонализация:
                createTextField(title: "Выбор этой ошибки мышления обозначает, что вы привыкли везде и во всем видеть свое присутствие. Например, если вы совершили ошибку, то обязательно её все заметят. Или если что-то случилось, то это обязательно по вашей вине.\nСейчас вам предстоит попрактиковаться в поиске других возможных причин происходящих событий и мыслей у окружающих вас людей.", placeholder: "Альтернативные причины и мысли")
                createLabel(text: "Пересмотрите свои записи и выберите из них те, которые более рационально объясняют происходящее.")
            
            case .туннельноемышление:
                createLabel(text: "Вы прекрасно умеете концентрироваться на негативных деталях. Настала пора приучать себя к поиску позитива. Найдите 10 позитивных моментов в сегодняшнем дне. Делайте это упражнение ежедневно в течение 1,5 месяцев.")
                createTextField(placeholder: "1. Позитивный момент")
                createTextField(placeholder: "2. Позитивный момент")
                createTextField(placeholder: "3. Позитивный момент")
                createTextField(placeholder: "4. Позитивный момент")
                createTextField(placeholder: "5. Позитивный момент")
                createTextField(placeholder: "6. Позитивный момент")
                createTextField(placeholder: "7. Позитивный момент")
                createTextField(placeholder: "8. Позитивный момент")
                createTextField(placeholder: "9. Позитивный момент")
                createTextField(placeholder: "10. Позитивный момент")
            
            case .долженствование1:
                createLabel(text: "Мы редко замечаем, как долженствования в отношении себя приводят к появлению требований к окружающим. Это упражнение направлено на выявление этих связей.\nПример: Я должна выполнить эту работу в срок, иначе я не получу оплату. А потому окружающие должны не мешать мне, не отвлекать меня на другие дела, иначе я не успею.\nПри таких обстоятельствах становится понятным чувство раздражения, которое возникает, как только окружающие люди меня о чем-либо спрашивают или просят во время работы.")
                createLabel(text: "Заполните пробелы:")
                createGap(text: "Я должен (-на)", end: ",")
                createGap(text: "Иначе", end: ",")
                createLabel(text: "А потому окружающие должны мне")
                createGap(end: ",")
                createGap(text: "Иначе", end: ".")
            
            case .долженствование2:
                createLabel(text: "В обратную сторону долженствование тоже передается.\nПример: люди должны приходить вовремя, чтобы можно было прогнозировать время, планировать дела, а потому я должна приходить вовремя или немного заранее, чтобы не заставлять себя ждать.")
                createLabel(text: "Заполните пробелы:")
                createGap(text: "Люди должны", end: ",")
                createGap(text: "Иначе", end: ",")
                createLabel(text: "А потому я должен")
                createGap(end: ",")
                createGap(text: "Иначе", end: ".")
            
            case .долженствование3:
                createTextField(title: "Выпишите свое требование к себе или другим. Обозначьте степень веры в это правило.", placeholder: "Требования к себе/другим")
                createSlider(text: "")
                createTextField(title: "Найдите как минимум 3 исключения из этого правила. Когда или в каких условиях его можно было бы не соблюдать? Чем больше исключений вы напишите, тем лучше.", placeholder: "Исключения")
                createSlider(text: "Оцените заново степень веры в собственное требование с учетом исключений.")
        }
        
        doneButton.isHidden = true
        if dz.next == nil {
            if dz == .катастрофизация2 && storedDz == nil {
                button.isHidden = true
                doneButton.isHidden = false
            } else {
                button.setTitle("Внести новую мысль в дневник", for: .normal)
            }
        } else {
            button.setTitle("Далее", for: .normal)
            anotherButton.isHidden = true
        }
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissEmotionPicker))
        gestureRecognizer.delegate = self
        scrollView.addGestureRecognizer(gestureRecognizer)
        scrollView.bounces = false
        
        updateNextButton()
    }
    
    @objc private func dismissEmotionPicker(tap: UITapGestureRecognizer) {
        guard
            let emoView = emoview,
            !emoView.slider.bounds.contains(tap.location(in: emoView.slider))
        else { return }
        
        emoview?.dismissEmotionPicker()
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    @IBAction private func done() {
        if dz == .катастрофизация2 {
            let cat = catastrhHomeWork!
            cat.worstHarm = textfields.first(where: { $0.placeholder == "Наихудший ожидаемый ущерб" })?.text
            cat.worstHarmPercent = Int(worstSlider.slider.value)
            cat.bestHarm = textfields.first(where: { $0.placeholder == "Наилучший ожидаемый ущерб" })?.text
            cat.bestHarmPercent = Int(bestSlider.slider.value)
            let realm = try! Realm()
            try! realm.write {
                realm.add(cat)
            }
        }
        onDone?()
    }
    
    @IBAction private func next() {
        if dz == .катастрофизация1 {
            let cat = catastrhHomeWork ?? CatastrhHomeWork()
            if cat.realm == nil {
                cat.goodResult = textfields.first(where: { $0.placeholder == "Хороший исход" })?.text
                cat.peopleThoughts = textfields.first(where: { $0.placeholder == "Мысли людей" })?.text
                cat.reasonableResult = textfields.first(where: { $0.placeholder == "Благоразумный прогноз" })?.text
            } else {
                let realm = try! Realm()
                try! realm.write {
                    cat.goodResult = textfields.first(where: { $0.placeholder == "Хороший исход" })?.text
                    cat.peopleThoughts = textfields.first(where: { $0.placeholder == "Мысли людей" })?.text
                    cat.reasonableResult = textfields.first(where: { $0.placeholder == "Благоразумный прогноз" })?.text
                }
            }
            onNext?(cat)
        } else if dz == .катастрофизация2 {
            let realm = try! Realm()
            if let storedDz = realm.objects(CatastrhHomeWork.self).first {
                try! realm.write {
                    realm.delete(storedDz)
                }
            }
            onNext?(nil)
        } else {
            onNext?(nil)
        }
    }
    
    @IBAction private func another() {
        onAnother?()
    }
    
    private func createSituationLabel(title: String, text: String) {
        let tit = UILabel()
        tit.numberOfLines = 0
        tit.text = title
        tit.font = .systemFont(ofSize: 16, weight: .semibold)
        tit.textColor = .darkSystemGray4
        
        let ans = UILabel()
        ans.numberOfLines = 0
        ans.text = text
        ans.font = .systemFont(ofSize: 17)
        ans.textColor = .lightSystemGray
        
        let under = UIView()
        under.backgroundColor = .active
        under.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        let underStack = UIStackView(arrangedSubviews: [ans, under])
        underStack.axis = .vertical
        underStack.spacing = 8
        
        let stack = UIStackView(arrangedSubviews: [tit, underStack])
        stack.axis = .vertical
        stack.spacing = 16
        
        rootStackView.addArrangedSubview(stack)
    }
    
    private func createTextField(title: String? = nil, text: String = "", placeholder: String = "Введите ответ", enabled: Bool = true) {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 12
        if let title = title {
            let label = UILabel()
            label.numberOfLines = 0
            label.text = title
            label.font = .systemFont(ofSize: 16, weight: .semibold)
            label.textColor = .darkSystemGray4
            stack.addArrangedSubview(label)
        }
        let textField = CustomTextField()
        textField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [.font: UIFont.systemFont(ofSize: 17), .foregroundColor: UIColor.lightSystemGray4])
        textField.textColor = .darkSystemGray5
        textField.font = .systemFont(ofSize: 17)
        textField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        textField.addTarget(self, action: #selector(updateNextButton), for: .editingChanged)
        textField.isUserInteractionEnabled = enabled
        textField.text = text
        stack.addArrangedSubview(textField)
        textfields.append(textField)
        
        rootStackView.addArrangedSubview(stack)
    }
    
    private func createLabel(text: String) {
        let tit = UILabel()
        tit.numberOfLines = 0
        tit.text = text
        tit.font = .systemFont(ofSize: 16, weight: .semibold)
        tit.textColor = .darkSystemGray4
        
        rootStackView.addArrangedSubview(tit)
    }
    
    @discardableResult
    private func createSlider(text: String) -> SliderView {
        let view = SliderView()
        view.text = text
        
        rootStackView.addArrangedSubview(view)
        
        return view
    }
    
    private func createEmoView() {
        emoview = EmoView()
        emoview?.onUpdateState = { [weak self] in
            self?.button.isEnabled = !(self?.emoview?.enabledEmotions.isEmpty ?? true)
        }
        
        rootStackView.addArrangedSubview(emoview!)
    }
    
    private func createGap(text: String? = nil, placeholder: String = "", end: String) {
        let stack = UIStackView()
        stack.distribution = .fill
        stack.axis = .horizontal
        stack.spacing = 2
        if let title = text {
            let label = UILabel()
            label.text = title
            label.font = .systemFont(ofSize: 16, weight: .semibold)
            label.textColor = .darkSystemGray4
            stack.addArrangedSubview(label)
        }
        let textField = CustomTextField()
        textField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [.font: UIFont.systemFont(ofSize: 17), .foregroundColor: UIColor.lightSystemGray4])
        textField.textColor = .darkSystemGray5
        textField.font = .systemFont(ofSize: 17)
        textField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        textField.setContentCompressionResistancePriority(.defaultHigh, for: .horizontal)
        textField.setContentHuggingPriority(.init(rawValue: 1), for: .horizontal)
        textField.addTarget(self, action: #selector(updateNextButton), for: .editingChanged)
        stack.addArrangedSubview(textField)
        textfields.append(textField)
        
        let endLabel = UILabel()
        endLabel.text = end
        endLabel.font = .systemFont(ofSize: 16, weight: .semibold)
        endLabel.textColor = .darkSystemGray4
        stack.addArrangedSubview(endLabel)
        
        rootStackView.addArrangedSubview(stack)
    }
    
    @objc private func updateNextButton() {
        let enabled = !textfields.contains(where: { $0.text?.isEmpty ?? true }) && !(emoview?.enabledEmotions.isEmpty ?? false)
        button.isEnabled = enabled
        doneButton.isEnabled = enabled
    }

}
