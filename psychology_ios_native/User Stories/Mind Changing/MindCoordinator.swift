//
//  MindCoordinator.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 08.02.2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import Foundation
import RealmSwift

final class MindCoordinator: BaseCoordinator {
    
    var onDiary: Callback?
    var editRecord: ObjectCallback<Record>?
    var finishFlow: Callback?
    var onAddRecord: Callback?
    
    var selectDzModule: Presentable?
    private let parentRouter: Routable
    
    init(router: Routable) {
        self.parentRouter = router
    }
    
    private weak var view: MindChangingViewController?
    
    private var rootRouter: Routable?
    private var flowRouter: Routable?
    
    var thoughtsDict: [Int: ThoughtRealm] = [:]
}

// MARK:- Coordinatable

extension MindCoordinator: Coordinatable {
    
    func start() {
        performFlow()
    }
    
    func start(record: Record) {
        guard (UIApplication.shared.delegate as? AppDelegate)?.isPurchased ?? false else { return }
        
        rootRouter?.popToRootModule(animated: true)
        self.show(record)
    }
    
    func startWork(record: Record) {
        guard (UIApplication.shared.delegate as? AppDelegate)?.isPurchased ?? false else { return }
        
        rootRouter?.popToRootModule(animated: true)
        let vc = VigDostViewController.instanceController(storyboard: .mind)
        vc.isVig = true
        vc.record = record
        vc.onContinue = { [weak self] in
            let vc = VigDostViewController.instanceController(storyboard: .mind)
            vc.isVig = false
            vc.record = record
            vc.onContinue = {
                self?.showDetect(record: record)
            }
            self?.rootRouter?.push(vc)
        }
        vc.onAnother = { [weak self] in
            self?.rootRouter?.popToRootModule(animated: true)
        }
        rootRouter?.push(vc)
    }
    
}

// MARK:- Private methods

private extension MindCoordinator {
    
    func performFlow() {
        let view = MindChangingViewController.instanceController(storyboard: .mind)
        view.onSelectDate = { [weak self, weak view] in
            self?.getDate(completion: { date in
                guard let date = date else { return }
                view?.select(date: date)
            })
        }
        view.onSelect = { [weak self] record in
            self?.show(record)
        }
        view.onFind = { [weak self] in
            self?.startFind()
        }
        view.onDz = { [weak self] in
            self?.showSelectDz()
        }
        view.onContrCop = { [weak self] in
            self?.startContrCop()
        }
        
        rootRouter = ModulesFactory().makeRouter(root: view, hideBar: false)
        
        parentRouter.push(rootRouter, animated: false)
        
        self.view = view
    }
    
    func getDate(completion: @escaping (Date?) -> ()) {
        let calendarCoordinator = CalendarCoordinator(with: ModulesFactory(), router: parentRouter)
        calendarCoordinator.finishFlow = { [weak self, weak calendarCoordinator] date in
            self?.removeDependency(calendarCoordinator)
            
            completion(date)
        }
        addDependency(calendarCoordinator)
        calendarCoordinator.start()
    }
    
    func startContrCop() {
        thoughtsDict = [:]
        let vc = ContrCopViewController.instanceController(storyboard: .mind)
        vc.onGo = { [weak self] thought in
            self?.showContrList(thought: thought)
        }
        vc.onAdd = { [weak self] in
            self?.startAddTh()
        }
        rootRouter?.push(vc)
    }
    
    func startAddTh() {
        let vc = AddThViewController.instanceController(storyboard: .mind)
        vc.onAdd = { [weak self] str in
            let th = ThoughtRealm()
            th.thought = str
            self?.showContrList(thought: th)
        }
        rootRouter?.push(vc)
    }
    
    func showContrList(thought: ThoughtRealm) {
        let vc3 = ContrListViewController.instanceController(storyboard: .mind)
        vc3.thought = thought
        
        vc3.onAdd = { [weak self] in
            let vc4 = ContrViewController.instanceController(storyboard: .mind)
            vc4.onNext = { [weak self, weak vc3] contr in
                let re = ContrRealm()
                re.contr = contr.rec
                re.percent = contr.percent
                vc3?.add(contr: re)
                self?.rootRouter?.popModule()
            }
            self?.rootRouter?.push(vc4)
        }
        vc3.onMind = { [weak self] in
            self?.rootRouter?.popToRootModule(animated: true)
        }
        vc3.onAnother = { [weak self] in
            self?.rootRouter?.popModule()
        }
        rootRouter?.push(vc3)
    }
    
    func startFind() {
        let vc = GlubViewController.instanceController(storyboard: .mind)
        vc.onGo = { [weak self] th in
            self?.thoughtsDict = [:]
            let vc2 = ChThoughtViewController.instanceController(storyboard: .mind)
            vc2.thoughts = th
            vc2.onGo = { [weak self] thought in
                let index = th.firstIndex(of: thought)!
                let p: ThoughtRealm
                if let cache = self?.thoughtsDict[index], cache.thought == thought {
                    p = cache
                } else {
                    p = ThoughtRealm()
                    p.thought = thought
                    self?.thoughtsDict[index] = p
                }
                self?.showContrList(thought: p)
            }
            self?.rootRouter?.push(vc2)
        }
        rootRouter?.push(vc)
    }
    
    
    func show(_ record: Record) {
        let view = AddRecordViewController.instanceController(storyboard: .diary)
        view.record = record
        view.onClose = { [weak self] in
            self?.rootRouter?.popToRootModule(animated: true)
        }
        view.canEdit = false
        view.forceShowStart = true
        view.onStartMind = { [weak self] data in
            let vc = VigDostViewController.instanceController(storyboard: .mind)
            vc.isVig = true
            vc.record = record
            vc.onContinue = { [weak self] in
                let vc = VigDostViewController.instanceController(storyboard: .mind)
                vc.isVig = false
                vc.record = record
                vc.onContinue = {
                    self?.showDetect(record: record)
                }
                vc.onAnother = { [weak self] in
                    self?.rootRouter?.popToRootModule(animated: true)
                }
                self?.rootRouter?.push(vc)
            }
            vc.onAnother = { [weak self] in
                self?.rootRouter?.popToRootModule(animated: true)
            }
            self?.rootRouter?.push(vc)
        }
        view.onHomeWork = { [weak self] in
            self?.showSelectDz()
        }
        view.onDiary = { [weak self] in
            guard let self = self else { return }
            
            self.rootRouter?.popToRootModule(animated: false)
            let tabVC = self.parentRouter as! UITabBarController
            tabVC.selectedViewController = tabVC.viewControllers![2]
        }
        
        view.valhalla = false
        
        rootRouter?.push(view)
    }
    
    func showDetect(record: Record) {
        let detect = DetectMindErrorViewController.instanceController(storyboard: .mind)
        detect.onReady = { [weak self] in
            self?.showSelect(record: record)
        }
        detect.onAnother = { [weak self] in
            self?.rootRouter?.popToRootModule(animated: true)
        }
        rootRouter?.push(detect)
    }
    
    func showSelect(record: Record) {
        let vc = DetectMindErrorActionViewController.instanceController(storyboard: .mind)
        vc.record = record
        vc.onSelect = { [weak self] shit in
            self?.showShit(error: shit, record: record)
        }
        vc.onDiary = onDiary
        rootRouter?.push(vc)
    }
    
    func showShit(error: MindError, record: Record) {
        let vc = MChangeViewController.instanceController(storyboard: .mind)
        vc.record = record
        vc.error = error
        vc.onNext = { [weak self] emo in
            if let next = error.next {
                self?.showShit(next: next, record: record)
            } else {
                self?.save(record: record, thought: emo.thought, emotions: emo.enabledEmotions)
                self?.rootRouter?.popToRootModule(animated: false)
                self?.editRecord?(record)
           }
        }
        rootRouter?.push(vc)
    }
    
    func showShit(next: MindContinue, record: Record) {
        let vc = MChangeViewController.instanceController(storyboard: .mind)
        vc.contnue = next
        vc.record = record
        vc.onNext = { [weak self] emo in
            if let next = next.next {
                self?.showShit(next: next, record: record)
            } else {
                self?.save(record: record, thought: emo.thought, emotions: emo.enabledEmotions)
                self?.rootRouter?.popToRootModule(animated: false)
                self?.editRecord?(record)
            }
        }
        rootRouter?.push(vc)
    }
    
    private func save(record: Record, thought: String?, emotions: [EmotionType: Int]?) {
        let realm = try! Realm()
        try! realm.write {
            if let thought = thought {
                record.thought = thought
            }
            if let emotions = emotions {
                record.emotions.removeAll()
                emotions.forEach { emo in
                    let emotion = EmotionRealm()
                    emotion.type = emo.key.rawValue
                    emotion.value = emo.value
                    record.emotions.append(emotion)
                }
            }
            self.view?.update(record: record)
        }
    }
    
    func showSelectDz() {
        let vc = DzActionViewController.instanceController(storyboard: .mind)
        vc.onSelect = { [weak self] error in
            let dz: Dz
            switch error {
                case .катастрофизация: dz = .катастрофизация1
                case .эмоцобоснов: dz = .эмоцобоснов
                case .чбмышление: dz = .чбмышление
                case .обесцениваниепозитивного: dz = .обесцениваниепозитивного
                case .минмакс: dz = .минмакс
                case .сверхобобщение: dz = .сверхобобщение
                case .чтениемыслей: dz = .чтениемыслей1
                case .навешиваниеярлыков: dz = .навешиваниеярлыков
                case .персонализация: dz = .персонализация
                case .туннельноемышление: dz = .туннельноемышление
                case .долженствование: dz = .долженствование1
            }
            self?.showDz(next: dz)
        }
        vc.onContinue = { [weak self] cat in
            self?.showDz(next: .катастрофизация1, catastrhHomeWork: cat)
        }
        vc.onDiary = onDiary
        rootRouter?.push(vc)
        selectDzModule = vc
    }
    
    func showDz(next: Dz, catastrhHomeWork: CatastrhHomeWork? = nil) {
        let vc = DzViewController.instanceController(storyboard: .mind)
        vc.dz = next
        vc.catastrhHomeWork = catastrhHomeWork
        vc.onDone = { [weak self] in
            self?.rootRouter?.popToRootModule(animated: true)
        }
        vc.onNext = { [weak self] catastrhHomeWork in
            if let dz = next.next {
                self?.showDz(next: dz, catastrhHomeWork: catastrhHomeWork)
            } else {
                self?.rootRouter?.popToRootModule(animated: false)
                self?.onAddRecord?()
            }
        }
        vc.onAnother = { [weak self] in
            if let selectDzModule = self?.selectDzModule {
                self?.rootRouter?.popToModule(selectDzModule, animated: true)
            }
        }
        rootRouter?.push(vc)
    }
    
}
