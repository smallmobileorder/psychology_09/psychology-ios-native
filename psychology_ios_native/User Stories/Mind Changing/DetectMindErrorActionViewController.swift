//
//  DetectMindErrorActionViewController.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 20.02.2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

class DetectMindErrorActionViewController: UIViewController {
    
    var onSelect: ObjectCallback<MindError>?
    var onDiary: Callback?
    var record: Record!
    
    @IBOutlet private var sitLabel: UILabel!
    @IBOutlet private var thLabel: UILabel!
    @IBOutlet private var buttons: [UIButton]!
    @IBOutlet private var goButton: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        sitLabel.text = record.situatuon
        thLabel.text = currentThought ?? record.thought

        for button in buttons {
            button.addTarget(self, action: #selector(selectt), for: .touchUpInside)
        }
    }
    
    @objc private func selectt(_ sender: UIButton) {
        goButton.isEnabled = true
        sender.isSelected = true
        for button in buttons where button != sender {
            button.isSelected = false
        }
    }
    
    @IBAction private func go() {
        let index = buttons.firstIndex(where: { $0.isSelected })!
        onSelect?(MindError.allCases[index])
    }
    
    @IBAction private func goDiary() {
        onDiary?()
    }
    
}
