//
//  DetectMindErrorViewController.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 18.02.2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

enum MindContinue: String, CaseIterable {
    case идеалКатастрофы
    case декатастрофизация
    case реальнаяОценка
    case возможныеВыгоды
    case яНеМогу
    case мнеНадо
    case онОнаДолжны
    
    var titleTop: String {
        switch self {
            case .идеалКатастрофы: return "Идеал катастрофы"
            case .декатастрофизация: return "Декатастрофизация"
            case .реальнаяОценка: return "Реальная оценка"
            case .возможныеВыгоды: return "Возможные выгоды"
            case .яНеМогу: return "Я не могу"
            case .мнеНадо: return "Мне надо"
            case .онОнаДолжны: return "Он, она, они должны"
        }
    }
    
    var next: MindContinue? {
        switch self {
            case .идеалКатастрофы: return .декатастрофизация
            case .реальнаяОценка: return .возможныеВыгоды
            case .яНеМогу: return .мнеНадо
            case .мнеНадо: return .онОнаДолжны
            default: return nil
        }
    }
}

enum MindError: String, CaseIterable {
    case катастрофизация = "disastrous"
    case эмоцобоснов = "rationale"
    case чбмышление = "bw"
    case обесцениваниепозитивного = "deprecation"
    case минмакс = "minMax"
    case сверхобобщение = "overgen"
    case чтениемыслей = "mindReading"
    case навешиваниеярлыков = "labeling"
    case персонализация = "person"
    case туннельноемышление = "tunnel"
    case долженствование = "commitment"
    
    var titleTop: String {
        switch self {
            case .катастрофизация: return "Катастрофизация"
            case .эмоцобоснов: return "Эмоциональное обоснование"
            case .чбмышление: return "Черно-белое мышление"
            case .обесцениваниепозитивного: return "Обесценивание позитивного"
            case .минмакс: return "Максимализм / минимализм"
            case .сверхобобщение: return "Сверхобобщение"
            case .чтениемыслей: return "Чтение мыслей"
            case .навешиваниеярлыков: return "Навешивание ярлыков"
            case .персонализация: return "Персонализация"
            case .туннельноемышление: return "Туннельное мышление"
            case .долженствование: return "Я должен"
        }
    }
    
    var next: MindContinue? {
        switch self {
            case .катастрофизация: return .идеалКатастрофы
            case .обесцениваниепозитивного: return .реальнаяОценка
            case .долженствование: return .яНеМогу
            default: return nil
        }
    }
    
    var title: String {
        switch self {
            case .катастрофизация: return "Катастрофизация – будущее предсказывается негативым. Даже если вероятность ужасного последствия минимальна, возникает мысль «а что, если…»."
            case .эмоцобоснов: return "Эмоциональное обоснование – обоснование вывода на чувствах и интуитивной вере, игнорирование или обесценивание доказательств обратного."
            case .чбмышление: return "Черно-белое мышление – привычка оценивать контрастно, либо плохой, либо хороший, среднего не дано. "
            case .обесцениваниепозитивного: return "Обесценивание позитивного – привычка не придавать значения успехам и позитивному опыту. Например, списывать свои или чужие заслуги на везение или сравнивать с другими людьми, у которых этот навык или знания лучше."
            case .минмакс: return "Минимализм / максимализм – привычка преувеличивать негативное и преуменьшать позитивное. Стремление к тому, чтобы сделать все по максимуму, на высший балл. То, что не достигает идеала, не засчитывается в достижения."
            case .сверхобобщение: return "Сверхобобщение – привычка делать неоправданные обобщения на избирательных фактах. Её легко найти по словам \"все\", \"никто\", \"ничто\", \"всюду\", \"нигде\", \"никогда\", \"всегда\", \"вечно\", \"постоянно\"."
            case .чтениемыслей: return "Чтение мыслей - уверенность в том, что вы знаете мысли окружающих, без принятия во внимание других, более вероятных возможностей."
            case .навешиваниеярлыков: return "Навешивание ярлыков – привычка давать оценку личности по отдельному поступку или чертам человека. Без учета доказательств, смягчающих эту оценку."
            case .персонализация: return "Персонализация – привычка считать себя причиной негативного поведения других людей без учета других вариантов объяснений. Считать, что окружающие замечают ваши ошибки или просчеты, как будто вы находитесь в центре внимания."
            case .туннельноемышление: return "Туннельное мышление – восприятие только негативных аспектов ситуации. "
            case .долженствование: return "Долженствование - наличие четкой непреложной идеи о том, как должны вести себя другие люди, как надо себя вести и каким обязан быть окружающий мир."
        }
    }
    
    var example: String {
        switch self {
            case .катастрофизация: return "Если я провалю экзамен, это будет ужасно!"
            case .эмоцобоснов: return "Да, он мне совсем не помогает, но я верю, что он хочет заботиться обо мне."
            case .чбмышление: return "Если я не добьюсь успеха во всем, то я неудачник."
            case .обесцениваниепозитивного: return "То, что я заняла второе место на городской олимпиаде – случайность."
            case .минмакс: return "Четверка говорит о том, что я неспособный. Оценка 5 не говорит о том, что я умный."
            case .сверхобобщение: return "Вечно ты закатываешь мне истерики."
            case .чтениемыслей: return "Он наверняка думает, что я неуверенная в себе."
            case .навешиваниеярлыков: return "Она не знает, что такое интеграл, да она тупица."
            case .персонализация: return "Они наверняка шепчутся обо мне."
            case .туннельноемышление: return "Ты совсем не справляешься с домашними заданиями, посмотри, вот тут помарки, тут ошибка."
            case .долженствование: return "Я должен заботиться о своей сестре."
        }
    }
    
    var activeIcon: UIImage { UIImage(named: rawValue.appending("Active"))! }
    var nonActiveIcon: UIImage { UIImage(named: rawValue.appending("NonActive"))! }
}

class DetectMindErrorViewController: UIViewController {
    
    var onReady: Callback?
    var onAnother: Callback?

    @IBOutlet private var stackView: UIStackView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for error in MindError.allCases {
            let imageView = UIImageView(image: error.nonActiveIcon)
            NSLayoutConstraint.activate([
                imageView.heightAnchor.constraint(equalToConstant: 36),
                imageView.widthAnchor.constraint(equalToConstant: 36)
            ])
            let descLabel = UILabel()
            descLabel.text = error.title
            descLabel.numberOfLines = 0
            descLabel.font = .systemFont(ofSize: 13)
            descLabel.textColor = .darkSystemGray2
            let exampleLabel = UILabel()
            exampleLabel.text = "Пример:"
            exampleLabel.font = .systemFont(ofSize: 13, weight: .semibold)
            exampleLabel.textColor = .darkSystemGray2
            let exampleContentLabel = UILabel()
            exampleContentLabel.text = error.example
            exampleContentLabel.numberOfLines = 0
            exampleContentLabel.font = .systemFont(ofSize: 13)
            exampleContentLabel.textColor = .darkSystemGray2
            let vertStack = UIStackView(arrangedSubviews: [descLabel, exampleLabel, exampleContentLabel])
            vertStack.axis = .vertical
            vertStack.spacing = 8
            vertStack.alignment = .fill
            let horStack = UIStackView(arrangedSubviews: [imageView, vertStack])
            horStack.axis = .horizontal
            horStack.spacing = 0
            horStack.alignment = .top
            
            stackView.addArrangedSubview(horStack)
        }
    }
    
    @IBAction func ready() {
        onReady?()
    }
    
    @IBAction func selectAnother() {
        onAnother?()
    }

}
