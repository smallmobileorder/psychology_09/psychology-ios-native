//
//  AddThViewController.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 24/04/2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

class AddThViewController: UIViewController {
    @IBOutlet private var thTextField: UITextField!
    @IBOutlet private var addButton: UIButton!
    
    var onAdd: ObjectCallback<String>?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.setDismissKeyboardOnTap()

        thTextField.addTarget(self, action: #selector(updateAddButton), for: .editingChanged)
        thTextField.attributedPlaceholder = NSAttributedString(string: "Введите мысль", attributes: [.font: UIFont.systemFont(ofSize: 17), .foregroundColor: UIColor.lightSystemGray4])
        thTextField.textColor = .darkSystemGray5
        thTextField.font = .systemFont(ofSize: 17)
    }
    
    @objc private func updateAddButton() {
        addButton.isEnabled = !(thTextField.text?.isEmpty ?? true)
    }
    
    @IBAction private func add() {
        onAdd?(thTextField.text ?? "")
    }
}
