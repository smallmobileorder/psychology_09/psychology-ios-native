//
//  GlubViewController.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 29/03/2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

class GlubViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet private var rootStackView: UIStackView!
    @IBOutlet private var button: UIButton!
    private var textfields: [UITextField] = []
    
    var onGo: ObjectCallback<[String]>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createLabel(text: "За теми мыслями, что автоматически появляются в нашей голове, всегда стоят глубинные убеждения, уходящие своими корнями в далекое прошлое. Готовы приступить к поиску? ")
        createTextField(title: "Запишите мысль, глубинное значение которой хотите найти.")
        createLabel(text: "Вам предстоит отвечать на вопрос, а затем читать ответ и снова задавать себе вопрос уже применительно к этому ответу. Так с каждым разом вы будете все глубже и глубже погружаться в исследование смысла своего убеждения. Если вы стали повторяться с ответами, то можете остановить свои поиски.")
        createTextField(title: "Начнем. Ответьте на вопрос: Что эта мысль значит для вас? ")
        createTextField(title: "Если это так, то что это значит для вас?")
        createTextField(title: "Если это так, то что это значит для вас?")
        createTextField(title: "Если это так, то как при этом вы воспринимаете себя?")
        createTextField(title: "Если это так, то что это значит для вас?")
        createTextField(title: "Если это так, то каким вы воспринимаете мир?")
        createTextField(title: "Если это так, то каким вы воспринимаете окружающих людей?")
        createTextField(title: "Если это так, то что это значит для вас?")
        createTextField(title: "Если это так, то на чем вы основываете свое убеждение?")
        createTextField(title: "Если это так, то что это значит для вас?")
        createTextField(title: "Если это так, то что вы думаете о своей жизни?")
        createTextField(title: "Если это так, то что это значит для вас?")
        createTextField(title: "Если это так, то каким вы (или окружающие) должны быть?")
        createTextField(title: "Если это так, то что это значит для вас?")
        createTextField(title: "Если это так, то что это значит для вас?")
        createTextField(title: "Если это так, то что это значит для вас?", next: false)
    }
    
    private func createSituationLabel(title: String, text: String) {
        let tit = UILabel()
        tit.numberOfLines = 0
        tit.text = title
        tit.font = .systemFont(ofSize: 16, weight: .semibold)
        tit.textColor = .darkSystemGray4
        
        let ans = UILabel()
        ans.numberOfLines = 0
        ans.text = text
        ans.font = .systemFont(ofSize: 17)
        ans.textColor = .lightSystemGray
        
        let under = UIView()
        under.backgroundColor = .active
        under.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        let underStack = UIStackView(arrangedSubviews: [ans, under])
        underStack.axis = .vertical
        underStack.spacing = 8
        
        let stack = UIStackView(arrangedSubviews: [tit, underStack])
        stack.axis = .vertical
        stack.spacing = 16
        
        rootStackView.addArrangedSubview(stack)
    }
    
    private func createTextField(title: String? = nil, placeholder: String = "Введите ответ", next: Bool = true) {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 12
        if let title = title {
            let label = UILabel()
            label.numberOfLines = 0
            label.text = title
            label.font = .systemFont(ofSize: 16, weight: .semibold)
            label.textColor = .darkSystemGray4
            stack.addArrangedSubview(label)
        }
        let textField = CustomTextField()
        textField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [.font: UIFont.systemFont(ofSize: 17), .foregroundColor: UIColor.lightSystemGray4])
        textField.textColor = .darkSystemGray5
        textField.font = .systemFont(ofSize: 17)
        textField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        textField.addTarget(self, action: #selector(updateNextButton), for: .editingChanged)
        textField.delegate = self
        if next {
            textField.returnKeyType = .continue
        } else {
            textField.returnKeyType = .done
        }
        stack.addArrangedSubview(textField)
        textfields.append(textField)
        
        rootStackView.addArrangedSubview(stack)
    }
    
    private func createLabel(text: String) {
        let tit = UILabel()
        tit.numberOfLines = 0
        tit.text = text
        tit.font = .systemFont(ofSize: 16, weight: .semibold)
        tit.textColor = .darkSystemGray4
        
        rootStackView.addArrangedSubview(tit)
    }
    
    private func createSlider(text: String) {
        let view = SliderView()
        view.text = text
        
        rootStackView.addArrangedSubview(view)
    }
    
    @IBAction private func go() {
        onGo?(textfields.dropFirst().map { $0.text ?? "" }.filter { !$0.isEmpty })
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textfields.last {
            view.endEditing(true)
        } else {
            let index = textfields.firstIndex(of: textField)!
            let nextIndex = textfields.index(after: index)
            textfields[nextIndex].becomeFirstResponder()
        }
        return true
    }
    
    @objc private func updateNextButton() {
        button.isEnabled = textfields.dropFirst().contains(where: { !($0.text?.isEmpty ?? true) })
    }

}
