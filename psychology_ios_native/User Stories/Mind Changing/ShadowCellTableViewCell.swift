//
//  ShadowCellTableViewCell.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 05/04/2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

class ShadowCellTableViewCell: UITableViewCell {
    
    @IBOutlet private var label: UILabel!
    @IBOutlet private var percLabel: UILabel!
    
    func set(title: String, perc: Int) {
        label.text = title
        percLabel.text = "\(perc)%"
    }
}
