//
//  MindChangingViewProtocol.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 12.01.2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

protocol MindChangingViewProtocol: BaseViewProtocol {
    var onSelectDate: Callback? { get set }
    var onFind: Callback? { get set }
    func select(date: Date)
    func update(record: Record)
}

