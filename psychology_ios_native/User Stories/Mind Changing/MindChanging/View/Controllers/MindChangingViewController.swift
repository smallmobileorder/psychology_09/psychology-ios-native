//
//  MindChangingViewController.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 12.01.2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit
import RealmSwift
import SwiftyStoreKit
import StoreKit

class MindChangingViewController: UIViewController, MindChangingAssemblable {
    
    @IBOutlet var ifYouWantLabel: UILabel!
    @IBOutlet var ifYouConstraint: NSLayoutConstraint!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var tableView: AdvancedTableView!
    @IBOutlet var emptyLabel: UILabel!
    @IBOutlet var notPurchasedView: UIView!
    @IBOutlet var hwButton: BrandButton!
    @IBOutlet var findButton: BrandButton!
    @IBOutlet var copButton: BrandButton!
    
	var presenter: MindChangingPresenterInput?
    var onSelectDate: Callback?
    var onFind: Callback?
    var onSelect: ObjectCallback<Record>?
    var onDz: Callback?
    var onContrCop: Callback?
    
    private var selectedRecordIndexPath: IndexPath?
    
    @IBAction func selectDate() {
        onSelectDate?()
    }
    
    @IBAction func goToFind() {
        onFind?()
    }
    
    @IBAction func showDz() {
        onDz?()
    }
    
    @IBAction func showCop() {
        onContrCop?()
    }
    
    func select(date: Date) {
        selectedDate = date
    }
    
    func update(record: Record) {
        if let indexPath = selectedRecordIndexPath {
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
    private var sections: [Record] = []
    private var selectedDate: Date! {
        didSet {
            tableView.sizeToFitTableHeaderView()
            dateLabel.text = selectedDate.string(format: "dd.MM.yyyy")
            
            let realm = try! Realm()
            let calendar = Calendar.current
            sections = Array(realm.objects(Record.self)).filter {
                calendar.isDate($0.date, equalTo: selectedDate, toGranularity: .day) &&
                calendar.isDate($0.date, equalTo: selectedDate, toGranularity: .month) &&
                calendar.isDate($0.date, equalTo: selectedDate, toGranularity: .year)
            }
            
            tableView.reloadData()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        notPurchasedView.isHidden = (UIApplication.shared.delegate as? AppDelegate)?.isPurchased ?? false
        
        ifYouConstraint.isActive = false
        
        tableView.registerNib(cell: DiaryRecordTableViewCell.self)
        tableView.contentInset = .only(bottom: 88)
        
        for button in [hwButton!, findButton!, copButton!] {
            button.normalColor = .white
            button.setTitleColor(.darkSystemGray2, for: .normal)
        }
        
        NotificationCenter.default.addObserver(forName: .purchased, object: nil, queue: .main) { _ in
            self.notPurchasedView.isHidden = (UIApplication.shared.delegate as! AppDelegate).isPurchased
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name("record_delete"), object: nil, queue: .main) { [weak self] notification in
            guard let self = self else { return }
            guard let recordID = notification.object as? String else { return }
            guard let index = self.sections.firstIndex(where: { $0.isInvalidated }) else { return }
            
            self.sections.remove(at: index)
            
            self.tableView.beginUpdates()
            self.tableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
            self.tableView.endUpdates()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let date = selectedDate
        if date != nil {
            self.selectedDate = date
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.sizeToFitTableHeaderView()
        tableView.sizeToFitTableFooterView()
    }
    
    @IBAction private func purchase() {
        SwiftyStoreKit.purchaseProduct("com.mozgoprav.app.mind_change") { result in
            switch result {
                case .success:
                    (UIApplication.shared.delegate as? AppDelegate)?.isPurchased = true
                    NotificationCenter.default.post(name: .purchased, object: nil)
                case .error(error: let error):
                    let errorString: String
                    switch error.code {
                        case .clientInvalid: errorString = "Оплата недоступна"
                        case .paymentCancelled: return
                        case .paymentInvalid: errorString = "Неверный номер оплаты"
                        case .paymentNotAllowed: errorString = "Это устройство недоступно для оплаты"
                        case .storeProductNotAvailable: errorString = "Подписка недоступна"
                        case .cloudServicePermissionDenied: errorString = "Нет доступа к Cloud сервису"
                        case .cloudServiceNetworkConnectionFailed: errorString = "Нет подключения к интернету"
                        case .cloudServiceRevoked: errorString = "Нет доступа к Cloud сервису"
                        default: errorString = "Неизвестная ошибка"
                    }
                    let alert = UIAlertController(title: "Ошибка", message: errorString, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "ОК", style: .default, handler: nil))
                    self.present(alert, animated: true)
            }
        }
    }
    
    @IBAction private func restorePurchases() {
        SwiftyStoreKit.restorePurchases(atomically: true) { [weak self] results in
            if results.restoreFailedPurchases.count > 0 {
                print("Restore Failed: \(results.restoreFailedPurchases)")
            }
            else if results.restoredPurchases.count > 0 {
                (UIApplication.shared.delegate as? AppDelegate)?.isPurchased = true
                NotificationCenter.default.post(name: .purchased, object: nil)
            }
            else {
                (UIApplication.shared.delegate as? AppDelegate)?.isPurchased = false
                self?.showAlert(.error("На Вашем аккаунте не куплен этот платный раздел"), onCompletion: {})
            }
        }
    }
    
}

extension MindChangingViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(DiaryRecordTableViewCell.self)!
        
        let record = sections[indexPath.row]
        
        cell.situationLabel.text = record.situatuon
        cell.timeLabel.text = record.date.string(format: "H:mm")
        cell.thoughtsLabel.text = record.thought
        
        return cell
    }
    
}

extension MindChangingViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let record = sections[indexPath.row]
        
        selectedRecordIndexPath = indexPath
        
        DispatchQueue.main.async {
            self.onSelect?(record)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let record = sections.remove(at: indexPath.row)
            let id = record.recordID
            let realm = try! Realm()
            try! realm.write {
                realm.delete(record)
            }
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
            
            NotificationCenter.default.post(name: NSNotification.Name("record_delete"), object: id)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        true
    }
}
