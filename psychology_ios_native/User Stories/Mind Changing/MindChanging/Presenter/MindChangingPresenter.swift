//
//  MindChangingPresenter.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 12.01.2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

protocol MindChangingPresenterInput: class {

}

protocol MindChangingPresenterOutput: class {
    var presenter: MindChangingPresenterInput? { get set }
}

final class MindChangingPresenter {

    weak var output: MindChangingPresenterOutput?
    
}

// MARK:- MindChangingPresenterInput
extension MindChangingPresenter: MindChangingPresenterInput {

}
