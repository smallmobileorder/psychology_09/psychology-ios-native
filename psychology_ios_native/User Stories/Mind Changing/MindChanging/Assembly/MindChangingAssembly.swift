//
//  MindChangingAssembly.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 12.01.2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

protocol MindChangingAssemblable: MindChangingViewProtocol, MindChangingPresenterOutput {}

final class MindChangingAssembly {
    static func assembly(with output: MindChangingPresenterOutput) {
        let presenter  = MindChangingPresenter()
    
        presenter.output     = output
        output.presenter     = presenter
    }
}
