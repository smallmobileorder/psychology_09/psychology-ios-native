//
//  MChangeViewController.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 21.02.2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

class MChangeViewController: UIViewController, UIGestureRecognizerDelegate {
    
    var onNext: ObjectCallback<(thought: String?, enabledEmotions: [EmotionType: Int]?)>?
    
    var record: Record!
    var error: MindError?
    var contnue: MindContinue?
    
    @IBOutlet private var scrollView: UIScrollView!
    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var rootStackView: UIStackView!
    @IBOutlet private var button: BrandButton!
    private var emoview: EmoView?
    private var textfields: [UITextField] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let error = error {
            titleLabel.text = error.titleTop
            createSituationLabel(title: "Ситуация:", text: record.situatuon ?? "")
            createSituationLabel(title: "Мысль", text: currentThought ?? record.thought ?? "")
            switch error {
                case .катастрофизация:
                    createSlider(text: "Оцените, насколько ужасным вам кажется произошедшее или предстоящее событие.")
                
                case .эмоцобоснов:
                    createLabel(text: "Сегодня ваша мысль направляется на судебное разбирательство.")
                    createTextField(title: "Докажите, что это так. Побудьте адвокатом для этого убеждения, защитите его.", placeholder: "Доказательства")
                    createTextField(title: "Докажите, что это не так. Побудьте прокурором для этого убеждения, приведите контраргументы. Обвините эту мысль в нелогичности и обоснуйте свои обвинения.", placeholder: "Доказательства")
                    createLabel(text: "К рассмотрению принимаются только факты, как это было бы в любом другом судебном процессе.")
                    createTextField(title: "Настало время судье принимать решение. Измените мысль так, чтобы она более реалистично оценивала ситуацию.", placeholder: "Новая мысль")
                    createEmoView()
                
                case .чбмышление:
                    createLabel(text: "Вы\u{2000}заметили\u{2000}у\u{2000}себя\u{2000}черно-белое\u{2000}мышление,\u{2000}это\u{2000}значит,\u{2000}что\u{2000}часто\u{2000}вы\u{2000}видите\u{2000}всего\u{2000}два\u{2000}варианта\u{2000}развития\u{2000}событий:\u{2000}либо\u{2000}так,\u{2000}либо\u{2000}эдак;\u{2000}либо\u{2000}хорошо,\u{2000}либо\u{2000}плохо;\u{2000}либо\u{2000}правильно,\u{2000}либо\u{2000}неправильно.\u{2000}Сегодня\u{2000}нам\u{2000}предстоит\u{2000}поиск\u{2000}промежуточных\u{2000}вариантов\u{2000}между\u{2000}этими\u{2000}полюсами.\nПеред\u{2000}вами\u{2000}окошки,\u{2000}соответствующие\u{2000}континууму\u{2000}от\u{2000}0%\u{2000}до\u{2000}100%.\nЗапишите\u{2000}в\u{2000}графу\u{2000}100%\u{2000}-\u{2000}самый\u{2000}позитивный\u{2000}полюс,\u{2000}соответствующий\u{2000}вашей\u{2000}мысли.\nА\u{2000}в\u{2000}графу\u{2000}0%\u{2000}-\u{2000}самый\u{2000}негативный\u{2000}полюс,\u{2000}соответствующий\u{2000}вашей\u{2000}установке.\nРазберу\u{2000}на\u{2000}примере\u{2000}двух\u{2000}мыслей:\n«Меня\u{2000}уволят\u{2000}из-за\u{2000}ошибки\u{2000}на\u{2000}работе»:\u{2000}0%\u{2000}-\u{2000}ничего\u{2000}не\u{2000}заплатят,\u{2000}100%\u{2000}-\u{2000}все\u{2000}равно\u{2000}заплатят.\n«Я\u{2000}буду\u{2000}выглядеть\u{2000}дурой,\u{2000}если\u{2000}пойду\u{2000}выступать\u{2000}перед\u{2000}всеми»:\u{2000}0%\u{2000}-\u{2000}«буду\u{2000}выглядеть\u{2000}дурой»,\u{2000}100%\u{2000}-\u{2000}«все\u{2000}пройдет\u{2000}хорошо,\u{2000}буду\u{2000}молодцом».")
                    createTextField(placeholder: "0%")
                    createTextField(placeholder: "10%")
                    createTextField(placeholder: "20%")
                    createTextField(placeholder: "30%")
                    createTextField(placeholder: "40%")
                    createTextField(placeholder: "50%")
                    createTextField(placeholder: "60%")
                    createTextField(placeholder: "70%")
                    createTextField(placeholder: "80%")
                    createTextField(placeholder: "90%")
                    createTextField(placeholder: "100%")
                    createLabel(text: "А теперь вам предстоит честно ответить на вопросы и согласно своим ответам внести промежуточные переменные, а может даже сместить записанные ранее полюса в окошко с другим процентом.")
                        createLabel(text: "Может ли быть что-то или кто-то, еще лучше, чем обозначенный вами 100% полюс? Например, могут ли вам заплатить не только зарплату, но ещё и дать премию. Может ли кто-то стать супер-молодцом и справиться еще лучше. Если это возможно, то сместите свой 100% полюс, ведь теперь у вас есть новые 100%")
                    createLabel(text: "Есть ли варианты развития событий, которые будут не так хороши как 90% или 100%, но при этом будут явно выше 0%? Впишите их в соответствующие графы. Например, могут выплатить аванс за ту часть работы, которая уже сделана, даже если в ней были совершены ошибки.")
                    createLabel(text: "Может ли быть на земле что-либо хуже, чем выглядеть дурой? Может ли быть что-то или кто-то еще хуже, чем обозначенный вами 0% полюс? Например, могут ли с вас ещё и деньги потребовать в качестве неустойки? Может, вы даже знаете человека гораздо глупее? Если это возможно, то сместите свой 0% полюс, ведь теперь у вас есть новые 0%.")
                    createLabel(text: "Постарайтесь заполнить полностью этот континуум, чтобы увидеть, где на самом деле находится данная ситуация.")
                    createTextField(title: "Измените мысль так, чтобы она более реалистично оценивала ситуацию.")
                    createTextField(title: "Измените мысль так, чтобы она более реалистично оценивала ситуацию.")
                    createEmoView()
                
                case .обесцениваниепозитивного:
                    createTextField(title: "Как изначально вы относились к этим достижениям? Каковы были ваши ожидания? Было ли важно для вас получить эти достижения?")
                    createTextField(title: "Связывали ли вы получение этих достижений с возможными кардинальными изменениями в своей жизни? Логичными ли были такие ожидания?")
                    createTextField(title: "Можно ли было реально выполнить то, что запланировано в указанные сроки?")
                    createTextField(title: "Как теперь вы оцениваете эти достижения?")

                case .минмакс:
                    createLabel(text: "Стремление к высоким стандартам становится ошибкой, когда доводится до крайности. Посмотрите, так ли важно достичь именно идеала? И стоит ли бросать работу, если она сделана на \"5-\" или \"4\", а не на \"5+\"?")
                    createLabel(text: "С этого дня нам придется привыкать к тому, что мир несовершенен, в нем возможны допустимые погрешности.")
                    createLabel(text: "Спросите себя честно. У меня (других) хватает ресурсов (времени, денег, сил, здоровья и т.д.), чтобы сделать именно это дело на \"5+\"? У вас (них) есть все необходимое, чтобы выполнить все именно так, как вы задумали в указанные сроки? Или вы превышаете свои (их) возможности?")
                    createTextField(title: "Сформулируйте, какую погрешность вы можете допустить, чтобы уложиться в имеющиеся у вас ресурсы? Что сейчас в приоритете, а что можно отложить?", placeholder: "Допустимая погрешность")
                    createTextField(title: "Помните, что в любом деле важно соотнести цену, которую вы платите, с тем результатом, который получаете. Сфомулируйте новую мысль так, чтобы цена была сопоставима с качеством.", placeholder: "Новая мысль")
                    createEmoView()

                case .сверхобобщение:
                    createTextField(title: "Для получения более рациональной мысли вам предстоит раскрыть все обобщающие слова. Например, если вы подумали «всегда», то уточните, «когда именно?». Если, вы написали «все», то будьте честны и раскройте, «кто конкретно». «Никогда» - «точно ли ни разу такого не было?». «Всюду» - «где именно?». «Каждый раз» - «сколько раз в неделю, в месяц или в год?», «правда ли это случалось каждый раз или все-таки были исключения?», «при каких именно обстоятельствах?». Запишите в окошко, как на самом деле все обстоит.", placeholder: "Объективное описание ситуации")
                    createTextField(title: "Посмотрите внимательно на то, что вы написали, и сформулируйте новое представление.", placeholder: "Новая мысль")
                    createEmoView()

                case .чтениемыслей:
                    createTextField(title: "Вам кажется, а может быть вы даже уверены, что точно знаете мысли и чувства другого человека. Давайте договоримся, что «прочитанные вами мысли» - это лишь гипотеза, которую мы не примем без неопровержимых доказательств. Запишите здесь только факты об этой ситуации. Внимание! Не допускаются домыслы, непроверенные моменты, догадки, оценки. Только голые факты. Что конкретно было сказано вслух, сделано. Что вы слышали, видели, что увидел и услышал бы любой другой человек, если бы посмотрел видеозапись этой ситуации.", placeholder: "Факты")
                    createTextField(title: "А теперь сосредоточьтесь на фактах. Достаточно ли их, чтобы подтвердить вашу гипотезу? Или может быть вернее будет другая мысль? Сформулируйте её.", placeholder: "Новая мысль")
                    createEmoView()

                case .навешиваниеярлыков:
                    createLabel(text: "Чтобы поставить оценку другому человеку или самому себе, надо для начала иметь шкалу. Например, 4 по 5-ти бальной системе оценок - это хорошо, а по 10-ти бальной системе оценок - неудовлетворительно. А какая у вас система оценок?")
                    createLabel(text: "Предположим, вы считаете себя некрасивым. Давайте определимся, по какой шкале идет этот замер. И так, на одном полюсе был бы самый некрасивый человек в мире. Кто это по вашему мнению? Опишите этот полюс. Пусть это будет 100% некрасивости.")
                    createTextField(title: "Это лишь пример, вам же предстоит описать 100% полюс вашей оценки.")
                    createTextField(title: "Теперь опишите другой полюс. В данном примере это был бы самый красивый человек в мире по вашему мнению. А вам предстоит описать 100%-ный полюс, противоположный вашей оценке.")
                    createTextField(title: "Вот теперь, когда у вас есть шкала, сделайте переоценку. Определите свое место или место другого человека на этой шкале.", placeholder: "Новая мысль")
                    createEmoView()
                
                case .персонализация:
                    createLabel(text: "Если вы действительно уверены в том, что находитесь в центре внимания окружающих и являетесь причиной происходящего вокруг или с другими людьми. То вам предстоит тщательно присмотреться к тому, где проходит граница разделения ответственности. Иначе вам будет сложно перестать излишне злиться, обижаться или винить себя.")
                    createTextField(title: "Опишите, за что в этой ситуации ответственны лично вы. Какие обязательства вы берете на себя и чем обосновываете это. В первую очередь старайтесь обращаться к ответственности, закрепленной юридически, затем к даным обещаниям.")
                    createTextField(title: "Опишите, за что в этой ситуации ответственны другие участники ситуации. Какие обязательства за ними и чем вы обосновываете это.")
                    createLabel(text: "Например, \"я виновата в том, что подруга рассталась со своим парнем. Ведь я понравилась её парню больше, чем она\".")
                    createLabel(text: "Моя ответственность при этом только в том, что я стремлюсь быть красивой, слежу за собой. Никаких других обязательств я в этой ситуации не давала. Я могу выбрать встречаться мне с мужчиной или нет.")
                    createLabel(text: "Ответственность моей подруги в том, чтобы строить отношения со своим парнем, формировать с ним общие интересы.")
                    createLabel(text: "Ответственность её парня в том, чтобы строить отношения со своей девушкой, формировать с ней общие интересы. И если ему понравилась другая, и парень понимает, что стоит расстаться, то сделать это.")
                    createEmoView()
                
                case .туннельноемышление:
                    createTextField(title: "Помните, что реальная оценка дает больше спокойствия, чем взгляд через негативную призму. Давайте учтем всю доступную информацию, а не только негатив.")
                    createTextField(title: "Выпишите негативные факты в этой ситуации.")
                    createTextField(title: "Выпишите позитивные факты в этой ситуации.")
                    createTextField(title: "Взвесьте все факты и переформулируйте свой вывод.")
                    createTextField(title: "Посмотрите внимательно на то, что вы написали и сформулируйте новое представление.", placeholder: "Новая мысль")
                    createEmoView()

                case .долженствование:
                    createLabel(text: "Посмотрите, что вы должны в связи с этой мыслью.")
                    createTextField(title: "Для более рациональной формулировки вашей мысли давайте заменим словосочетание «я должен» на «я решил». Отметьте, как изменяются ваши эмоции, когда вы примеряете к своей жизни это решение. Возможно, вам захочется каким-то образом изменить свое решение. Запишите здесь свое решение в таком виде, в котором оно соответствует вашему внутреннему желанию. Например, вместо «я должна быть всегда на связи» может появиться «я решила оставлять телефон включенным с 8 до 23 часов, а ночью я решила спать».", placeholder: "Ваше решение")
            }
            
            if error.next == nil {
                createLabel(text: "Повторная запись новой мысли позволит вам лучше её запомнить.")
                button.setTitle("Внести новую мысль в дневник", for: .normal)
            } else {
                button.setTitle("Далее", for: .normal)
            }
        }
        
        if let next = contnue {
            titleLabel.text = next.titleTop
            switch next {
                case .идеалКатастрофы:
                    createTextField(title: "Опишите самую максимальную катастрофу, какую только можете себе представить.  Самое ужасное, что только можете себе вообразить.", placeholder: "Ваш идеал катастрофы")
                
                case .декатастрофизация:
                    createLabel(text: "Поставьте описанный вами идеал катастрофы на полюс 100%.")
                    createSlider(text: "А теперь оцените заново, насколько ужасным вам кажется произошедшее или предстоящее событие в сравнении с идеалом катастрофы.")
                    createTextField(title: "Измените мысль так, чтобы она более реалистично оценивала уровень катастрофы.", placeholder: "Новая мысль")
                    createEmoView()
                
                case .реальнаяОценка:
                    createSlider(text: "Насколько процентов был осуществлен задуманный этап действий в реальности? Где 0% - не приступали вовсе, а 100% - все задуманное на данном этапе осуществлено.")
                    createSlider(text: "Насколько процентов было получено удовольствие от выполненного действия или в процессе действий? Где 0% - совсем не было удовольствия, а 100% - максимум удовольствия, которое можно было получить, выполняя это действие, или получая результат.")
                    createTextField(title: "Снизили ли бы вы цену таких достижений у самого дорого для вас человека (если нет в реальности такого человека, то представьте, например, своего будущего ребенка или любимого человека)?")
                
                case .возможныеВыгоды:
                    createTextField(title: "Для чего вы снизили цену? Из-за чего вы не хотите оплатить себе или другим усилия и старания в полном объеме? Что это вам дает?")
                    createTextField(title: "Что вам предстояло бы сделать, что вы возможно должны были бы сделать, если бы признали истинную цену?")
                    createTextField(title: "Измените мысль так, чтобы она более реалистично оценивала уровень достижений.", placeholder: "Новая мысль")
                    createEmoView()
                
                case .яНеМогу:
                    createSituationLabel(title: "Ситуация:", text: record.situatuon ?? "")
                    createSituationLabel(title: "Мысль", text: currentThought ?? record.thought ?? "")
                    createLabel(text: "Подумайте, что вы не можете себе позволить сделать или сказать в связи с этой мыслью.")
                    createTextField(title: "Для более рациональной формулировки вашей мысли давайте заменим словосочетание «я не могу» на «я не хочу». Отметьте, как изменяются ваши эмоции, когда вы примеряете к своей жизни этот запрет, этот выбор не желать чего-либо. Возможно, вам захочется каким-то образом изменить свое нежелание. Запишите здесь новую мысль с формулировкой «я не хочу» в таком виде, в которой она соответствует вашему внутреннему желанию. Например, вместо «я не могу отказывать друзьям» может появиться «я не хочу отказывать друзьям, когда у меня есть желание и возможность им помочь».", placeholder: "Ваше нежелание")
                    createEmoView()
                
                case .мнеНадо:
                    createSituationLabel(title: "Ситуация:", text: record.situatuon ?? "")
                    createSituationLabel(title: "Мысль", text: currentThought ?? record.thought ?? "")
                    createLabel(text: "Возможно, вам что-то требуется, что-то нужно в связи с этой мыслью.")
                    createTextField(title: "Для более рациональной формулировки вашей мысли давайте заменим словосочетание «мне надо» на «я хочу». Отметьте, как изменяются ваши эмоции, когда вы примеряете к своей жизни это желание, этот выбор. Возможно, вам захочется каким-то образом изменить свое желание. Запишите здесь новую мысль с формулировкой «я хочу» в таком виде, в которой она соответствует вашему внутреннему желанию. Например, вместо «мне надо выполнить эту работу в срок» может появиться «я  хочу сдать эту работу вовремя, т.к. я не люблю долгов».", placeholder: "Ваше желание")
                    createEmoView()
                
                case .онОнаДолжны:
                    createLabel(text: "Когда вы предъявляете к кому-то требования, а они не исполняются, то важно увидеть, кто в этой ситуации страдает больше: тот, кто не исполняет требования другого человека, или тот, кто ждет, что его требования исполнят, но этого не происходит?")
                    createLabel(text: "Если вы заметили, что ваши страдания выше, предлагаю ответить на вопросы:")
                    createTextField(title: "1. Прописан ли где-либо официально этот долг? Другими словами, имею ли я законные основания предъявлять свои требования?")
                    createTextField(title: "2. Имеет ли право другой человек быть самим собой, иметь свои ценности, свои желания и нежелания? Имеет ли он право не соответствовать моим требованиям, моим ожиданиям?")
                    createTextField(title: "3. Насколько выгодно другому человеку было бы соответствовать моим требованиям? Какие плюсы он получает сейчас, действуя по-своему? Хотелось ли бы мне самому порой также действовать, получить такие же плюсы?")
                    createTextField(title: "Сформулируйте новую мысль. Пусть вместо слов «он, она, они должны мне…» это убеждение начинается со слов «Мне бы хотелось, чтобы … И вместе с тем, я понимаю…» ", placeholder: "Новая мысль")
                    createEmoView()
            }
            
            if next.next == nil {
                createLabel(text: "Повторная запись новой мысли позволит вам лучше её запомнить.")
                button.setTitle("Внести новую мысль в дневник", for: .normal)
            } else {
                button.setTitle("Далее", for: .normal)
            }
        }
        
        updateNextButton()
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissEmotionPicker))
        gestureRecognizer.delegate = self
        scrollView.addGestureRecognizer(gestureRecognizer)
        scrollView.bounces = false
    }
    
    @objc private func dismissEmotionPicker(tap: UITapGestureRecognizer) {
        guard
            let emoView = emoview,
            !emoView.slider.bounds.contains(tap.location(in: emoView.slider))
        else { return }
        
        emoview?.dismissEmotionPicker()
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    @IBAction private func next() {
        onNext?((thought: textfields.first(where: { $0.placeholder == "Новая мысль" })?.text, enabledEmotions: emoview?.enabledEmotions))
    }
    
    private func createSituationLabel(title: String, text: String) {
        let tit = UILabel()
        tit.numberOfLines = 0
        tit.text = title
        tit.font = .systemFont(ofSize: 16, weight: .semibold)
        tit.textColor = .darkSystemGray4
        
        let ans = UILabel()
        ans.numberOfLines = 0
        ans.text = text
        ans.font = .systemFont(ofSize: 17)
        ans.textColor = .lightSystemGray
        
        let under = UIView()
        under.backgroundColor = .active
        under.heightAnchor.constraint(equalToConstant: 0.5).isActive = true
        
        let underStack = UIStackView(arrangedSubviews: [ans, under])
        underStack.axis = .vertical
        underStack.spacing = 8
        
        let stack = UIStackView(arrangedSubviews: [tit, underStack])
        stack.axis = .vertical
        stack.spacing = 16
        
        rootStackView.addArrangedSubview(stack)
    }
    
    private func createTextField(title: String? = nil, placeholder: String = "Введите ответ") {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 12
        if let title = title {
            let label = UILabel()
            label.numberOfLines = 0
            label.text = title
            label.font = .systemFont(ofSize: 16, weight: .semibold)
            label.textColor = .darkSystemGray4
            stack.addArrangedSubview(label)
        }
        let textField = CustomTextField()
        textField.attributedPlaceholder = NSAttributedString(string: placeholder, attributes: [.font: UIFont.systemFont(ofSize: 17), .foregroundColor: UIColor.lightSystemGray4])
        textField.textColor = .darkSystemGray5
        textField.font = .systemFont(ofSize: 17)
        textField.heightAnchor.constraint(equalToConstant: 44).isActive = true
        textField.addTarget(self, action: #selector(updateNextButton), for: .editingChanged)
        stack.addArrangedSubview(textField)
        textfields.append(textField)
        
        rootStackView.addArrangedSubview(stack)
    }
    
    private func createLabel(text: String) {
        let tit = UILabel()
        tit.numberOfLines = 0
        tit.text = text
        tit.font = .systemFont(ofSize: 16, weight: .semibold)
        tit.textColor = .darkSystemGray4
        
        rootStackView.addArrangedSubview(tit)
    }
    
    private func createSlider(text: String) {
        let view = SliderView()
        view.text = text
        
        rootStackView.addArrangedSubview(view)
    }
    
    private func createEmoView() {
        emoview = EmoView()
        emoview?.onUpdateState = { [weak self] in
            self?.updateNextButton()
        }
        
        rootStackView.addArrangedSubview(emoview!)
    }
    
    @objc private func updateNextButton() {
        button.isEnabled = !textfields.contains(where: { $0.text?.isEmpty ?? true }) && !(emoview?.enabledEmotions.isEmpty ?? false)
    }

}
