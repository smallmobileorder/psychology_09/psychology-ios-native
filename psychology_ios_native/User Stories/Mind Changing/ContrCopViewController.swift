//
//  ContrCopViewController.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 05/04/2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit
import RealmSwift

class ContrCopViewController: UIViewController {

    @IBOutlet private var tableView: AdvancedTableView!
    
    var onGo: ObjectCallback<ThoughtRealm>?
    var onAdd: Callback?
    
    var thoughts: [ThoughtRealm] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.registerNib(cell: ShadowCellTableViewCell.self)
        tableView.dataSource = self
        tableView.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let realm = try! Realm()
        thoughts = Array(realm.objects(ThoughtRealm.self))
        tableView.reloadData()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.sizeToFitTableHeaderView()
    }
    
    @IBAction private func next() {
        let selected = tableView.indexPathForSelectedRow!
        onGo?(thoughts[selected.row])
    }
    
    @IBAction private func add() {
        onAdd?()
    }

}

extension ContrCopViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        thoughts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(ShadowCellTableViewCell.self)!
        cell.set(title: thoughts[indexPath.row].thought, perc: thoughts[indexPath.row].percent)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let thought = thoughts[indexPath.row]
        onGo?(thought)
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let record = thoughts.remove(at: indexPath.row)
            let realm = try! Realm()
            try! realm.write {
                realm.delete(record)
            }
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        true
    }
}
