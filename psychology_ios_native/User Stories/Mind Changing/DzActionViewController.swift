//
//  DzActionViewController.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 22.02.2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit
import RealmSwift

class DzActionViewController: UIViewController {
    
    var onContinue: ObjectCallback<CatastrhHomeWork>?
    var onSelect: ObjectCallback<MindError>?
    var onDiary: Callback?
    
    @IBOutlet private var buttons: [UIButton]!
    @IBOutlet private var continueButton: UIButton!
    @IBOutlet private var goButton: UIButton!
    
    private lazy var catastrophicDz: CatastrhHomeWork? = {
        (try! Realm()).objects(CatastrhHomeWork.self).first
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        continueButton.isHidden = true

        for button in buttons {
            button.addTarget(self, action: #selector(selectt), for: .touchUpInside)
        }
    }
    
    @objc private func selectt(_ sender: UIButton) {
        goButton.isEnabled = true
        sender.isSelected = true
        for button in buttons where button != sender {
            button.isSelected = false
        }
        continueButton.isHidden = sender != buttons.first || catastrophicDz == nil
    }
    
    @IBAction private func catastrophicContinue() {
        if let dz = catastrophicDz {
            onContinue?(dz)
        }
    }
    
    @IBAction private func go() {
        let index = buttons.firstIndex(where: { $0.isSelected })!
        onSelect?(MindError.allCases[index])
    }

    @IBAction private func goDiary() {
        onDiary?()
    }
}
