//
//  ContrTableViewCell.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 29/03/2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

class ContrTableViewCell: UITableViewCell {
    
    @IBOutlet private var contrLabel: UILabel!
    @IBOutlet private var percentLabel: UILabel!
    
    func set(contr: ContrRealm) {
        contrLabel.text = contr.contr
        percentLabel.text = "\(contr.percent)%"
    }
    
}
