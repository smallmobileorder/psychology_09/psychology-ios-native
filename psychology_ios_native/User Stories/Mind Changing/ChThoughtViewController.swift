//
//  ChThoughtViewController.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 29/03/2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit
import RealmSwift

class ThoughtRealm: Object {
    @objc dynamic var thought: String = ""
    @objc dynamic var percent = 0
    let contrs = List<ContrRealm>()
}

class ContrRealm: Object {
    @objc dynamic var contr: String = ""
    @objc dynamic var percent = 0
}

class ChThoughtViewController: UIViewController {
    
    @IBOutlet private var tableView: AdvancedTableView!
    @IBOutlet private var button: UIButton!
    
    var onGo: ObjectCallback<String>?
    
    var thoughts: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.registerNib(cell: SingleSelectionTableViewCell.self)
        tableView.dataSource = self
        tableView.delegate = self
        tableView.contentInset = .only(bottom: 52 + 16 + 16)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.sizeToFitTableHeaderView()
    }
    
    @IBAction private func next() {
        let selected = tableView.indexPathForSelectedRow!
        onGo?(thoughts[selected.row])
    }

}

extension ChThoughtViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        thoughts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(SingleSelectionTableViewCell.self)!
        
        cell.type = .radioFlat
        cell.configure(with: thoughts[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        button.isEnabled = true
    }
    
}
