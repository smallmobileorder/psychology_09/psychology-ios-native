//
//  CalendarViewController.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 30/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit
import RealmSwift
import FSCalendar

class CalendarViewController: UIViewController, CalendarAssemblable {
    
    var onFinish: OptionalObjectCallback<Date>?

	var presenter: CalendarPresenterInput?
    
    private let maxDate = Date()
    private var records: [Record] = []
    
    @IBOutlet private weak var calendarView: FSCalendar!
    @IBOutlet private weak var containerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let realm = try! Realm()
        records = realm.objects(Record.self).sorted(\.date)
        
        modalTransitionStyle = .crossDissolve
        
        calendarView.appearance.headerTitleFont = .systemFont(ofSize: 16, weight: .semibold)
        calendarView.appearance.weekdayFont = .systemFont(ofSize: 16, weight: .semibold)
        calendarView.appearance.titleFont = .systemFont(ofSize: 11, weight: .semibold)
        
        calendarView.appearance.eventSelectionColor = .active
        calendarView.appearance.headerTitleColor = .active
        calendarView.appearance.selectionColor = .active
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(hide))
        containerView.addGestureRecognizer(gestureRecognizer)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DispatchQueue.main.async {
            self.fixMonthName()
        }
    }

}

extension CalendarViewController: FSCalendarDataSource {
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        return Date(timeIntervalSince1970: 0)
    }
    
    func maximumDate(for calendar: FSCalendar) -> Date {
        return Date()
    }
    
}

extension CalendarViewController: FSCalendarDelegate, FSCalendarDelegateAppearance {
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        fixMonthName()
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        dismiss(animated: true)
        onFinish?(date)
    }
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
        records.filter {
            Calendar.current.isDate($0.date, equalTo: date, toGranularity: .day)
            &&
            Calendar.current.isDate($0.date, equalTo: date, toGranularity: .month)
            &&
            Calendar.current.isDate($0.date, equalTo: date, toGranularity: .year)
        }
        .count
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, eventDefaultColorsFor date: Date) -> [UIColor]? {
        [UIColor.active]
    }
}

private extension CalendarViewController {

    @objc func hide() {
        dismiss(animated: true)
        onFinish?(nil)
    }
    
    func fixMonthName() {
        calendarView.calendarHeaderView.collectionView.visibleCells.forEach { cell in
            let cell = (cell as! FSCalendarHeaderCell)
            cell.titleLabel.text = cell.titleLabel.text?.capitalized
        }
    }

}
