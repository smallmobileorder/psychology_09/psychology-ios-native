//
//  CalendarViewProtocol.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 30/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol CalendarViewProtocol: BaseViewProtocol {
    var onFinish: OptionalObjectCallback<Date>? { get set }
}

