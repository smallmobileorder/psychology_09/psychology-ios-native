//
//  CalendarCoordinatorOutput.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 30/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

protocol CalendarCoordinatorOutput: class {
    var finishFlow: OptionalObjectCallback<Date>? { get set }
}
