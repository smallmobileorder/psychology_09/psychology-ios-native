//
//  CalendarCoordinator.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 30/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

final class CalendarCoordinator: BaseCoordinator, CalendarCoordinatorOutput {
    
    var finishFlow: OptionalObjectCallback<Date>?
    
    private let factory: CalendarFactoryProtocol
    private let router: Routable
    
    init(with factory: CalendarFactoryProtocol, router: Routable) {
        self.factory = factory
        self.router = router
    }
    
}

// MARK:- Coordinatable

extension CalendarCoordinator: Coordinatable {
    
    func start() {
        performFlow()
    }
    
}

private extension CalendarCoordinator {
    
    func performFlow() {
        let view = factory.makeCalendarView()
        view.onFinish = finishFlow
        router.present(view)
    }
    
}
