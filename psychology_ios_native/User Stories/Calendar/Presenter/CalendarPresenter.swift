//
//  CalendarPresenter.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 30/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol CalendarPresenterInput: class {

}

protocol CalendarPresenterOutput: class {
    var presenter: CalendarPresenterInput? { get set }
}

final class CalendarPresenter {

    weak var output: CalendarPresenterOutput?
    
}

// MARK:- CalendarPresenterInput
extension CalendarPresenter: CalendarPresenterInput {

}
