//
//  CalendarAssembly.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 30/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol CalendarAssemblable: CalendarViewProtocol, CalendarPresenterOutput {}

final class CalendarAssembly {
    static func assembly(with output: CalendarPresenterOutput) {
        let presenter  = CalendarPresenter()
    
        presenter.output     = output
        output.presenter     = presenter
    }
}
