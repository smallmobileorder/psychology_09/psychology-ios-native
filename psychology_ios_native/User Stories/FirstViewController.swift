//
//  FirstViewController.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 13/04/2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
    
    var completion: Callback?
    
    override func loadView() {
        super.loadView()
        
        view = BackgroundView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        (view as! BackgroundView).image = UIImage(named: "smoke")
        
        showAlert(.message(
            "Добро пожаловать",
            "Рада, что вы присоединились к числу людей, работающих над собой. Это приложение позволит вам самостоятельно обнаружить и проработать убеждения, ухудшающие ваше настроение и здоровье, либо может стать дополнительным помощником в работе с психологом (психотерапевтом). Если вам потребуется моя помощь, то вы всегда можете оставить заявку на консультацию. Приступим.",
            "Приступить")
        ) { [weak self] in
            self?.completion?()
        }
    }

}
