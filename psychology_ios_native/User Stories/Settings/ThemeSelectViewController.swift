//
//  ThemeSelectViewController.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 03.02.2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

class ThemeCell: UITableViewCell {
    override func setSelected(_ selected: Bool, animated: Bool) {
        (contentView.subviews.first(where: { $0 is UIImageView }) as? UIImageView)?.image = UIImage(named: "radio_flat_\(selected ? "" : "un")marked")
    }
}

public extension UIColor {
    /// Constructing color from hex string
    ///
    /// - Parameter hex: A hex string, can either contain # or not
    convenience init(hex string: String) {
        var hex = string.hasPrefix("#")
            ? String(string.dropFirst())
            : string
        guard hex.count == 3 || hex.count == 6
            else {
                self.init(white: 1.0, alpha: 0.0)
                return
        }
        if hex.count == 3 {
            for (index, char) in hex.enumerated() {
                hex.insert(char, at: hex.index(hex.startIndex, offsetBy: index * 2))
            }
        }
        
        self.init(
            red:   CGFloat((Int(hex, radix: 16)! >> 16) & 0xFF) / 255.0,
            green: CGFloat((Int(hex, radix: 16)! >> 8) & 0xFF) / 255.0,
            blue:  CGFloat((Int(hex, radix: 16)!) & 0xFF) / 255.0, alpha: 1.0)
    }
}


class ThemeSelectViewController: UITableViewController {
    
    private let colors: [UIColor] = [
        .init(hex: "BF5AF2"),
        .init(hex: "32D74B"),
        .init(hex: "64D2FF"),
        .init(hex: "0A84FF"),
        .init(hex: "FFD60A"),
        .init(hex: "FF9F0A"),
        .init(hex: "FF453A")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundView = BackgroundView(frame: .zero)

//        let button = BrandButton()
//        button.setTitle("Сохранить", for: .normal)
//        button.translatesAutoresizingMaskIntoConstraints = false
//        tableView.addSubview(button)
//        NSLayoutConstraint.activate([
//            button.leadingAnchor.constraint(equalTo: tableView.leadingAnchor, constant: 16),
//            tableView.trailingAnchor.constraint(equalTo: button.trailingAnchor, constant: 16),
//            tableView.layoutMarginsGuide.bottomAnchor.constraint(equalTo: button.bottomAnchor, constant: 16)
//        ])
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        UIColor.active = colors[indexPath.row]
        (UIApplication.shared.delegate as? AppDelegate)?.start()
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        for case let imageView as UIImageView in cell.contentView.subviews {
            imageView.tintColor = colors[indexPath.row]
        }
    }

}
