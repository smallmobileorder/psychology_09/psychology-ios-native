//
//  BackSelectViewController.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 22/03/2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

class BackSelectViewController: UIViewController {
    
    @IBOutlet private var collectionView: UICollectionView!
    
    let backs = ["", "beach", "mountains", "smoke"]
    
    private var selected = "" {
        didSet {
            let indexS = backs.firstIndex(of: selected)!
            if let indices = collectionView.indexPathsForSelectedItems {
                for index in indices where index.row != indexS {
                    collectionView.deselectItem(at: index, animated: true)
                }
            }
            collectionView.selectItem(at: IndexPath(row: indexS, section: 0), animated: true, scrollPosition: .centeredHorizontally)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let index = backs.firstIndex(of: UserDefaults.standard.string(forKey: "background_image") ?? "")!
        
        collectionView.dataSource = self
        collectionView.delegate = self
        
        selected = UserDefaults.standard.string(forKey: "background_image") ?? ""
        
//        collectionView.selectItem(at: IndexPath(row: index, section: 0), animated: false, scrollPosition: .centeredHorizontally)
    }
    
    @IBAction private func save() {
        UserDefaults.standard.set(selected, forKey: "background_image")
        NotificationCenter.default.post(name: .backgroundChange, object: nil)
        navigationController?.popViewController(animated: true)
    }
    
}

extension BackSelectViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        backs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "back", for: indexPath)
        (cell.contentView.viewWithTag(2) as? UIImageView)?.image = UIImage(named: backs[indexPath.row])
        cell.contentView.viewWithTag(1)?.isHidden = backs[indexPath.row] != selected
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        selected = backs[indexPath.row]
        
        (view as! BackgroundView).image = UIImage(named: selected)
        
        collectionView.cellForItem(at: indexPath)?.contentView.viewWithTag(1)?.isHidden = false
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        collectionView.cellForItem(at: indexPath)?.contentView.viewWithTag(1)?.isHidden = true
    }
}
