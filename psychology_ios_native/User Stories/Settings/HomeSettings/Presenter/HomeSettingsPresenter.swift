//
//  HomeSettingsPresenter.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 12.01.2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

protocol HomeSettingsPresenterInput: class {

}

protocol HomeSettingsPresenterOutput: class {
    var presenter: HomeSettingsPresenterInput? { get set }
}

final class HomeSettingsPresenter {

    weak var output: HomeSettingsPresenterOutput?
    
}

// MARK:- HomeSettingsPresenterInput
extension HomeSettingsPresenter: HomeSettingsPresenterInput {

}
