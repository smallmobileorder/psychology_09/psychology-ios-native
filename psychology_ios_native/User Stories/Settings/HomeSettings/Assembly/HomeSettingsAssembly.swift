//
//  HomeSettingsAssembly.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 12.01.2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

protocol HomeSettingsAssemblable: HomeSettingsViewProtocol, HomeSettingsPresenterOutput {}

final class HomeSettingsAssembly {
    static func assembly(with output: HomeSettingsPresenterOutput) {
        let presenter  = HomeSettingsPresenter()
    
        presenter.output     = output
        output.presenter     = presenter
    }
}
