//
//  HomeSettingsViewController.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 12.01.2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit
import StoreKit

class HomeSettingsViewController: UITableViewController, HomeSettingsAssemblable {

	var presenter: HomeSettingsPresenterInput?

	override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.backgroundView = BackgroundView(frame: .zero)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.row == 0 {
            if let url = URL(string: "itms-apps://itunes.apple.com/app/id1504010817"),
                UIApplication.shared.canOpenURL(url) {
                if #available(iOS 10.0, *) {
                    UIApplication.shared.open(url, options: [:], completionHandler: nil)
                } else {
                    UIApplication.shared.openURL(url)
                }
            }
        }
    }

}
