//
//  HomeCoordinator.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

final class HomeCoordinator: BaseCoordinator, HomeCoordinatorOutput {
    
    var finishFlow: Callback?
    
    private let factory: HomeFactoryProtocol
    private let coordinatorFactory: CoordinatorFactoryProtocol
    private let router: Routable
    
    init(with factory: HomeFactoryProtocol, coordinatorFactory: CoordinatorFactoryProtocol, router: Routable) {
        self.factory = factory
        self.coordinatorFactory = coordinatorFactory
        self.router = router
    }
    
}

// MARK:- Coordinatable

extension HomeCoordinator: Coordinatable {
    
    func start() {
        performFlow()
    }
    
}

// MARK:- Private methods

private extension HomeCoordinator {
    
    func performFlow() {
        let view = factory.makeHomeView()
        router.setRootModule(view, hideBar: true)
        
        startExperiments(router: view)
        startTests(router: view)
        startDiary(router: view)
        startMind(router: view)
        startWrite(router: view)
    }
    
    func startExperiments(router: Routable) {
        let experimentsCoordinator = coordinatorFactory.makeExperimentsCoordinator(
            router: router,
            coordinatorFactory: coordinatorFactory)
        experimentsCoordinator.finishFlow = { [weak self, weak experimentsCoordinator] in
            self?.removeDependency(experimentsCoordinator)
        }
        experimentsCoordinator.onOpenDiary = {
            let tabVC = router as! UITabBarController
            tabVC.selectedIndex = 2
        }
        addDependency(experimentsCoordinator)
        experimentsCoordinator.start()
    }
    
    func startTests(router: Routable) {
        let testsCoordinator = coordinatorFactory.makeTestsCoordinator(
            router: router,
            coordinatorFactory: coordinatorFactory)
        testsCoordinator.finishFlow = { [weak self] in
            self?.removeDependency(testsCoordinator)
        }
        testsCoordinator.onOpenDiary = {
            let tabVC = router as! UITabBarController
            tabVC.selectedIndex = 2
        }
        addDependency(testsCoordinator)
        testsCoordinator.start()
    }
    
    func startDiary(router: Routable) {
        let diaryCoordinator = coordinatorFactory.makeDiaryCoordinator(
            router: router,
            coordinatorFactory: coordinatorFactory)
        diaryCoordinator.finishFlow = { [weak self] in
            self?.removeDependency(diaryCoordinator)
        }
        diaryCoordinator.onMindChanging = { [weak self] record in
            let tabVC = router as! UITabBarController
            tabVC.selectedIndex = 3
            if let mind = self?.childCoordinators.first(where: { $0 is MindCoordinator }) as? MindCoordinator {
                mind.start(record: record)
            }
        }
        diaryCoordinator.onMindStart = { [weak self] record in
            let tabVC = router as! UITabBarController
            tabVC.selectedIndex = 3
            if let mind = self?.childCoordinators.first(where: { $0 is MindCoordinator }) as? MindCoordinator {
                mind.startWork(record: record)
            }
        }
        addDependency(diaryCoordinator)
        diaryCoordinator.start()
    }
    
    func startMind(router: Routable) {
        let mindCoordinator = MindCoordinator(router: router)
        mindCoordinator.finishFlow = { [weak self] in
            self?.removeDependency(mindCoordinator)
        }
        mindCoordinator.onDiary = {
            let tabVC = router as! UITabBarController
            tabVC.selectedIndex = 2
        }
        mindCoordinator.onAddRecord = { [weak self] in
            guard let self = self else { return }
            
            if let diaryCoordinator = self.childCoordinators.first(where: { $0 is DiaryCoordinator }) as? DiaryCoordinator {
                let tabVC = router as! UITabBarController
                tabVC.selectedIndex = 2
                diaryCoordinator.addRecord()
            }
        }
        mindCoordinator.editRecord = { [weak self] record in
            guard let self = self else { return }
            
            if let diaryCoordinator = self.childCoordinators.first(where: { $0 is DiaryCoordinator }) as? DiaryCoordinator {
                let tabVC = router as! UITabBarController
                tabVC.selectedIndex = 2
                diaryCoordinator.editRecord(record: record)
            }
        }
        addDependency(mindCoordinator)
        mindCoordinator.start()
    }
    
    func startWrite(router: Routable) {
        router.push(ModulesFactory().makeRouter(root: ConsultViewController.instanceController(storyboard: .consult), hideBar: false))
    }
    
}
