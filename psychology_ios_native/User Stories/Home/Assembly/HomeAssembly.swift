//
//  HomeAssembly.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 16/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol HomeAssemblable: HomeViewProtocol, HomePresenterOutput {}

final class HomeAssembly {
    static func assembly(with output: HomePresenterOutput) {
        let presenter  = HomePresenter()
    
        presenter.output     = output
        output.presenter     = presenter
    }
}
