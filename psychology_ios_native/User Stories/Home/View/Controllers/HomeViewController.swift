//
//  HomeViewController.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 16/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class HomeViewController: UITabBarController, HomeAssemblable {
    func pushReplacing(_ module: Presentable?, animated: Bool, completion: Callback?) {
        
    }
    
    
	var presenter: HomePresenterInput?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
    }

}

extension HomeViewController {
    
    var rootModule: Presentable? {
        return selectedViewController
    }
    
    var modules: [Presentable]? {
        return viewControllers
    }
    
    func push(_ module: Presentable?, animated: Bool, completion: Callback?) {
        guard let vc = module?.toPresent else { return }
        
        let _ = vc.view
        
        var controllers = viewControllers ?? []
        controllers.append(vc)
        viewControllers = controllers
        
//        if viewControllers?.count == 3 {
//            for (image, title) in zip([#imageLiteral(resourceName: "changes"), #imageLiteral(resourceName: "settings")], ["Изменения", "Настройки"]) {
//                let vc = UIViewController()
//                vc.tabBarItem.image = image
//                vc.tabBarItem.title = title
//                viewControllers?.append(vc)
//                vc.view.backgroundColor = .white
//                let placeholder = UILabel()
//                placeholder.textAlignment = .center
//                placeholder.text = "Раздел в разработке"
//                vc.view.addSubview(placeholder)
//                placeholder.frame = vc.view.bounds
//                placeholder.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//            }
//        }
    }
    
    func present(_ module: Presentable?, animated: Bool) {
        guard let vc = module?.toPresent else { return }
        
        present(vc, animated: animated)
    }
    
    func popToRootModule(animated: Bool) {
        selectedViewController = viewControllers?.first
    }
    
    func popModule(animated: Bool) {
        selectedViewController = viewControllers?.first
    }
    
    func popToModule(_ module: Presentable?, animated: Bool) {
        selectedViewController = module?.toPresent
    }
    
    func setRootModule(_ module: Presentable?, hideBar: Bool) {
        selectedViewController = module?.toPresent
    }
    
    func dismissModule(animated: Bool, completion: Callback?) {
        selectedViewController = viewControllers?.first
    }
    
}
