//
//  HomeViewProtocol.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 16/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol HomeViewProtocol: BaseViewProtocol, Routable { }

