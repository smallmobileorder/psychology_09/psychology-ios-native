//
//  MainCoordinatorOutput.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

protocol HomeCoordinatorOutput: class {
    var finishFlow: Callback? { get set }
}
