//
//  HomePresenter.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 16/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol HomePresenterInput: class {

}

protocol HomePresenterOutput: class {
    var presenter: HomePresenterInput? { get set }
}

final class HomePresenter {

    weak var output: HomePresenterOutput?
    
}

// MARK:- HomePresenterInput
extension HomePresenter: HomePresenterInput {

}
