//
//  TestResultRealm.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 16/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation
import RealmSwift

class TestResultRealm: Object {
    @objc dynamic let date = Date()
    @objc dynamic var test: String? = nil
    let results = List<ResultRealmDescription>()
}

class ResultRealmDescription: Object {
    @objc dynamic var title: String = ""
    @objc dynamic var advice: String = ""
}
