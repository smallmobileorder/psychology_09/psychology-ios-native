//
//  TestConclusionPresenter.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 15/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol TestConclusionPresenterInput: class {

}

protocol TestConclusionPresenterOutput: class {
    var presenter: TestConclusionPresenterInput? { get set }
}

final class TestConclusionPresenter {

    weak var output: TestConclusionPresenterOutput?
    
}

// MARK:- TestConclusionPresenterInput
extension TestConclusionPresenter: TestConclusionPresenterInput {

}
