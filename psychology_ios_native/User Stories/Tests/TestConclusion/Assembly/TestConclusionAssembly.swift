//
//  TestConclusionAssembly.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 15/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol TestConclusionAssemblable: TestConclusionViewProtocol, TestConclusionPresenterOutput {}

final class TestConclusionAssembly {
    static func assembly(with output: TestConclusionPresenterOutput) {
        let presenter  = TestConclusionPresenter()
    
        presenter.output     = output
        output.presenter     = presenter
    }
}
