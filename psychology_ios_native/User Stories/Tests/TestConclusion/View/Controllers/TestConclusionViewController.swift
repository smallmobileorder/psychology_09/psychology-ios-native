//
//  TestConclusionViewController.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 15/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit
import RealmSwift

class TestConclusionViewController: UIViewController, TestConclusionAssemblable {

    var result: TestResult?
    var onNextTest: Callback?
    var onSelectTest: Callback?
    var onOpenDiary: Callback?
    
	var presenter: TestConclusionPresenterInput?
    
    @IBOutlet private weak var tableView: AdvancedTableView!
    @IBOutlet private weak var nextTestButton: UIButton!
    @IBOutlet private weak var selectTestButton: UIButton!
    @IBOutlet private weak var openDiaryButton: UIButton!

	override func viewDidLoad() {
        super.viewDidLoad()
        
        if let result = result {
            let resultRecord = TestResultRealm()
            resultRecord.test = result.test.title
            resultRecord.results.append(objectsIn: result.results.map { result in
                let resultRecord = ResultRealmDescription()
                resultRecord.title = result.title
                resultRecord.advice = result.advice
                return resultRecord
            })
            let realm = try! Realm()
            try! realm.write {
                realm.add(resultRecord)
            }
        }
        
        setup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.sizeToFitTableHeaderView()
        tableView.sizeToFitTableFooterView()
    }

}

private extension TestConclusionViewController {
    
    @IBAction func openNext(_ sender: UIButton) {
        switch sender {
        case nextTestButton: onNextTest?()
        case selectTestButton: onSelectTest?()
        case openDiaryButton: onOpenDiary?()
        default: break
        }
    }
    
}

extension TestConclusionViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return result?.results.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(TestConclusionTableViewCell.self)!
        
        let item = result?.results[indexPath.row]
        cell.configure(with: item)
        
        return cell
    }
    
}

private extension TestConclusionViewController {
    
    func setup() {
        tableView.registerNib(cell: TestConclusionTableViewCell.self)
        
        nextTestButton.isHidden = onNextTest == nil
        selectTestButton.isHidden = onSelectTest == nil
        openDiaryButton.isHidden = onOpenDiary == nil
    }
    
}
