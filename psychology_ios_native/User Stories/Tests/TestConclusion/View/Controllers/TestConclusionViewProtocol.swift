//
//  TestConclusionViewProtocol.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 15/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol TestConclusionViewProtocol: BaseViewProtocol {
    var result: TestResult? { get set }
    var onNextTest: Callback? { get set }
    var onSelectTest: Callback? { get set }
    var onOpenDiary: Callback? { get set }
}

