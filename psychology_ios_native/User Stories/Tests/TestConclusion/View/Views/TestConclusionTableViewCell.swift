//
//  TestConclusionTableViewCell.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class TestConclusionTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var adviceLabel: UILabel!

    func configure(with result: ResultDescription?) {
        titleLabel.text = result?.title
        adviceLabel.text = result?.advice
    }
    
}
