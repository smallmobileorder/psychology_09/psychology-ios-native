//
//  TestQuestionPresenter.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 14/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol TestQuestionPresenterInput: class {

}

protocol TestQuestionPresenterOutput: class {
    var presenter: TestQuestionPresenterInput? { get set }
}

final class TestQuestionPresenter {

    weak var output: TestQuestionPresenterOutput?
    
}

// MARK:- TestQuestionPresenterInput
extension TestQuestionPresenter: TestQuestionPresenterInput {

}
