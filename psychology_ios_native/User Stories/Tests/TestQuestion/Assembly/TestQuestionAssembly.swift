//
//  TestQuestionAssembly.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 14/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol TestQuestionAssemblable: TestQuestionViewProtocol, TestQuestionPresenterOutput {}

final class TestQuestionAssembly {
    static func assembly(with output: TestQuestionPresenterOutput) {
        let presenter  = TestQuestionPresenter()
    
        presenter.output     = output
        output.presenter     = presenter
    }
}
