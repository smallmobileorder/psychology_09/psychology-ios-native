//
//  SingleSelectionTableViewCell.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 14/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class SingleSelectionTableViewCell: UITableViewCell {
    
    enum SelectionType: String {
        case checkbox = "check"
        case radioButton = "radio"
        case radioFlat = "radio_flat"
    }
    
    var type: SelectionType = .checkbox {
        didSet {
            setSelected(isSelected, animated: false)
        }
    }
    
    @IBOutlet private weak var indicatorImageView: UIImageView!
    @IBOutlet private weak var selectedIndicatorImageView: UIImageView!
    @IBOutlet private weak var answerLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        selectedIndicatorImageView.image = UIImage(named: "\(type.rawValue)_marked")
        selectedIndicatorImageView.isHidden = !selected
        indicatorImageView.image = UIImage(named: "\(type.rawValue)_unmarked")
    }
    
    func configure(with answer: String?) {
        answerLabel.text = answer?.capitalizingFirstLetter()
    }
    
}
