//
//  TestQuestionViewProtocol.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 14/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol TestQuestionViewProtocol: BaseViewProtocol {
    var headerChild: Presentable? { get set }
    var onSelect: ObjectCallback<TestAnswer>? { get set }
    var onClose: Callback? { get set }
    var question: TestQuestion? { get set }
}

