//
//  TestQuestionViewController.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 14/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class TestQuestionViewController: UIViewController, TestQuestionAssemblable {
    
    var headerChild: Presentable? { didSet { headerChildVC = headerChild?.toPresent } }
    var onSelect: ObjectCallback<TestAnswer>?
    var onClose: Callback?
    var question: TestQuestion?

	var presenter: TestQuestionPresenterInput?
    
    private var headerChildVC: UIViewController?
    
    @IBOutlet private weak var tableView: AdvancedTableView!
    @IBOutlet private weak var headerContainerView: UIView!
    @IBOutlet private weak var questionLabel: UILabel!
    @IBOutlet private weak var continueButton: UIButton!

	override func viewDidLoad() {
        super.viewDidLoad()
        
        questionLabel.text = question?.question
        
        addChild(headerChildVC, container: headerContainerView)
        
        setup()
        
        addNavigationBackButton(selector: #selector(close))
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.sizeToFitTableHeaderView()
    }

}

private extension TestQuestionViewController {
    
    @IBAction func next() {
        guard let selectedIndexPath = tableView.indexPathForSelectedRow,
            let answer = question?.answers?[selectedIndexPath.row] else { return }
        
        onSelect?(answer)
    }
    
    @objc func close() {
        showAlertСontroller(
            title: "Вы действительно хотите покинуть тест?",
            actions: [
                (title: "Отмена", handler: {}),
                (title: "Да", handler: { [weak self] in self?.onClose?() })
            ]
        )
    }
    
}

extension TestQuestionViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return question?.answers?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(SingleSelectionTableViewCell.self)!
        
        let answer = question?.answers?[indexPath.row]
        cell.type = .radioButton
        cell.configure(with: answer?.answer)
        
        return cell
    }
    
}

extension TestQuestionViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        continueButton.isEnabled = true
    }
    
}

private extension TestQuestionViewController {
    
    func setup() {
        tableView.registerNib(cell: SingleSelectionTableViewCell.self)
    }
    
}
