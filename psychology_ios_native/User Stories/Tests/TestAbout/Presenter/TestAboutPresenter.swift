//
//  TestAboutPresenter.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 20/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol TestAboutPresenterInput: class {

}

protocol TestAboutPresenterOutput: class {
    var presenter: TestAboutPresenterInput? { get set }
}

final class TestAboutPresenter {

    weak var output: TestAboutPresenterOutput?
    
}

// MARK:- TestAboutPresenterInput
extension TestAboutPresenter: TestAboutPresenterInput {

}
