//
//  TestAboutViewProtocol.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 20/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol TestAboutViewProtocol: BaseViewProtocol {
    var onFinish: Callback? { get set }
}

