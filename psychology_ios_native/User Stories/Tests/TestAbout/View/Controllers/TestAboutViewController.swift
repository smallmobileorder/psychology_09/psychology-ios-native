//
//  TestAboutViewController.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 20/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit
import RealmSwift

enum SexEnum: String, CaseIterable {
    case MAN
    case WOMAN
}

enum MaritalStatusEnum: String, CaseIterable {
    case SINGLE
    case MARRIED
    case DIVORCED
    case WIDOWER
}

enum EducationEnum: String, CaseIterable {
    case PRIMARY
    case SECONDARY
    case SECONDARY_VOCATIONAL
    case HIGHER_VOCATIONAL
}

enum FrequencyOfUseEnum: String, CaseIterable {
    case EVERYDAY
    case EVERYWEEK
    case EVERYMONTH
    case LESS_OFTEN
}

enum FrequencyOfTherapyEnum: String, CaseIterable {
    case DONT_APPEAL
    case FEW_TIMES_A_WEEK
    case ONE_TIME_A_WEEK
    case FEW_TIMES_A_MONTH
    case LESS_OFTEN
}

enum MedicinesEnum: String, CaseIterable {
    case ANTIDEPRESSANTS
    case TRANQUILIZERS
    case ANTIPSYCHOTICS
    case NORMOTIMICS
}

enum Sex: Int {
    case man
    case woman
    
    var maritalStatusAnswers: [String] {
        switch self {
        case .man: return manMartial
        case .woman: return womanMartial
        }
    }
}

let manMartial = ["Не женат", "В браке", "В разводе", "Вдовец"]
let womanMartial = ["Не замужем", "В браке", "В разводе", "Вдова"]

let drugsAnswers = [
    "Нет",
    "Принимаю антидепрессанты",
    "Принимаю транквилизаторы",
    "Принимаю нейролептики",
    "Принимаю нормотимики",
    "Принимаю другие лекарства"
]

let educationAnswers = [
    "Начальное",
    "Среднее",
    "Среднее профессиональное",
    "Высшее профессиональное",
    "Другоe"
]

let applicationUsageAnswers = [
    "Ежедневно",
    "1 или несколько раз в неделю",
    "1 или несколько раз в месяц",
    "Реже"
]

let doctorFrequencyAnswers = [
    "Не обращаюсь",
    "Несколько раз в неделю",
    "1 раз в неделю",
    "2-3 раза в месяц",
    "Реже"
]

class TestAboutViewController: UIViewController, TestAboutAssemblable {
    
    var onFinish: Callback?

	var presenter: TestAboutPresenterInput?
    
    var sex: Sex? {
        didSet {
            if let selectedMartialStatusAnswer = maritalStatusTextField.text,
                let selectedIndex = (oldValue ?? .man).maritalStatusAnswers.index(of: selectedMartialStatusAnswer) {
                maritalStatusTextField.text = sex?.maritalStatusAnswers[selectedIndex]
            }
            
            manButton.isSelected = sex == .man
            womanButton.isSelected = sex == .woman
            
            updateSaveButton()
        }
    }
    
    var hasDoctor: Bool? {
        didSet {
            guard let hasDoctor = hasDoctor else { return }
            
            hasDoctorButton.isSelected = hasDoctor
            hasNotDoctorButton.isSelected = !hasDoctor
            setDoctorSection(hidden: !hasDoctor)
            
            updateSaveButton()
        }
    }
    
    var agreed: Bool = false {
        didSet {
            updateAgree()
        }
    }
    
    private func updateAgree() {
        agreeButton.isSelected = agreed
        disagreeButton.isSelected = !agreed
    }
    
    private let noAnswerIndexPath = IndexPath(row: 0, section: 0)
    
    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var tableView: UITableView!
    
    @IBOutlet private weak var emailTextField: UITextField!
    
    @IBOutlet private weak var manButton: LeveledButton!
    @IBOutlet private weak var womanButton: LeveledButton!
    
    @IBOutlet private weak var ageTextField: UITextField!
    @IBOutlet private weak var maritalStatusTextField: CustomPickerTextField!
    @IBOutlet private weak var educationTextField: CustomPickerTextField!
    @IBOutlet private weak var monthsOfUsingTextField: UITextField!
    @IBOutlet private weak var frequencyOfUsingTextField: CustomPickerTextField!
    @IBOutlet private weak var monthsWithDoctorTextField: UITextField!
    @IBOutlet private weak var frequencyWithDoctorTextField: CustomPickerTextField!
    
    @IBOutlet private weak var hasDoctorButton: LeveledButton!
    @IBOutlet private weak var hasNotDoctorButton: LeveledButton!
    @IBOutlet private weak var doctorSection: UIStackView!
    @IBOutlet private weak var additionalDrugsSection: UIStackView!
    @IBOutlet private weak var additionalDrugsTextField: UITextField!
    
    
    @IBOutlet private weak var agreeButton: LeveledButton!
    @IBOutlet private weak var disagreeButton: LeveledButton!
    
    @IBOutlet private weak var saveButton: UIButton!
    
	override func viewDidLoad() {
        super.viewDidLoad()
        
        updateAgree()
        
        UIView.performWithoutAnimation {
            hasDoctor = false
        }
        
        addNavigationBackButton(selector: #selector(close))
        
        if let info = UserInfoService().info {
            emailTextField.text = info.email
            sex = info.sex
            ageTextField.text = "\(info.age)"
            maritalStatusTextField.text = info.maritalStatus
            educationTextField.text = info.educationStatus
            monthsOfUsingTextField.text = "\(info.applicationUsageDuration)"
            frequencyOfUsingTextField.text = info.applicationUsageFrequency
            hasDoctor = info.doctorDuration > 0
            monthsWithDoctorTextField.text = "\(info.doctorDuration)"
            frequencyWithDoctorTextField.text = info.doctorFrequency
            agreed = info.canSend
            
            var selectedIndexPaths: [IndexPath] = []
            let drugs = info.drugs
            if drugs.isEmpty {
                selectedIndexPaths.append(noAnswerIndexPath)
            } else {
                var additionalDrugs: [String] = []
                for drug in drugs {
                    guard let index = drugsAnswers.index(of: drug) else {
                        additionalDrugs.append(drug)
                        continue
                    }
                    
                    selectedIndexPaths.append(IndexPath(row: index, section: 0))
                }
                additionalDrugsTextField.text = additionalDrugs.joined(separator: ", ")
                if !additionalDrugs.isEmpty {
                    selectedIndexPaths.append(IndexPath(row: 5, section: 0))
                    setAdditionalDrugsSection(hidden: false)
                }
            }
            selectedIndexPaths.forEach { tableView.selectRow(at: $0, animated: false, scrollPosition: .none) }
        }
        
        setup()
    }
    
}

private extension TestAboutViewController {
    
    @objc func close() {
        showAlertСontroller(
            title: "Вы действительно хотите выйти из теста \"Немного о вас\"?",
            actions: [
                (title: "Отмена", handler: {}),
                (title: "Да", handler: { [weak self] in self?.onFinish?() })
            ]
        )
    }
    
    @IBAction func changeSex(_ sender: UIButton) {
        sex = sender === manButton ? .man : .woman
    }
    
    @IBAction func setDoctor(_ sender: UIButton) {
        hasDoctor = sender === hasDoctorButton
    }
    
    @IBAction func setAgree(_ sender: UIButton) {
        agreed = sender === agreeButton
    }
    
    @IBAction func save() {
        let realm = try! Realm()
        let info = realm.objects(InfoAboutRealm.self).first ?? {
            let info = InfoAboutRealm()
            try! realm.write {
                realm.add(info)
            }
            return info
        }()
        try! realm.write {
            info.email = emailTextField.text ?? ""
            info.sex = sex!
            info.age = Int(ageTextField.text ?? "") ?? 0
            info.maritalStatus = maritalStatusTextField.text ?? ""
            info.educationStatus = educationTextField.text ?? ""
            info.applicationUsageDuration = Int(monthsOfUsingTextField.text ?? "") ?? 0
            info.applicationUsageFrequency = frequencyOfUsingTextField.text ?? ""
            if hasDoctor ?? false {
                info.doctorDuration = Int(monthsWithDoctorTextField.text ?? "") ?? 0
            }
            info.doctorFrequency = frequencyWithDoctorTextField.text ?? ""
            let selectedDrugsIndices = tableView.indexPathsForSelectedRows?.map(\.row) ?? []
            var drugs: [String] = []
            for index in selectedDrugsIndices where index != 0 {
                if index == drugsAnswers.count - 1, let additionalDrugs = additionalDrugsTextField.text {
                    drugs.append(additionalDrugs)
                } else {
                    drugs.append(drugsAnswers[index])
                }
            }
            info.drugs.append(objectsIn: drugs)
            info.canSend = agreeButton.isSelected
        }
        onFinish?()
    }
    
}

extension TestAboutViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return drugsAnswers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(SingleSelectionTableViewCell.self)!
        
        let answer = drugsAnswers[indexPath.row]
        cell.type = .checkbox
        cell.configure(with: answer)
        
        return cell
    }
    
}

extension TestAboutViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath == noAnswerIndexPath {
            for drugIndexPath in tableView.indexPathsForSelectedRows ?? [] where drugIndexPath != noAnswerIndexPath {
                tableView.deselectRow(at: drugIndexPath, animated: true)
                if drugIndexPath.row == drugsAnswers.count - 1 {
                    setAdditionalDrugsSection(hidden: true)
                }
            }
        } else {
            tableView.deselectRow(at: noAnswerIndexPath, animated: true)
            if indexPath.row == drugsAnswers.count - 1 {
                setAdditionalDrugsSection(hidden: false)
                scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentSize.height - scrollView.frame.height), animated: true)
            }
        }
        
        updateSaveButton()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if indexPath.row == drugsAnswers.count - 1 {
            setAdditionalDrugsSection(hidden: true)
        }
        
        updateSaveButton()
    }
    
}

extension TestAboutViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView {
        case maritalStatusTextField.pickerView: return (sex ?? .man).maritalStatusAnswers.count
        case educationTextField.pickerView: return educationAnswers.count
        case frequencyOfUsingTextField.pickerView: return applicationUsageAnswers.count
        case frequencyWithDoctorTextField.pickerView: return doctorFrequencyAnswers.count
        default: return 0
        }
    }
    
}

extension TestAboutViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView {
        case maritalStatusTextField.pickerView: return (sex ?? .man).maritalStatusAnswers[row]
        case educationTextField.pickerView: return educationAnswers[row]
        case frequencyOfUsingTextField.pickerView: return applicationUsageAnswers[row]
        case frequencyWithDoctorTextField.pickerView: return doctorFrequencyAnswers[row]
        default: return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView {
        case maritalStatusTextField.pickerView: maritalStatusTextField.text = (sex ?? .man).maritalStatusAnswers[row]
        case educationTextField.pickerView: educationTextField.text = educationAnswers[row]
        case frequencyOfUsingTextField.pickerView: frequencyOfUsingTextField.text = applicationUsageAnswers[row]
        case frequencyWithDoctorTextField.pickerView: frequencyWithDoctorTextField.text =  doctorFrequencyAnswers[row]
        default: break
        }
        
        updateSaveButton()
    }
    
}

private extension TestAboutViewController {
    
    func setDoctorSection(hidden: Bool) {
        UIView.animateKeyframes(withDuration: 0.3, delay: 0, options: [], animations: {
            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.3) {
                self.doctorSection.alpha = hidden ? 0 : 1
            }
            UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.7) {
                self.doctorSection.isHidden = hidden
            }
        })
    }
    
    func setAdditionalDrugsSection(hidden: Bool) {
        UIView.animate(withDuration: 0.3) {
            self.additionalDrugsSection.isHidden = hidden
        }
    }
    
    func updateSaveButton() {
        let hasEmail = !(emailTextField.text?.isEmpty ?? true)
        let hasSex = sex != nil
        let hasAge = !(ageTextField.text?.isEmpty ?? true)
        let appUsageMonths = !(monthsOfUsingTextField.text?.isEmpty ?? true)
        let doctorFilled =
            !(hasDoctor ?? false)
            ||
            !(monthsWithDoctorTextField.text?.isEmpty ?? true)
        let hasSelectedDrugs =
            !(tableView.indexPathsForSelectedRows?.isEmpty ?? true)
            &&
            (
                !(tableView.indexPathsForSelectedRows?.contains(where: { $0.row == drugsAnswers.count - 1 }) ?? false)
                ||
                !(additionalDrugsTextField.text?.isEmpty ?? true)
            )
        
        saveButton.isEnabled = hasEmail && hasSex && hasAge && appUsageMonths && doctorFilled && hasSelectedDrugs
    }
    
}

private extension TestAboutViewController {
    
    func setup() {
        tableView.registerNib(cell: SingleSelectionTableViewCell.self)
        
        for pickerTextField in [
            maritalStatusTextField,
            educationTextField,
            frequencyOfUsingTextField,
            frequencyWithDoctorTextField] {
                pickerTextField?.pickerDelegate = self
                pickerTextField?.pickerDataSource = self
        }
        
        for textField in [
            emailTextField,
            ageTextField,
            monthsOfUsingTextField,
            monthsWithDoctorTextField,
            additionalDrugsTextField] {
                textField?.addTarget(self, action: #selector(textFieldUpdated(_:)), for: .editingChanged)
        }
        
        maritalStatusTextField.text = (sex ?? .man).maritalStatusAnswers.first
        educationTextField.text = educationAnswers.first
        frequencyOfUsingTextField.text = applicationUsageAnswers.first
        frequencyWithDoctorTextField.text =  doctorFrequencyAnswers.first
        
        updateSaveButton()
    }
    
    @objc func textFieldUpdated(_ textField: UITextField) {
        let text = textField.text ?? ""
        let limitedText: String
        switch textField {
        case ageTextField: limitedText = String(text.prefix(3))
        case monthsOfUsingTextField, monthsWithDoctorTextField: limitedText = String(text.prefix(2))
        case additionalDrugsTextField: limitedText = String(text.prefix(100))
        default: limitedText = text
        }
        textField.text = limitedText
        
        updateSaveButton()
    }
    
}
