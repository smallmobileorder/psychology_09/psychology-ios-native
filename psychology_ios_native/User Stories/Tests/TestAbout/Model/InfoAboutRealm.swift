//
//  InfoAboutRealm.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 21/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation
import RealmSwift

class InfoAboutRealm: Object {
    @objc private dynamic var sexRawValue = 0
    @objc dynamic var email = ""
    @objc dynamic var age = 0
    @objc dynamic var canSend = false
    @objc dynamic var maritalStatus = ""
    @objc dynamic var educationStatus = ""
    @objc dynamic var applicationUsageDuration = 0
    @objc dynamic var applicationUsageFrequency = ""
    @objc dynamic var doctorDuration = -1
    @objc dynamic var doctorFrequency = ""
    let drugs = List<String>()
}

extension InfoAboutRealm {
    var sex: Sex {
        get { return Sex(rawValue: sexRawValue)! }
        set { sexRawValue = newValue.rawValue }
    }
}
