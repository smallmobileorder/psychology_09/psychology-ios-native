//
//  TestAboutAssembly.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 20/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol TestAboutAssemblable: TestAboutViewProtocol, TestAboutPresenterOutput {}

final class TestAboutAssembly {
    static func assembly(with output: TestAboutPresenterOutput) {
        let presenter  = TestAboutPresenter()
    
        presenter.output     = output
        output.presenter     = presenter
    }
}
