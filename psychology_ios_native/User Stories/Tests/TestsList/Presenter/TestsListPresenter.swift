//
//  TestsListPresenter.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 16/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import RealmSwift

protocol TestsListPresenterInput: class {
    func fetchTests()
}

protocol TestsListPresenterOutput: PresenterOutputProtocol {
    var presenter: TestsListPresenterInput? { get set }
    
    func update(with tests: [Test])
}

final class TestsListPresenter {

    private let client = Client()
    
    weak var output: TestsListPresenterOutput?
    
}

// MARK:- TestsListPresenterInput
extension TestsListPresenter: TestsListPresenterInput {

    func fetchTests() {
        output?.startLoading()
        
        client.request(API.tests) { [weak self] result in
            switch result {
            case .success(let tests):
                var filledTests: [Test] = []
                for test in tests {
                    var test = test
                    if test.title == "Шкала дисфункциональных отношений Вейсмана – Бека" {
                        let answers = [
                            TestAnswer(answer: "полностью согласен", weight: 1),
                            TestAnswer(answer: "в основном согласен", weight: 2),
                            TestAnswer(answer: "скорее согласен", weight: 3),
                            TestAnswer(answer: "трудно определить", weight: 4),
                            TestAnswer(answer: "скорее не согласен", weight: 5),
                            TestAnswer(answer: "в основном не согласен", weight: 6),
                            TestAnswer(answer: "полностью не согласен", weight: 7)
                        ]
                        var questions: [TestQuestion] = []
                        for question in test.questions {
                            questions.append(TestQuestion(question: question.question, answers: answers))
                        }
                        test = Test(title: test.title, description: test.description, intro: test.intro, questions: questions)
                    } else if test.title == "Тест Эллиса" {
                        let answers = [
                            TestAnswer(answer: "Полностью согласен", weight: 1),
                            TestAnswer(answer: "В основном согласен", weight: 2),
                            TestAnswer(answer: "Слегка согласен", weight: 3),
                            TestAnswer(answer: "Слегка не согласен", weight: 4),
                            TestAnswer(answer: "В основном не согласен", weight: 5),
                            TestAnswer(answer: "Полностью не согласен", weight: 6)
                        ]
                        var questions: [TestQuestion] = []
                        for question in test.questions {
                            questions.append(TestQuestion(question: question.question, answers: answers))
                        }
                        test = Test(title: test.title, description: test.description, intro: test.intro, questions: questions)
                    } else if test.title == "Интегративный тест тревожности" {
                        let situationAnswers = [
                            TestAnswer(answer: "Совсем нет", weight: 0),
                            TestAnswer(answer: "Слабо выражено", weight: 1),
                            TestAnswer(answer: "Выражено", weight: 2),
                            TestAnswer(answer: "Очень выражено", weight: 3)
                        ]
                        let personalAnswers = [
                            TestAnswer(answer: "Никогда", weight: 0),
                            TestAnswer(answer: "Редко", weight: 1),
                            TestAnswer(answer: "Часто", weight: 2),
                            TestAnswer(answer: "Почти всё время", weight: 3)
                        ]
                        var questions: [TestQuestion] = []
                        for (index, question) in test.questions.enumerated() {
                            questions.append(TestQuestion(question: question.question, answers: index <= 14 ? situationAnswers : personalAnswers))
                        }
                        test = Test(title: test.title, description: test.description, intro: test.intro, questions: questions)
                    } else if test.title == "Способы совладающего поведения Лазаруса" {
                        let answers = [
                            TestAnswer(answer: "Совсем нет", weight: 0),
                            TestAnswer(answer: "Слабо выражено", weight: 1),
                            TestAnswer(answer: "Выражено", weight: 2),
                            TestAnswer(answer: "Очень выражено", weight: 3)
                        ]
                        var questions: [TestQuestion] = []
                        for question in test.questions {
                            questions.append(TestQuestion(question: question.question, answers: answers))
                        }
                        test = Test(title: test.title, description: test.description, intro: test.intro, questions: questions)
                    } else if test.title == "Индекс жизненного стиля" || test.title == "Методика исследования самоотношения" {
                        let answers = [
                            TestAnswer(answer: "Да", weight: 1),
                            TestAnswer(answer: "Нет", weight: 0)
                        ]
                        
                        var questions: [TestQuestion] = []
                        for question in test.questions {
                            questions.append(TestQuestion(question: question.question, answers: answers))
                        }
                        test = Test(title: test.title, description: test.description, intro: test.intro, questions: questions)
                    }
                    
                    filledTests.append(test)
                }
                
                
                self?.output?.update(with: filledTests)
                self?.output?.stopLoading()
                
            case .failure(_):
                break
            }
        }
    }
    
}

final class CompletedTestsListPresenter {

    private let client = Client()
    
    weak var output: TestsListPresenterOutput?
    
}

// MARK:- TestsListPresenterInput
extension CompletedTestsListPresenter: TestsListPresenterInput {

    func fetchTests() {
        output?.startLoading()
        
        client.request(API.tests) { [weak self] result in
            switch result {
            case .success(let tests):
                let realm = try! Realm()
                let completedTests = realm.objects(TestResultRealm.self).map { $0.test }
                
                let filteredTests = tests.filter { test in
                    return completedTests.contains(where: { test.title == $0 })
                }
                
                self?.output?.update(with: filteredTests)
                self?.output?.stopLoading()
                
            case .failure(_):
                break
            }
        }
    }
    
}
