//
//  TestsListAssembly.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 16/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol TestsListAssemblable: TestsListViewProtocol, TestsListPresenterOutput {}

final class TestsListAssembly {
    static func assembly(with output: TestsListPresenterOutput, filtered: Bool) {
        var presenter: TestsListPresenterInput
        if filtered {
            let filteredPresenter = CompletedTestsListPresenter()
            filteredPresenter.output = output
            presenter = filteredPresenter
        } else {
            let defaultPresenter = TestsListPresenter()
            defaultPresenter.output = output
            presenter = defaultPresenter
        }
    
        output.presenter = presenter
    }
}
