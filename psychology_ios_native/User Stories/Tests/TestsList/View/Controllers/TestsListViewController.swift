//
//  TestsListViewController.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 16/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class TestsListViewController: UIViewController, TestsListAssemblable {
    
    var onReceivedTests: ObjectCallback<[Test]>?
    var onSelectTest: ObjectCallback<Test>?
    var headerChild: Presentable? { didSet { headerChildVC = headerChild?.toPresent } }
    var isBottomBarHidden: Bool? {
        didSet {
            if let isBottomBarHidden = isBottomBarHidden { hidesBottomBarWhenPushed = isBottomBarHidden }
        }
    }

	var presenter: TestsListPresenterInput?
    
    private var headerChildVC: UIViewController?
    
    @IBOutlet private weak var topUnderNavigationBarConstraint: NSLayoutConstraint!
    @IBOutlet private weak var topNavigationBarConstraint: NSLayoutConstraint!
    
    @IBOutlet private weak var tableView: AdvancedTableView!
    @IBOutlet private weak var headerContainerView: UIView!
    
    private var tests: [Test] = [] {
        didSet {
            tableView.reloadData()
        }
    }

	override func viewDidLoad() {
        super.viewDidLoad()
        
        if isBottomBarHidden != nil {
            let settings = UserDefaults.standard
            if !settings.bool(forKey: "test_completed") {
                show(message: .message("Результаты тестирования", "В этом разделе вы можете возвращаться к результатам своего тестирования и отслеживать динамику изменений. Если вы проходите консультации у психолога или психотерапевта, то можете показывать ему свои результаты. Это может стать дополнительным источником информации о вас и позволит учесть её в оказании помощи.", "OK")) {
                    settings.set(true, forKey: "test_completed")
                }
            }
        }
        
        let index = navigationController?.viewControllers.firstIndex(of: self) ?? 0
        let isPushed = index > 0
        topUnderNavigationBarConstraint.isActive = !isPushed
        topNavigationBarConstraint.isActive = isPushed
        
        addChild(headerChildVC, container: headerContainerView)
        
        setup()
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isBottomBarHidden == nil {
            navigationController?.setNavigationBarHidden(true, animated: animated)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if isBottomBarHidden == nil {
            navigationController?.setNavigationBarHidden(false, animated: animated)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.sizeToFitTableHeaderView()
    }

}

extension TestsListViewController {
    
    func startLoading() {
        DispatchQueue.main.async {
            let refreshControl = UIRefreshControl()
            self.tableView.refreshControl = refreshControl
            refreshControl.beginRefreshing()
            self.tableView.setContentOffset(CGPoint(x: 0, y: -150), animated: true)
        }
    }
    
    func stopLoading() {
        DispatchQueue.main.async {
            let refreshControl = self.tableView.refreshControl
            refreshControl?.endRefreshing()
            self.tableView.refreshControl = nil
        }
    }
    
    func update(with tests: [Test]) {
        DispatchQueue.main.async {
            self.tests = tests
            self.onReceivedTests?(tests)
        }
    }
    
}

extension TestsListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tests.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(TestTableViewCell.self)!
        
        let test = tests[indexPath.row]
        cell.configure(with: test)
        
        return cell
    }
    
}

extension TestsListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let test = tests[indexPath.row]
        DispatchQueue.main.async { self.onSelectTest?(test) }
    }
    
}

private extension TestsListViewController {
    
    func setup() {
        extendedLayoutIncludesOpaqueBars = false
        edgesForExtendedLayout = [.top, .bottom]
        
        setupTableView()
    }
    
    func setupTableView() {
        tableView.registerNib(cell: TestTableViewCell.self)
    }
    
    func loadData() {
        presenter?.fetchTests()
    }
    
}
