//
//  TestsListViewProtocol.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 16/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol TestsListViewProtocol: BaseViewProtocol {
    var onReceivedTests: ObjectCallback<[Test]>? { get set }
    var onSelectTest: ObjectCallback<Test>? { get set }
    var headerChild: Presentable? { get set }
    var isBottomBarHidden: Bool? { get set }
}

