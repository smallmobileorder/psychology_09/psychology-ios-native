//
//  TestDescriptionAssembly.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 14/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol TestDescriptionAssemblable: TestDescriptionViewProtocol, TestDescriptionPresenterOutput {}

final class TestDescriptionAssembly {
    static func assembly(with output: TestDescriptionPresenterOutput) {
        let presenter  = TestDescriptionPresenter()
    
        presenter.output     = output
        output.presenter     = presenter
    }
}
