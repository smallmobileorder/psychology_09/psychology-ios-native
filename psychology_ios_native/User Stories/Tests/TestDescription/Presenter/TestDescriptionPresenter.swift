//
//  TestDescriptionPresenter.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 14/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol TestDescriptionPresenterInput: class {

}

protocol TestDescriptionPresenterOutput: PresenterOutputProtocol {
    var presenter: TestDescriptionPresenterInput? { get set }
}

final class TestDescriptionPresenter {

    weak var output: TestDescriptionPresenterOutput?
    
}

// MARK:- TestDescriptionPresenterInput
extension TestDescriptionPresenter: TestDescriptionPresenterInput {

}
