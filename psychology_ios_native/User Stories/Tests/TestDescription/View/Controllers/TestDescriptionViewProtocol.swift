//
//  TestDescriptionViewProtocol.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 14/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol TestDescriptionViewProtocol: BaseViewProtocol {
    var onStart: Callback? { get set }
    var test: Test? { get set }
}

