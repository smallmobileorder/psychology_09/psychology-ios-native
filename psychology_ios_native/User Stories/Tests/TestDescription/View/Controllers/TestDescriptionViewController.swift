//
//  TestDescriptionViewController.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 14/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class TestDescriptionViewController: UIViewController, TestDescriptionAssemblable {
    
    var onStart: Callback?
    var test: Test?

	var presenter: TestDescriptionPresenterInput?
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var startButton: UIButton!

	override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = test?.title
        descriptionLabel.text = test?.intro
        startButton.isEnabled = onStart != nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if onStart == nil {
            show(message: .message("Ошибка", "Перед прохождением этого теста пройдите тест \"Немного о вас\"", "OK"))
        }
    }

}

private extension TestDescriptionViewController {
    
    @IBAction func start() {
        onStart?()
    }
    
}
