//
//  TestsWelcomeAssembly.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 07/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol TestsWelcomeAssemblable: TestsWelcomeViewProtocol, TestsWelcomePresenterOutput {}

final class TestsWelcomeAssembly {
    static func assembly(with output: TestsWelcomePresenterOutput) {
        let presenter = TestsWelcomePresenter()
    
        presenter.output = output
        output.presenter = presenter
    }
}
