//
//  TestsWelcomeViewProtocol.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 07/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol TestsWelcomeViewProtocol: BaseViewProtocol {
    var onSelectPersonalityTest: Callback? { get set }
    var onSelectResults: Callback? { get set }
}

