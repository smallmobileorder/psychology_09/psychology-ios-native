//
//  TestsWelcomeViewController.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 07/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class TestsWelcomeViewController: UIViewController, TestsWelcomeAssemblable {
    
    var onSelectPersonalityTest: Callback?
    var onSelectResults: Callback?
    
	var presenter: TestsWelcomePresenterInput?

    @IBOutlet private weak var personalityTestButton: BrandButton!
    @IBOutlet private weak var testsResultsButton: BrandButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let settings = UserDefaults.standard
        if !settings.bool(forKey: "test_accept") {
            show(message: .message("Раздел Тестирование", "Тесты в этом разделе направлены на то, чтобы вы смогли оценить выраженность тревоги, депрессии и возможных других симптомов, а также для самоконтроля эффекта своей работы с мыслями. Вы обнаружите свои ошибки мышления, а также психологические механизмы, которые помогают вам справляться с различными сложностями в своей жизни.\nИнформированное согласие на анонимное участие в исследовании эффективности приложения.\nДля полноценного участия в исследовании предстоит пройти тестирование в начале использования этого приложения, через 1 месяц, через 2 месяца и через год. Пожалуйста, заполните все разделы, включая раздел «Немного о вас». Приложение напомнит вам о повторном тестировании. Если у вас возникнет желание чаще отслеживать эффективность своей собственной работы с мыслями, можете проходить тестирование столько раз, сколько захотите.\nИсследование проводится анонимно и позволит нам улучшать работу приложения. Результаты исследования позволят более качественно оказывать дистанционную психологическую помощь русскоязычным людям во всем мире.\nВ любой момент вы можете отказаться от участия в исследовании, выслав письмо на адрес psychotherapevt@yandex.ru\nВсе полученные сведения конфиденциальны.\nУчастие в исследовании является добровольным и отказ от него не ведет ни к каким негативным последствиям. \nВ разделе \"Немного о вас\" вы можете выбрать вариант согласия или отказа от участия в исследовании. Тестирование будет доступно вам в любом случае.", "OK")) {
                settings.set(true, forKey: "test_accept")
                settings.synchronize()
            }
        }
        
        setup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setButtonsShadows()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        setButtonsShadows()
    }

}

private extension TestsWelcomeViewController {
    
    @IBAction func showPersonalityTest() {
        onSelectPersonalityTest?()
    }
    
    @IBAction func showResults() {
        onSelectResults?()
    }
    
}

private extension TestsWelcomeViewController {
    
    func setup() {
        setupButtons()
    }
    
    func setupButtons() {
        for button in [personalityTestButton!, testsResultsButton!] {
            button.normalColor = .white
            button.setTitleColor(.darkSystemGray2, for: .normal)
        }
    }
    
    func setButtonsShadows() {
//        for button in [personalityTestButton!, testsResultsButton!] {
//            button.addShadow(color: .shadow, opacity: 0.5, radius: 3.0, offset: .zero)
//        }
    }
    
}
