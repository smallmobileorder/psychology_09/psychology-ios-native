//
//  TestTableViewCell.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 07/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class TestTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var separator: UIView!
    @IBOutlet private weak var descriptionLabel: UILabel!
    
    func configure(with test: Test) {
        nameLabel.text = test.title
        descriptionLabel.text = test.description
        separator.backgroundColor = .active
    }
    
}
