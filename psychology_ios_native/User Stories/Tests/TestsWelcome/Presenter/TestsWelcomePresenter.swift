//
//  TestsWelcomePresenter.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 07/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol TestsWelcomePresenterInput: class {
    
}

protocol TestsWelcomePresenterOutput: PresenterOutputProtocol {
    var presenter: TestsWelcomePresenterInput? { get set }
}

final class TestsWelcomePresenter {
    weak var output: TestsWelcomePresenterOutput?
}

// MARK:- TestsWelcomePresenterInput
extension TestsWelcomePresenter: TestsWelcomePresenterInput {
    
}
