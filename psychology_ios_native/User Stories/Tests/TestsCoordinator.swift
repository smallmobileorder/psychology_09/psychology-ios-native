//
//  DiaryCoordinator.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 28/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation
import RealmSwift
import UserNotifications
import FirebaseDatabase

final class TestsCoordinator: BaseCoordinator, TestsCoordinatorOutput {
    
    var finishFlow: Callback?
    var onOpenDiary: Callback?
    
    private let factory: TestsFactoryProtocol
    private let coordinatorFactory: CoordinatorFactoryProtocol
    private let parentRouter: Routable
    
    init(with factory: TestsFactoryProtocol, coordinatorFactory: CoordinatorFactoryProtocol, router: Routable) {
        self.factory = factory
        self.coordinatorFactory = coordinatorFactory
        self.parentRouter = router
    }
    
    private weak var descriptionView: BaseViewProtocol?
    private var rootRouter: Routable?
    private var answers: [TestAnswer] = []
    private var tests: [Test] = []
    
}

// MARK:- Coordinatable

extension TestsCoordinator: Coordinatable {
    
    func start() {
        performFlow()
    }
    
}

// MARK:- Private methods

private extension TestsCoordinator {
    
    func performFlow() {
        let view = factory.makeTestsList(filtered: false)
        view.onSelectTest = { [unowned self] test in
            self.show(test)
        }
        view.onReceivedTests = { [unowned self] tests in
            self.tests = tests
        }
        
        let welcomeHeaderView = factory.makeTestsWelcomeView()
        welcomeHeaderView.onSelectPersonalityTest = { [unowned self] in
            self.showTestAbout()
        }
        welcomeHeaderView.onSelectResults = { [unowned self] in
            self.showCompletedTests()
        }
        view.headerChild = welcomeHeaderView
        
        rootRouter = factory.makeRouter(root: view, hideBar: false)
        
        parentRouter.push(rootRouter, animated: false)
    }
    
    func show(_ test: Test) {
        let view = factory.makeTestDescriptionView()
        view.test = test
        if test.title != "Интегративный тест тревожности" &&
            test.title != "Способы совладающего поведения Лазаруса" ||
            UserInfoService().info != nil {
            view.onStart = { [unowned self] in
                self.answers = []
                self.start(test)
            }
        }
        
        rootRouter?.push(view)
        
        descriptionView = view
    }
    
    func start(_ test: Test, index: Int = 0) {
        guard 0 ..< test.questions.count ~= index else {
            var calculator: TestCalculator?
            switch test.title {
            case "Шкала дисфункциональных отношений Вейсмана – Бека": calculator = WeissmanBeckCalculator(test: test)
            case "Тест Эллиса": calculator = EllisCalulcator(test: test)
            case "Госпитальная шкала тревоги и депрессии": calculator = DepressionAndAnxietyCalculator(test: test)
            case "Интегративный тест тревожности": if let info = UserInfoService().info { calculator = IntegralAnxietyCalculator(test: test, info: info) }
            case "Способы совладающего поведения Лазаруса": if let info = UserInfoService().info { calculator = LazarusCalculator(test: test, info: info) }
            case "Индекс жизненного стиля": calculator = LifestyleCalculator(test: test)
            case "Методика исследования самоотношения": calculator = SelfAttitudeCalculator(test: test)
            default: return
            }
            showConclusion(test, calculator: calculator)
            return
        }
        
        let progressHeaderView = factory.makeProgressHeaderView()
        progressHeaderView.headerTitle = "Вопрос \(index + 1)"
        progressHeaderView.casesCount = test.questions.count
        progressHeaderView.currentCaseIndex = index
        
        let view = factory.makeTestQuestionView()
        view.headerChild = progressHeaderView
        view.onClose = { [unowned self] in
            self.rootRouter?.popToModule(self.descriptionView, animated: true)
        }
        view.question = test.questions[index]
        view.onSelect = { [unowned self] answer in
            self.answers.append(answer)
            self.start(test, index: index + 1)
        }
        
        if index == 0 {
            rootRouter?.push(view)
        } else {
            rootRouter?.pushReplacing(view)
        }
    }
    
    func showConclusion(_ test: Test, calculator: TestCalculator?) {
        let notificationCenter = UNUserNotificationCenter.current()
        let options: UNAuthorizationOptions = [.alert, .sound, .badge]
        notificationCenter.requestAuthorization(options: options) {
            (didAllow, error) in
            guard didAllow else { return }
            
            self.scheduleNotification()
        }
        
        
        let view = factory.makeTestConclusionView()
        let result = calculator!.calculate(summary: TestAnswerSummary(answers: answers))
        view.result = result
        var dict = result.res.pairs.enumerated().reduce(into: [String: Any](), { result, pair in
            result["_\(pair.offset)"] = pair.element.value
        })
        var popa: [String: Any] = [:]
        if test.title == "Шкала дисфункциональных отношений Вейсмана – Бека" {
            let gen = dict.removeValue(forKey: "_40")
            popa["results"] = dict
            popa["general"] = gen
        } else {
            popa["results"] = dict
        }
        let user = UserInfoService().info
        var info: [String: Any] = [:]
        if let user = user {
            info["email"] = user.email
            info["age"] = user.age
            info["education"] = EducationEnum.allCases[educationAnswers.firstIndex(of: user.educationStatus)!].rawValue
            info["frequencyOfTherapy"] = FrequencyOfTherapyEnum.allCases[doctorFrequencyAnswers.firstIndex(of: user.doctorFrequency)!].rawValue
            info["frequencyOfUse"] = FrequencyOfUseEnum.allCases[applicationUsageAnswers.firstIndex(of: user.applicationUsageFrequency)!].rawValue
            info["maritalStatus"] = MaritalStatusEnum.allCases[user.sex == .man ? manMartial.firstIndex(of: user.maritalStatus)! : womanMartial.firstIndex(of: user.maritalStatus)!].rawValue
            
            var first: [String] = []
            var second: [String] = []
            user.drugs.forEach { string in
                if let i = drugsAnswers.firstIndex(of: string) {
                    first.append(MedicinesEnum.allCases[i].rawValue)
                } else {
                    second.append(string)
                }
            }
            info["medicines"] = [
                "first": first,
                "second": second
            ]
            info["sex"] = user.sex == .man ? "MAN" : "WOMAN"
            info["timeOfPsychologistVisit"] = max(0, user.doctorDuration)
            info["timeOfUse"] = user.applicationUsageDuration
            info["visitTherapy"] = user.doctorFrequency != "Не обращаюсь"
            info["agreeToSendMyTestInfo"] = user.canSend
        }
        
        Database.database().reference().child("analytics").childByAutoId().setValue([
            "date": Date().string(format: "dd.MM.yyyy"),
            "nameTest": test.title ?? "",
            "userModel": info,
            test.title!: popa
        ])
        
        if let currentIndex = tests.index(of: test), currentIndex != tests.count - 1 {
            let nextTest = tests[currentIndex + 1]
            view.onNextTest = { [weak self] in
                self?.rootRouter?.popToRootModule(animated: true)
                self?.show(nextTest)
            }
        }
        view.onSelectTest = { [weak self] in
            self?.rootRouter?.popToRootModule(animated: true)
        }
        view.onOpenDiary = { [weak self] in
            self?.rootRouter?.popToRootModule(animated: false)
            self?.onOpenDiary?()
        }
        rootRouter?.pushReplacing(view)
    }
    
    enum Interval: Int, CaseIterable {
        case month = 1
        case month2 = 2
        case year = 12
        
        var date: Date {
            Calendar.current.date(byAdding: .month, value: rawValue, to: Date())!
        }
        
        var title: String {
            switch self {
                case .month:
                    return "Прошел 1 месяц с последнего тестирования"
                case .month2:
                    return "Прошло 2 месяца с последнего тестирования"
                case .year:
                    return "Прошло 12 месяцев с последнего тестирования"
            }
        }
    }
    
    private func scheduleNotification() {
        for interval in Interval.allCases {
            let content = UNMutableNotificationContent()
            content.title = interval.title
            content.body = "Не забывайте проходить тесты для большего понимания себя и изменения своих мыслей"
            content.sound = .default
            content.badge = 1
            
            let triggerDate = Calendar.current.dateComponents([.year,.month,.day,.hour,.minute,.second], from: interval.date)
            
            let trigger = UNCalendarNotificationTrigger(dateMatching: triggerDate, repeats: false)
            
            let identifier = "notification_reminder_\(interval.rawValue)"
            let request = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)

            UNUserNotificationCenter.current().add(request) { (error) in
                if let error = error {
                    print("Error \(error.localizedDescription)")
                }
            }
        }
    }
    
    func showCompletedTests() {
        let headerView = factory.makeSectionHeaderView()
        headerView.headerTitle = "Результаты тестирования"
        
        let view = factory.makeTestsList(filtered: false)
        view.onSelectTest = { [unowned self] test in
            self.showCompletedTests(for: test)
        }
        view.headerChild = headerView
        view.isBottomBarHidden = true
        
        rootRouter?.push(view)
    }
    
    func showTestAbout() {
        let view = factory.makeTestAboutView()
        view.onFinish = { [unowned self] in
            self.rootRouter?.popToRootModule(animated: true)
        }
        rootRouter?.push(view)
    }
    
    func showCompletedTests(for test: Test) {
        let headerView = factory.makeSectionHeaderView()
        headerView.headerTitle = test.title
        
        let realm = try! Realm()
        let results = realm.objects(TestResultRealm.self).filter("test == '\(test.title ?? "")'")
    
        let view = factory.makeCompletedTestsView()
        view.headerChild = headerView
        view.results = results
        
        rootRouter?.push(view)
    }
    
}
