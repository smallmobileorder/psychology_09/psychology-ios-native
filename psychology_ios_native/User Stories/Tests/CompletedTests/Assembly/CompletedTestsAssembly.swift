//
//  CompletedTestsAssembly.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 16/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol CompletedTestsAssemblable: CompletedTestsViewProtocol, CompletedTestsPresenterOutput {}

final class CompletedTestsAssembly {
    static func assembly(with output: CompletedTestsPresenterOutput) {
        let presenter  = CompletedTestsPresenter()
    
        presenter.output     = output
        output.presenter     = presenter
    }
}
