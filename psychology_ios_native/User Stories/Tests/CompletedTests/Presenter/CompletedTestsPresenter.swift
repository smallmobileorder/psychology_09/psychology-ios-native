//
//  CompletedTestsPresenter.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 16/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol CompletedTestsPresenterInput: class {

}

protocol CompletedTestsPresenterOutput: class {
    var presenter: CompletedTestsPresenterInput? { get set }
}

final class CompletedTestsPresenter {

    weak var output: CompletedTestsPresenterOutput?
    
}

// MARK:- CompletedTestsPresenterInput
extension CompletedTestsPresenter: CompletedTestsPresenterInput {

}
