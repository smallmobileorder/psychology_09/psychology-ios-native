//
//  CompletedTestsViewController.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 16/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit
import RealmSwift

struct TestResultViewModel {
    let date: Date
    let holders: [ExpandedHolder<ResultRealmDescription>]
}

class CompletedTestsViewController: UIViewController, CompletedTestsAssemblable {
    
    var headerChild: Presentable? { didSet { headerChildVC = headerChild?.toPresent } }
    var results: Results<TestResultRealm>?

	var presenter: CompletedTestsPresenterInput?
    
    private var headerChildVC: UIViewController?
    
    @IBOutlet private weak var tableView: AdvancedTableView!
    @IBOutlet private weak var headerContainerView: UIView!
    
    private var heights: [IndexPath: CGFloat] = [:]
    
    private var holders: [ExpandedHolder<TestResultViewModel>] = []

	override func viewDidLoad() {
        super.viewDidLoad()
        
        addChild(headerChildVC, container: headerContainerView)
        
        if let results = results {
            holders = results.map { result in
                let descriptions = Array(result.results).map { ExpandedHolder(with: $0) }
                return ExpandedHolder(with: TestResultViewModel(date: result.date, holders: descriptions))
            }
        }
        
        for (index, holder) in holders.enumerated() {
            let updateBlock = { [weak self] in
                self?.tableView.beginUpdates()
                self?.tableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .automatic)
                self?.tableView.endUpdates()
            }
            holder.onChange = {
                holder.model.holders.forEach { $0.collapse(callingCallback: false) }
                updateBlock()
            }
            for holder in holder.model.holders {
                holder.onChange = {
                    UIView.performWithoutAnimation {
                        updateBlock()
                    }
                }
            }
        }
        
        setup()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.sizeToFitTableHeaderView()
    }

}

extension CompletedTestsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return holders.isEmpty ? 0 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return holders.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(CompletedTestTableViewCell.self)!
        
        let result = holders[indexPath.row]
        cell.configure(with: result)
        
        DispatchQueue.main.async {
            cell.setNeedsLayout()
            cell.layoutIfNeeded()
        }
        
        return cell
    }
    
}

extension CompletedTestsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        heights[indexPath] = cell.frame.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return heights[indexPath] ?? UITableView.automaticDimension
    }
    
}

private extension CompletedTestsViewController {
    
    func setup() {
        setupTableView()
    }
    
    func setupTableView() {
        tableView.registerNib(cell: CompletedTestTableViewCell.self)
        let emptyView = EmptyDatabaseView()
        emptyView.text = "У вас пока здесь нет результатов"
        tableView.emptyView = emptyView
    }
    
}
