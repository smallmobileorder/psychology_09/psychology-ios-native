//
//  CompletedTestsViewProtocol.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 16/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit
import RealmSwift

protocol CompletedTestsViewProtocol: BaseViewProtocol {
    var headerChild: Presentable? { get set }
    var results: Results<TestResultRealm>? { get set }
}

