//
//  CompletedTestTableViewCell.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 16/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class CompletedTestTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var containerView: UIView!
    @IBOutlet private weak var dateButton: UIButton!
    @IBOutlet private weak var expandedStackView: UIStackView!
    @IBOutlet private weak var resultsStackView: UIStackView!
    @IBOutlet private weak var line: UIView!
    
    private var result: ExpandedHolder<TestResultViewModel>?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        line.backgroundColor = .active
        resultsStackView.spacing = 16
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if result?.expanded ?? false {
            removeShadow()
            addInnerShadow()
        } else {
            layerInnerShadow?.removeFromSuperlayer()
            containerView.addShadow(color: .shadow, opacity: 0.5, radius: 3.0, offset: .zero)
        }
    }
    
    private weak var layerInnerShadow: CALayer?
    
    private func addInnerShadow() {
        let innerShadow = layerInnerShadow ?? {
            let innerShadow = CALayer()
            containerView.layer.addSublayer(innerShadow)
            layerInnerShadow = innerShadow
            return innerShadow
        }()
        innerShadow.frame = containerView.bounds
        let path = UIBezierPath(rect: innerShadow.bounds.inset(by: .only(top: -0.8, left: -0.8, bottom: -0.4, right: -0.8)))
        let cutout = UIBezierPath(rect: innerShadow.bounds).reversing()
        path.append(cutout)
        containerView.clipsToBounds = true
        innerShadow.shadowPath = path.cgPath
        innerShadow.masksToBounds = true
        innerShadow.shadowColor = UIColor(white: 0, alpha: 1).cgColor
        innerShadow.shadowOffset = CGSize.zero
        innerShadow.shadowOpacity = 0.6
        innerShadow.shadowRadius = 3
    }

    func configure(with result: ExpandedHolder<TestResultViewModel>?) {
        self.result = result
        
        guard let result = result else { return }
        
        let model = result.model
        dateButton.setTitle(model.date.string(format: "dd.MM.yyyy"), for: .normal)
        expandedStackView.isHidden = !result.expanded
        
        resultsStackView.subviews.forEach { $0.removeFromSuperview() }
        
        let hasExpandArrow = model.holders.count > 1
        
        for (index, result) in model.holders.enumerated() {
            let stackView = UIStackView()
            stackView.spacing = 12
            stackView.axis = .vertical
            stackView.alignment = .fill
            
            let headerView = UIView()
            
            let headerStackView = UIStackView()
            headerStackView.axis = .horizontal
            headerStackView.alignment = .fill
            headerStackView.spacing = 8
            headerStackView.translatesAutoresizingMaskIntoConstraints = false
            headerView.addSubview(headerStackView)
            headerStackView.setEqualSize(on: headerView)
            
            let button = UIButton()
            button.tag = index
            button.addTarget(self, action: #selector(expandResult(_:)), for: .touchUpInside)
            button.translatesAutoresizingMaskIntoConstraints = false
            headerView.addSubview(button)
            button.setEqualSize(on: headerView)
            
            stackView.addArrangedSubview(headerView)
            
            let titleLabel = UILabel()
            titleLabel.font = .systemFont(ofSize: 17, weight: .semibold)
            titleLabel.textColor = .black
            titleLabel.text = result.model.title
            titleLabel.numberOfLines = 0
            headerStackView.addArrangedSubview(titleLabel)
            
            if hasExpandArrow && !result.model.advice.isEmpty {
                let arrowImageView = UIImageView(image: UIImage(named: "expand"))
                arrowImageView.transform = CGAffineTransform(rotationAngle: result.expanded ? 0 : .pi * 0.999)
                arrowImageView.tintColor = .active
                arrowImageView.contentMode = .center
                arrowImageView.widthAnchor(constant: 15)
                headerStackView.addArrangedSubview(arrowImageView)
                if result.previousState != result.expanded {
                    arrowImageView.transform = CGAffineTransform(rotationAngle: result.expanded ? 0 : .pi * 0.999)
                    let oldAnimationState = UIView.areAnimationsEnabled
                    UIView.setAnimationsEnabled(true)
//                    UIView.animate(withDuration: 0.3) {
                        arrowImageView.transform = CGAffineTransform(rotationAngle: result.expanded ? -.pi : 0.001)
//                    }
                    UIView.setAnimationsEnabled(oldAnimationState)
                } else {
                    arrowImageView.transform = CGAffineTransform(rotationAngle: result.expanded ? .pi : 0)
                }
            }
            
            if !hasExpandArrow || result.expanded {
                let adviceLabel = UILabel()
                adviceLabel.font = .systemFont(ofSize: 16)
                adviceLabel.textColor = .darkSystemGray2
                adviceLabel.text = result.model.advice
                adviceLabel.numberOfLines = 0
                stackView.addArrangedSubview(adviceLabel)
            }
            
            resultsStackView.addArrangedSubview(stackView)
        }
    }
    
}

private extension CompletedTestTableViewCell {
    
    @IBAction func expand() {
        guard let result = result else { return }
        
        if result.expanded { result.collapse() }
        else { result.expand() }
    }
    
    @objc func expandResult(_ sender: UIButton) {
        guard let result = result?.model.holders[sender.tag],
            !result.model.advice.isEmpty else { return }
        
        if result.expanded { result.collapse() }
        else { result.expand() }
    }
    
}
