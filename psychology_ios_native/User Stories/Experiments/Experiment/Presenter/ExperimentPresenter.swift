//
//  ExperimentPresenter.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 25/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol ExperimentPresenterInput: class {

}

protocol ExperimentPresenterOutput: class {
    var presenter: ExperimentPresenterInput? { get set }
}

final class ExperimentPresenter {

    weak var output: ExperimentPresenterOutput?
    
}

// MARK:- ExperimentPresenterInput
extension ExperimentPresenter: ExperimentPresenterInput {

}
