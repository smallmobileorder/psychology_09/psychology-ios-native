//
//  ExperimentViewController.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 25/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class ExperimentViewController: UIViewController, ExperimentAssemblable {
    
    var experimentTitle: String?
    var experimentSituation: ExperimentSituation?
    var onClose: Callback?
    var onCompletion: Callback?
    
    var presenter: ExperimentPresenterInput?
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var situationLabel: UILabel!
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var questionPrefixLabel: UILabel!
    @IBOutlet private weak var tableView: AdvancedTableView!
    @IBOutlet private weak var finishButton: UIButton!
    
    var answers: [Int: String] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
        
        addNavigationBackButton(selector: #selector(close))
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.sizeToFitTableHeaderView()
    }
    
}

private extension ExperimentViewController {
    
    @IBAction func finish(_ sender: Any) {
        onCompletion?()
    }
    
    @objc func close() {
        showAlertСontroller(
            title: "Вы действительно хотите закончить эксперимент?",
            actions: [
                (title: "Отмена", handler: {}),
                (title: "Да", handler: { [weak self] in self?.onClose?() })
            ]
        )
    }
    
}

extension ExperimentViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return experimentSituation?.questions?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(ExpAnswerTableViewCell.self)!
        
        let currentRow = indexPath.row
        let question = experimentSituation?.questions?[currentRow]
        cell.configure(
            with: question,
            answer: answers[currentRow],
            isLast: currentRow + 1 == experimentSituation?.questions?.count,
            onTextChange: { [weak self] answer in
                if answer.isEmpty {
                    self?.answers.removeValue(forKey: currentRow)
                } else {
                    self?.answers[currentRow] = answer
                }
                self?.finishButton.isEnabled = self?.answers.keys.count == self?.experimentSituation?.questions?.count
                DispatchQueue.main.async {
                    UIView.performWithoutAnimation {
                        self?.tableView.beginUpdates()
                        self?.tableView.endUpdates()
                    }
                }
            },
            onNext: { [weak self] in
                let nextRow = currentRow + 1
                if nextRow == self?.experimentSituation?.questions?.count {
                    self?.view.endEditing(true)
                } else {
                    UIView.animate(withDuration: 0.3, animations: {
                        self?.tableView.scrollToRow(
                            at: IndexPath(row: nextRow, section: 0),
                            at: .bottom,
                            animated: false
                        )
                    }, completion: { _ in
                        self?.tableView.cellForRow(at: IndexPath(row: nextRow, section: 0))?.becomeFirstResponder()
                    })
                }
            }
        )
        
        return cell
    }
    
}

private extension ExperimentViewController {
    
    func setup(){
        view.setDismissKeyboardOnTap()
        setupTableView()
        setupWithData()
    }
    
    func setupTableView(){
        tableView.registerNib(cell: ExpAnswerTableViewCell.self)
        tableView.layoutMargins = .only(left: 16, right: 16)
    }
    
    func setupWithData() {
        imageView.image = UIImage(named: experimentSituation?.image ?? "")
        titleLabel.text = experimentTitle
        situationLabel.text = experimentSituation?.text
        if let text = experimentSituation?.text,
            let data = text.data(using: .unicode),
             let attributedString = try? NSMutableAttributedString(data: data,
                                                                   options: [.documentType: NSAttributedString.DocumentType.html], documentAttributes: nil) {
            attributedString.setFontFace(
                font: .systemFont(ofSize: 16, weight: .semibold),
                color: .darkSystemGray4
            )
            situationLabel.attributedText = attributedString
        } else {
            situationLabel.removeFromSuperview()
        }
        questionPrefixLabel.text = experimentSituation?.questionPrefix
        if experimentSituation?.questionPrefix == nil {
            questionPrefixLabel.removeFromSuperview()
        }
    }
    
}
