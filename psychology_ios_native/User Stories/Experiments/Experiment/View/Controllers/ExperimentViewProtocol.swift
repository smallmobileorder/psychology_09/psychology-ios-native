//
//  ExperimentViewProtocol.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 25/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol ExperimentViewProtocol: BaseViewProtocol {
    var experimentTitle: String? { get set }
    var experimentSituation: ExperimentSituation? { get set }
    var onClose: Callback? { get set }
    var onCompletion: Callback? { get set }
}

