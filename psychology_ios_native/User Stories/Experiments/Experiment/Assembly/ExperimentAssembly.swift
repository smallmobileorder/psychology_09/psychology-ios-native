//
//  ExperimentAssembly.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 25/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol ExperimentAssemblable: ExperimentViewProtocol, ExperimentPresenterOutput {}

final class ExperimentAssembly {
    static func assembly(with output: ExperimentPresenterOutput) {
        let presenter  = ExperimentPresenter()
    
        presenter.output     = output
        output.presenter     = presenter
    }
}
