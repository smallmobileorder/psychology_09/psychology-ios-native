//
//  ExperimentDescriptionAssembly.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 25/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol ExperimentDescriptionAssemblable: ExperimentDescriptionViewProtocol, ExperimentDescriptionPresenterOutput {}

final class ExperimentDescriptionAssembly {
    static func assembly(with output: ExperimentDescriptionPresenterOutput) {
        let presenter  = ExperimentDescriptionPresenter()
    
        presenter.output     = output
        output.presenter     = presenter
    }
}
