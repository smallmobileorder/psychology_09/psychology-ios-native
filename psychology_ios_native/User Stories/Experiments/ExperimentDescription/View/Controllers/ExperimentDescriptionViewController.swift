//
//  ExperimentDescriptionViewController.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 25/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class ExperimentDescriptionViewController: UIViewController, ExperimentDescriptionImageDestination, ExperimentDescriptionAssemblable {
    
    var imageTransitionTo: UIImageView! { return imageView }
    
    var experimentTitle: String?
    var experimentDescription: ExperimentDescription?
    var onStart: Callback?
    var onClose: Callback?

	var presenter: ExperimentDescriptionPresenterInput?
    
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    
    private var popDelegate: UIGestureRecognizerDelegate?
    
    override var prefersStatusBarHidden: Bool { return true }
    
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation { return .slide }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupWithData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationController?.setNavigationBarHidden(true, animated: animated)
        
        popDelegate = navigationController?.interactivePopGestureRecognizer?.delegate
        navigationController?.interactivePopGestureRecognizer?.delegate = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        navigationController?.interactivePopGestureRecognizer?.delegate = popDelegate
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        navigationController?.interactivePopGestureRecognizer?.delegate = popDelegate
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        navigationController?.setNavigationBarHidden(false, animated: animated)
        
        navigationController?.interactivePopGestureRecognizer?.delegate = popDelegate
    }
    
}

private extension ExperimentDescriptionViewController {
    
    @IBAction func close(_ sender: Any) {
        onClose?()
    }
    
    @IBAction func start(_ sender: Any) {
        onStart?()
    }
    
}

private extension ExperimentDescriptionViewController {
    
    func setupWithData() {
        imageView.image = UIImage(named: experimentDescription?.image ?? "")
        titleLabel.text = experimentTitle
        descriptionLabel.text = experimentDescription?.text
    }
    
}
