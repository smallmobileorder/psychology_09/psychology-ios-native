//
//  ExperimentDescriptionViewProtocol.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 25/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol ExperimentDescriptionViewProtocol: BaseViewProtocol {
    var experimentTitle: String? { get set }
    var experimentDescription: ExperimentDescription? { get set }
    var onStart: Callback? { get set }
    var onClose: Callback? { get set }
}

