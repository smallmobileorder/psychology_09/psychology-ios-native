//
//  ExperimentDescriptionPresenter.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 25/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol ExperimentDescriptionPresenterInput: class {

}

protocol ExperimentDescriptionPresenterOutput: class {
    var presenter: ExperimentDescriptionPresenterInput? { get set }
}

final class ExperimentDescriptionPresenter {

    weak var output: ExperimentDescriptionPresenterOutput?
    
}

// MARK:- ExperimentDescriptionPresenterInput
extension ExperimentDescriptionPresenter: ExperimentDescriptionPresenterInput {

}
