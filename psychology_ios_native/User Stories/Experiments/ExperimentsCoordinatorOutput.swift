//
//  ExperimentsCoordinatorOutput.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 26/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

protocol ExperimentsCoordinatorOutput: class {
    var finishFlow: Callback? { get set }
    var onOpenDiary: Callback? { get set }
}
