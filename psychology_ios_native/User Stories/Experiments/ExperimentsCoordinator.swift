//
//  ExperimentsCoordinator.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 26/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

final class ExperimentsCoordinator: BaseCoordinator, ExperimentsCoordinatorOutput {
    
    var finishFlow: Callback?
    var onOpenDiary: Callback?
    
    private let factory: ExperimentsFactoryProtocol
    private let coordinatorFactory: CoordinatorFactoryProtocol
    private let parentRouter: Routable
    
    init(with factory: ExperimentsFactoryProtocol, coordinatorFactory: CoordinatorFactoryProtocol, router: Routable) {
        self.factory = factory
        self.coordinatorFactory = coordinatorFactory
        self.parentRouter = router
    }
    
    private var rootRouter: Routable?
    private var flowRouter: Routable?
    
    private lazy var experiments: [Experiment] = {
        let json = Bundle.main.json(named: "experiments")
        return (try? JSONDecoder().decode([Experiment].self, from: json)) ?? []
    }()
    
    private lazy var exercises: [Exercise] = {
        let json = Bundle.main.json(named: "exercises")
        return (try? JSONDecoder().decode([Exercise].self, from: json)) ?? []
    }()
    
    private weak var listView: BaseViewProtocol?
    private weak var descriptionView: BaseViewProtocol?
    
}

// MARK:- Coordinatable

extension ExperimentsCoordinator: Coordinatable {
    
    func start() {
        performFlow()
    }
    
}

// MARK:- Private methods

private extension ExperimentsCoordinator {
    
    func performFlow() {
        let view = factory.makeExperimentsListView()
        
        view.experiments = experiments
        view.exercises = exercises
        view.onSelectExperiment = { [weak self] experiment in self?.show(experiment) }
        view.onSelectExercise = { [weak self] exercise in self?.show(exercise) }
        
        rootRouter = factory.makeRouter(root: view, hideBar: false)
        
        parentRouter.push(rootRouter, animated: false)
        
        listView = view
    }
    
    func show(_ experiment: Experiment) {
        guard experiment.description != nil else {
            start(experiment)
            return
        }
        
        let view = factory.makeExperimentDescriptionView()
        view.experimentTitle = experiment.title?.text
        view.experimentDescription = experiment.description
        view.onStart = { [weak self] in self?.start(experiment) }
        view.onClose = { [weak self] in
            self?.flowRouter?.dismissModule()
            self?.flowRouter = nil
        }
        
//        (view as! UIViewController).transitioningDelegate = (listView as! ExperimentsListImageSource)
        
        flowRouter = factory.makeRouter(root: view, hideBar: false)
        
        rootRouter?.present(flowRouter)
        
        descriptionView = view
    }
    
    func start(_ experiment: Experiment) {
        let view = factory.makeExperimentView()
        view.experimentTitle = experiment.title?.text
        view.onClose = { [weak self] in
            let router = self?.flowRouter ?? self?.rootRouter
            if let descriptionView = self?.descriptionView {
                router?.popToModule(descriptionView, animated: true)
            } else {
                router?.popToRootModule(animated: true)
            }
        }
        view.experimentSituation = experiment.situation
        view.onCompletion = { [weak self] in
            self?.showConclusion(experiment)
        }
        (flowRouter ?? rootRouter)?.push(view)
    }
    
    func showConclusion(_ experiment: Experiment) {
        let view = factory.makeExperimentConclusionView()
        view.experimentTitle = experiment.title?.text
        view.experimentConclusion = experiment.conclusion
        
        if let currentIndex = experiments.index(of: experiment), currentIndex != experiments.count - 1 {
            let nextExperiment = experiments[currentIndex + 1]
            view.nextExperimentTitle = nextExperiment.title?.text
            view.onNextExperiment = { [weak self] in
                self?.rootRouter?.popToRootModule(animated: true)
                self?.flowRouter?.dismissModule()
                self?.show(nextExperiment)
            }
        }
        view.onClose = { [weak self] in
            self?.flowRouter?.dismissModule(animated: true)
            self?.rootRouter?.popToRootModule(animated: true)
            self?.flowRouter = nil
        }
        view.onAnotherExperiment = { [weak self] in
            self?.rootRouter?.popToRootModule(animated: true)
            self?.flowRouter?.dismissModule()
            self?.flowRouter = nil
        }
        view.onOpenExercise = { [weak self] in
            self?.rootRouter?.popToRootModule(animated: true)
            self?.flowRouter?.dismissModule()
            if let exercise = self?.exercises.first { self?.show(exercise) }
        }
        
        (flowRouter ?? rootRouter)?.push(view)
    }
    
    func show(_ exercise: Exercise) {
        guard exercise.description != nil else {
            start(exercise)
            return
        }
        
        let view = factory.makeExerciseDescriptionView()
        view.experimentTitle = exercise.title?.text
        view.experimentDescription = exercise.description
        view.onStart = { [weak self] in self?.start(exercise) }
        view.onClose = { [weak self] in
            self?.flowRouter?.dismissModule()
            self?.rootRouter?.popToRootModule(animated: true)
            self?.flowRouter = nil
        }
        
        flowRouter = factory.makeRouter(root: view, hideBar: false)
        
        rootRouter?.present(flowRouter)
        
        descriptionView = view
    }
    
    func start(_ exercise: Exercise, withIndex index: Int = 0) {
        guard 0 ..< exercise.cases.count ~= index else {
            showConclusion(exercise)
            return
        }
        
        let progressHeaderView = factory.makeProgressHeaderView()
        progressHeaderView.headerTitle = exercise.title?.text
        progressHeaderView.casesCount = exercise.cases.count
        progressHeaderView.currentCaseIndex = index
        
        let showingCase = exercise.cases[index]
        
        let exerciseQuestionsView = factory.makeExerciseQuestionsView()
        exerciseQuestionsView.headerChild = progressHeaderView
        exerciseQuestionsView.onClose = { [weak self] in
            let router = self?.flowRouter ?? self?.rootRouter
            if let descriptionView = self?.descriptionView {
                router?.popToModule(descriptionView, animated: true)
            } else {
                router?.popToRootModule(animated: true)
            }
        }
        exerciseQuestionsView.exerciseCase = showingCase
        exerciseQuestionsView.onCompletion = { [weak self] in
            exerciseQuestionsView.showAnswers()
            exerciseQuestionsView.onCompletion = {
                self?.start(exercise, withIndex: index + 1)
            }
        }
        
        let router = flowRouter ?? rootRouter
        
        if index == 0 {
            router?.push(exerciseQuestionsView)
        } else {
            router?.pushReplacing(exerciseQuestionsView)
        }
    }
    
    func showConclusion(_ exercise: Exercise) {
        let view = factory.makeExperimentConclusionView()
        view.experimentTitle = exercise.title?.text
        view.experimentConclusion = exercise.conclusion
        view.onClose = { [weak self] in
            self?.flowRouter?.dismissModule(animated: true)
            self?.rootRouter?.popToRootModule(animated: true)
            self?.flowRouter = nil
        }
        view.onChooseExperiment = { [weak self] in
            self?.rootRouter?.popToRootModule(animated: true)
            self?.flowRouter?.dismissModule()
            self?.flowRouter = nil
        }
        view.onOpenDiary = { [weak self] in
            self?.rootRouter?.popToRootModule(animated: true)
            self?.flowRouter?.dismissModule()
            self?.flowRouter = nil
            self?.onOpenDiary?()
        }
        
        (flowRouter ?? rootRouter)?.push(view)
    }
    
}
