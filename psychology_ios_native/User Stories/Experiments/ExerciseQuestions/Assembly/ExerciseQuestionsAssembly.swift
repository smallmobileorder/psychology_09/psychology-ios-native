//
//  ExerciseQuestionsAssembly.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 28/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol ExerciseQuestionsAssemblable: ExerciseQuestionsViewProtocol, ExerciseQuestionsPresenterOutput {}

final class ExerciseQuestionsAssembly {
    static func assembly(with output: ExerciseQuestionsPresenterOutput) {
        let presenter  = ExerciseQuestionsPresenter()
    
        presenter.output     = output
        output.presenter     = presenter
    }
}
