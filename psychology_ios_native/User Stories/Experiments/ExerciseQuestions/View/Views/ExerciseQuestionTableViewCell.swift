//
//  ExpAnswerTableViewCell.swift
//  psychology_ios_native
//
//  Created by Alex Korneev on 21.09.2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class ExerciseQuestionTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var questionLabel: UILabel!
    @IBOutlet private weak var textView: UITextView!
    @IBOutlet private weak var answerLabel: UILabel!
    @IBOutlet private weak var correctAnswerLabel: UILabel!
    
    var onTextChangeCallback: ObjectCallback<String>?
    var onNextCallback: Callback?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        textView.sizeToFit()
        textView.delegate = self
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        onTextChangeCallback = nil
        onNextCallback = nil
    }
    
    override func becomeFirstResponder() -> Bool {
        return super.becomeFirstResponder() || textView.becomeFirstResponder()
    }
    
    func configure(
        with question: String?,
        answer: String?,
        correctAnswer: String? = nil,
        isLast: Bool,
        onTextChange: ObjectCallback<String>?,
        onNext: Callback?) {
        textView.isHidden = correctAnswer != nil
        answerLabel.isHidden = correctAnswer == nil
        correctAnswerLabel.isHidden = correctAnswer == nil
        if let correctAnswer = correctAnswer {
            answerLabel.text = answer
            correctAnswerLabel.text = correctAnswer
        } else {
            textView.text = answer
            textView.returnKeyType = isLast ? .done : .continue
        }
        questionLabel.text = question
        onTextChangeCallback = onTextChange
        onNextCallback = onNext
    }
    
}

extension ExerciseQuestionTableViewCell: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        onTextChangeCallback?(textView.text ?? "")
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            onNextCallback?()
            return false
        }
        return true
    }
    
}
