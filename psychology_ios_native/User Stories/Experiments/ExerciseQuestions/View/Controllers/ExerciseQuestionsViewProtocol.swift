//
//  ExerciseQuestionsViewProtocol.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 28/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol ExerciseQuestionsViewProtocol: BaseViewProtocol {
    var headerChild: Presentable? { get set }
    var exerciseCase: ExerciseCase? { get set }
    var onClose: Callback? { get set }
    var onCompletion: Callback? { get set }
    func showAnswers()
}

