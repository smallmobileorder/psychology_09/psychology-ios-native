//
//  ExerciseQuestionsViewController.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 28/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class ExerciseQuestionsViewController: UIViewController, ExerciseQuestionsAssemblable {

    var headerChild: Presentable? { didSet { headerChildVC = headerChild?.toPresent } }
    var exerciseCase: ExerciseCase?
    var onClose: Callback?
    var onCompletion: Callback?
    
    var presenter: ExerciseQuestionsPresenterInput?
    
    private var headerChildVC: UIViewController?
    
    @IBOutlet private weak var headerContainerView: UIView!
    @IBOutlet private weak var situationLabel: UILabel!
    @IBOutlet private weak var tableView: AdvancedTableView!
    @IBOutlet private weak var finishButton: UIButton!
    
    private var finished = false
    var answers: [Int: String] = [:]
    var shouldScrollToTopAfterReloading = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addChild(headerChildVC, container: headerContainerView)
        
        setup()
        
        addNavigationBackButton(selector: #selector(close))
    }
    
    @IBOutlet weak var mylabel: UILabel!
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        tableView.sizeToFitTableHeaderView()
    }
    
    @objc func close() {
        showAlertСontroller(
            title: "Вы действительно хотите прервать эксперимент?",
            actions: [
                (title: "Отмена", handler: {}),
                (title: "Да", handler: { [weak self] in self?.onClose?() })
            ]
        )
    }

}

extension ExerciseQuestionsViewController {
    
    func showAnswers() {
        finishButton.setTitle("Далее", for: .normal)
        finished = true
        UIView.performWithoutAnimation {
            tableView.beginUpdates()
            tableView.reloadSections(IndexSet(integer: 0), with: .none)
            tableView.endUpdates()
        }
        let attr = NSMutableAttributedString(string: "*Ваш ответ  ", attributes: [.font: UIFont.systemFont(ofSize: 16, weight: .regular), .foregroundColor: UIColor(rgb: 0x8E8E93).cgColor])
        let attr2 = NSAttributedString(string: "*Правильный ответ", attributes: [.font: UIFont.systemFont(ofSize: 16, weight: .regular), .foregroundColor: UIColor.active.cgColor])
        
        attr.append(attr2)
        
        mylabel.attributedText = attr
        
        shouldScrollToTopAfterReloading = true
    }
    
}

private extension ExerciseQuestionsViewController {
    
    @IBAction func finish(_ sender: Any) {
        onCompletion?()
    }
    
}

extension ExerciseQuestionsViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exerciseCase?.questions?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(ExerciseQuestionTableViewCell.self)!
        
        let currentRow = indexPath.row
        let question = exerciseCase?.questions?[currentRow]
        cell.configure(
            with: question?.title,
            answer: answers[currentRow],
            correctAnswer: finished ? question?.answer : nil,
            isLast: currentRow + 1 == exerciseCase?.questions?.count,
            onTextChange: { [weak self] answer in
                if answer.isEmpty {
                    self?.answers.removeValue(forKey: currentRow)
                } else {
                    self?.answers[currentRow] = answer
                }
                self?.finishButton.isEnabled = self?.answers.keys.count == self?.exerciseCase?.questions?.count
                DispatchQueue.main.async {
                    UIView.performWithoutAnimation {
                        self?.tableView.beginUpdates()
                        self?.tableView.endUpdates()
                    }
                }
            },
            onNext: { [weak self] in
                let nextRow = currentRow + 1
                if nextRow == self?.exerciseCase?.questions?.count {
                    self?.view.endEditing(true)
                } else {
                    UIView.animate(withDuration: 0.3, animations: {
                        self?.tableView.scrollToRow(
                            at: IndexPath(row: nextRow, section: 0),
                            at: .bottom,
                            animated: false
                        )
                    }, completion: { _ in
                        self?.tableView.cellForRow(at: IndexPath(row: nextRow, section: 0))?.becomeFirstResponder()
                    })
                }
            }
        )
        
        return cell
    }
    
}

extension ExerciseQuestionsViewController: UITableViewDelegate {
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        guard shouldScrollToTopAfterReloading else { return }
        
        shouldScrollToTopAfterReloading = false
        if (exerciseCase?.questions?.count ?? 0) > 0 {
            tableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .bottom, animated: true)
        }
    }
    
}

private extension ExerciseQuestionsViewController {
    
    func setup(){
        view.setDismissKeyboardOnTap()
        setupTableView()
        setupWithData()
    }
    
    func setupTableView(){
        tableView.registerNib(cell: ExerciseQuestionTableViewCell.self)
        tableView.layoutMargins = .only(left: 16, right: 16)
    }
    
    func setupWithData() {
        situationLabel.text = exerciseCase?.text
    }
    
}
