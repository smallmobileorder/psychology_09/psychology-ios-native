//
//  ExerciseQuestionsPresenter.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 28/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol ExerciseQuestionsPresenterInput: class {

}

protocol ExerciseQuestionsPresenterOutput: class {
    var presenter: ExerciseQuestionsPresenterInput? { get set }
}

final class ExerciseQuestionsPresenter {

    weak var output: ExerciseQuestionsPresenterOutput?
    
}

// MARK:- ExerciseQuestionsPresenterInput
extension ExerciseQuestionsPresenter: ExerciseQuestionsPresenterInput {

}
