//
//  ExperimentsListViewProtocol.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 25/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol ExperimentsListViewProtocol: BaseViewProtocol {
    var experiments: [Experiment] { get set }
    var exercises: [Exercise] { get set }
    var onSelectExperiment: ObjectCallback<Experiment>? { get set }
    var onSelectExercise: ObjectCallback<Exercise>? { get set }
}

