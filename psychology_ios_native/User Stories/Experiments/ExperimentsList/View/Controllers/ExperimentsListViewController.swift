//
//  ExperimentsListViewController.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 25/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit
import SwiftyStoreKit

class ExperimentsListViewController: UIViewController, ExperimentsListImageSource, ExperimentsListAssemblable {
    
    var imageTransitionFrom: UIImageView!
    
    var experiments: [Experiment] = []
    var exercises: [Exercise] = []
    var onSelectExperiment: ObjectCallback<Experiment>?
    var onSelectExercise: ObjectCallback<Exercise>?
    
    @IBOutlet private weak var collectionView: UICollectionView!

	var presenter: ExperimentsListPresenterInput?
    var cachedHeaderSize: CGSize?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        collectionView.collectionViewLayout.invalidateLayout()
    }

}

extension ExperimentsListViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        return collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "header", for: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return experiments.count + exercises.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row < experiments.count {
            let cell = collectionView.dequeue(ExperimentCollectionViewCell.self, indexPath: indexPath)!
            
            let experiment = experiments[indexPath.row]
            cell.configure(with: experiment.title)
            
            return cell
        } else {
            let cell = collectionView.dequeue(ExerciseCollectionViewCell.self, indexPath: indexPath)!
            
            let experiment = exercises[indexPath.row - experiments.count]
            cell.configure(with: experiment.title)
            
            return cell
        }
    }
    
}

extension ExperimentsListViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if let cachedSize = cachedHeaderSize { return cachedSize }

        if let headerView = collectionView.visibleSupplementaryViews(ofKind: UICollectionView.elementKindSectionHeader).first {
            // Layout to get the right dimensions
            headerView.layoutIfNeeded()

            // Automagically get the right height
            let height = headerView.subviews.first!.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height

            // return the correct size
            let size = CGSize(width: collectionView.frame.width, height: height)
            cachedHeaderSize = size
            return size
        }

        // You need this because this delegate method will run at least
        // once before the header is available for sizing.
        // Returning zero will stop the delegate from trying to get a supplementary view
        return CGSize(width: 1, height: 1)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch traitCollection.horizontalSizeClass {
        case .regular:
            let width = (collectionView.frame.width - 48) / 2.0
            return CGSize(width: width, height: width)

        default:
            let isExperiment = indexPath.row < experiments.count
            
            let width = collectionView.frame.width - 32
            let height = width * (isExperiment ? 431.0 : 202.0) / 343.0
            return CGSize(width: width, height: height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 16
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as? ExperimentsListImageSource
        imageTransitionFrom = cell?.imageTransitionFrom
        
        if indexPath.row < experiments.count {
            let experiment = experiments[indexPath.row]
            DispatchQueue.main.async { self.onSelectExperiment?(experiment) }
        } else {
            let exercise = exercises[indexPath.row - experiments.count]
            DispatchQueue.main.async { self.onSelectExercise?(exercise) }
        }
    }
    
}

private extension ExperimentsListViewController {
    
    func setup() {
        extendedLayoutIncludesOpaqueBars = false
        edgesForExtendedLayout = [.top, .bottom]
        setupCollectionView()
    }
    
    func setupCollectionView() {
        collectionView.registerNib(cell: ExperimentCollectionViewCell.self)
        collectionView.registerNib(cell: ExerciseCollectionViewCell.self)
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.contentInset = .only(top: 16, left: 16, right: 16)
    }
    
}

private extension ExperimentsListViewController {
    
    func invalidateLayout() {
        cachedHeaderSize = nil
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
}

extension ExperimentsListViewController {
    
//    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        if dismissed is ExperimentDescriptionImageDestination {
//            return FromDescriptionToExperimentsListTransition()
//        }
//
//        return nil
//    }
//    
//    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
//        if presented is ExperimentsListImageSource {
//            return FromExperimentsListToDescriptionTransition()
//        }
//
//        return nil
//    }
    
}
