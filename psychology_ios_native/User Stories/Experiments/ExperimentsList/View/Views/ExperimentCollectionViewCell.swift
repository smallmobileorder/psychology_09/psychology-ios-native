//
//  ExperimentCollectionViewCell.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 26/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class ExperimentCollectionViewCell: UICollectionViewCell, ExperimentsListImageSource {
    
    var imageTransitionFrom: UIImageView! { return posterImageView }

    @IBOutlet private weak var posterImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!

    func configure(with title: ExperimentTitle?) {
        titleLabel.text = title?.text
        posterImageView.image = UIImage(named: title?.image ?? "")
    }

}
