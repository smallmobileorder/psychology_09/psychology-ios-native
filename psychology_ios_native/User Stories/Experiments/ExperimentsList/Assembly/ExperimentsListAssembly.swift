//
//  ExperimentsListAssembly.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 25/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol ExperimentsListAssemblable: ExperimentsListViewProtocol, ExperimentsListPresenterOutput {}

final class ExperimentsListAssembly {
    static func assembly(with output: ExperimentsListPresenterOutput) {
        let presenter  = ExperimentsListPresenter()
    
        presenter.output     = output
        output.presenter     = presenter
    }
}
