//
//  ExperimentsListPresenter.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 25/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol ExperimentsListPresenterInput: class {

}

protocol ExperimentsListPresenterOutput: class {
    var presenter: ExperimentsListPresenterInput? { get set }
}

final class ExperimentsListPresenter {

    weak var output: ExperimentsListPresenterOutput?
    
}

// MARK:- ExperimentsListPresenterInput
extension ExperimentsListPresenter: ExperimentsListPresenterInput {

}
