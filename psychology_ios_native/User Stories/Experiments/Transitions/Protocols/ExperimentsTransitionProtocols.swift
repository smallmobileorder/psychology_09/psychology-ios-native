//
//  ExperimentsTransitionProtocols.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 26/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

protocol ExperimentsListImageSource: UIViewControllerTransitioningDelegate {
    var imageTransitionFrom: UIImageView! { get }
}

protocol ExperimentDescriptionImageDestination {
    var imageTransitionTo: UIImageView! { get }
}
