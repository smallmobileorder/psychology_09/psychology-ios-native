//
//  ExperimentConclusionPresenter.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 26/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol ExperimentConclusionPresenterInput: class {

}

protocol ExperimentConclusionPresenterOutput: class {
    var presenter: ExperimentConclusionPresenterInput? { get set }
}

final class ExperimentConclusionPresenter {

    weak var output: ExperimentConclusionPresenterOutput?
    
}

// MARK:- ExperimentConclusionPresenterInput
extension ExperimentConclusionPresenter: ExperimentConclusionPresenterInput {

}
