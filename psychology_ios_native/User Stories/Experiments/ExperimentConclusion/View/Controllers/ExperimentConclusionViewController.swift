//
//  ExperimentConclusionViewController.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 26/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class ExperimentConclusionViewController: UIViewController, ExperimentConclusionAssemblable {
    
    var experimentTitle: String?
    var experimentConclusion: ExperimentConclusion?
    var nextExperimentTitle: String?
    var onClose: Callback?
    var onNextExperiment: Callback?
    var onAnotherExperiment: Callback?
    var onOpenExercise: Callback?
    var onOpenDiary: Callback?
    var onChooseExperiment: Callback?

	var presenter: ExperimentConclusionPresenterInput?
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var conclusionLabel: UILabel!
    
    @IBOutlet private weak var nextExperimentButton: BrandButton!
    @IBOutlet private weak var anotherExperimentButton: BrandButton!
    @IBOutlet private weak var openExerciseButton: BrandButton!
    @IBOutlet private weak var openDiaryButton: BrandButton!
    @IBOutlet private weak var chooseExperimentButton: BrandButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupWithData()
        
        addNavigationBackButton(selector: #selector(close))
    }

}

private extension ExperimentConclusionViewController {
    
    @IBAction func openNext(_ sender: UIButton) {
        switch sender {
        case nextExperimentButton: onNextExperiment?()
        case anotherExperimentButton: onAnotherExperiment?()
        case openExerciseButton: onOpenExercise?()
        case openDiaryButton: onOpenDiary?()
        case chooseExperimentButton: onChooseExperiment?()
        default: break
        }
    }
    
    @objc func close() {
        onClose?()
    }
    
}

private extension ExperimentConclusionViewController {
    
    func setupWithData() {
        titleLabel.text = experimentTitle
        imageView.image = UIImage(named: experimentConclusion?.image ?? "")
        conclusionLabel.text = experimentConclusion?.text
        
        nextExperimentButton.isHidden = onNextExperiment == nil
        nextExperimentButton.setTitle(nextExperimentTitle, for: .normal)
        anotherExperimentButton.isHidden = onAnotherExperiment == nil
        openExerciseButton.isHidden = onOpenExercise == nil
        openDiaryButton.isHidden = onOpenDiary == nil
        chooseExperimentButton.isHidden = onChooseExperiment == nil
    }
    
}
