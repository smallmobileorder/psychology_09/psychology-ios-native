//
//  ExperimentConclusionViewProtocol.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 26/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol ExperimentConclusionViewProtocol: BaseViewProtocol {
    var experimentTitle: String? { get set  }
    var experimentConclusion: ExperimentConclusion? { get set }
    var nextExperimentTitle: String? { get set }
    var onClose: Callback? { get set }
    var onNextExperiment: Callback? { get set }
    var onAnotherExperiment: Callback? { get set }
    var onOpenExercise: Callback? { get set }
    var onOpenDiary: Callback? { get set }
    var onChooseExperiment: Callback? { get set }
}

