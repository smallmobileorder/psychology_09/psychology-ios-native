//
//  ExperimentConclusionAssembly.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 26/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol ExperimentConclusionAssemblable: ExperimentConclusionViewProtocol, ExperimentConclusionPresenterOutput {}

final class ExperimentConclusionAssembly {
    static func assembly(with output: ExperimentConclusionPresenterOutput) {
        let presenter  = ExperimentConclusionPresenter()
    
        presenter.output     = output
        output.presenter     = presenter
    }
}
