//
//  RecordsListAssembly.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 30/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol RecordsListAssemblable: RecordsListViewProtocol, RecordsListPresenterOutput {}

final class RecordsListAssembly {
    static func assembly(with output: RecordsListPresenterOutput) {
        let presenter  = RecordsListPresenter()
    
        presenter.output     = output
        output.presenter     = presenter
    }
}
