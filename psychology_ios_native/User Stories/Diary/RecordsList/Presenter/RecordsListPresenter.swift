//
//  RecordsListPresenter.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 30/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol RecordsListPresenterInput: class {

}

protocol RecordsListPresenterOutput: PresenterOutputProtocol {
    var presenter: RecordsListPresenterInput? { get set }
}

final class RecordsListPresenter {

    weak var output: RecordsListPresenterOutput?
    
}

// MARK:- RecordsListPresenterInput
extension RecordsListPresenter: RecordsListPresenterInput {

}
