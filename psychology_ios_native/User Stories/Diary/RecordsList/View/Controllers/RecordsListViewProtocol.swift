//
//  RecordsListViewProtocol.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 30/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol RecordsListViewProtocol: BaseViewProtocol {
    var onAdd: ObjectCallback<Date>? { get set }
    var onSetDate: Callback? { get set }
    var onEdit: ObjectCallback<Record>? { get set }
    func update(with date: Date)
    func update(editing record: Record)
    func update(adding record: Record)
}

