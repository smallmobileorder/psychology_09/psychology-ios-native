//
//  RecordsListViewController.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 30/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit
import UPCarouselFlowLayout
import RealmSwift

class EmotionRealm: Object {
    @objc dynamic var type = 0
    @objc dynamic var value = 0
}

class Record: Object {
    @objc dynamic var recordID = UUID().uuidString

    override static func primaryKey() -> String? {
      return "recordID"
    }
    
    @objc dynamic var date = Date()
    @objc dynamic var situatuon: String? = nil
    @objc dynamic var thought: String? = nil
    let emotions = List<EmotionRealm>()
    @objc dynamic var reaction: String? = nil
}

class RecordsListViewController: UIViewController, RecordsListAssemblable {
    
    var onSetDate: Callback?
    var onEdit: ObjectCallback<Record>?
    
    let colors: [UIColor] = [
        .init(rgb: 0xFFCC00),
        .init(rgb: 0xF61FED),
        .init(rgb: 0x55DE52),
        .init(rgb: 0xFF7E20),
        .init(rgb: 0x5A60FA),
        .init(rgb: 0xAF52DE),
        .init(rgb: 0xFF3B30)
    ]

    var onAdd: ObjectCallback<Date>?
    
	var presenter: RecordsListPresenterInput?
    
    @IBOutlet private weak var emptyLabel: UILabel!
    @IBOutlet private weak var arrowImageView: UIImageView!
    
    @IBOutlet private weak var datesCollectionView: UICollectionView!
    @IBOutlet private weak var tableView: UITableView!
    
    private lazy var minDate: Date = {
        var dateComponents = DateComponents()
        dateComponents.year = 2020
        dateComponents.month = 1
        dateComponents.day = 1
        return Calendar.current.date(from: dateComponents)!
    }()
    
    private var sections: [DateComponents: [Record]] = [:]
    private var date = Date()
    private var selectedDate: DateComponents? {
        didSet {
            guard let selectedDate = selectedDate else { return }
            
            emptyLabel.isHidden = !(sections[selectedDate]?.isEmpty ?? true)
            arrowImageView.isHidden = !(sections[selectedDate]?.isEmpty ?? true)
            tableView.reloadData()
        }
    }
    private var selectedRecordIndexPath: IndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let settings = UserDefaults.standard
        if !settings.bool(forKey: "diary_accept") {
            show(message: .message("Раздел Тестирование", "Ведение дневника мыслей позволяет более реалистично оценивать события, которые с вами происходят, а также находить ошибки мышления. В дневнике есть 4 раздела:\nСитуация – описание того, что происходило. Постарайтесь без оценок описать то, что вы видели, слышали, ощущали кожей или внутри своего тела. Например, еду в вагоне метро.\n• Мысли – опишите, о чем вы думали в тот момент или чуть позже в связи с этой ситуацией, как вы восприняли происходящее. Может быть, у вас были какие-то ожидания или вы дали какую-то оценку этому событию. Будьте честны с собой. Например, мне сейчас станет плохо. Я упаду в обморок, окружающие подумают, что я сумасшедший. Как же мне надоел этот страх. Когда он уже пройдет. Хочется ездить спокойно, как другие люди.\n• Эмоции – напишите, что вы чувствовали в связи с этой ситуацией. Эмоция могла быть одна или несколько. Для простоты мы ограничили количество эмоций до 8. По опыту, для эффективной работы достаточно выбрать, к какой эмоции вы сейчас ближе, и обозначить степень её выраженности в процентах. Где 0% - нет этой эмоции, 100% - максимальное переживание этой эмоции. Например, тревога 70% и раздражение 30%\n• Поведение и телесные ощущения – запишите, что вы сделали или захотели сделать в связи с этой ситуацией. Отметьте, какие ощущения в теле у вас возникли. Будем считать, что телесные ощущения – это поведение вашего тела. Например, захотелось убежать, сердце забилось чаще, ладони вспотели. \nВы можете прямо в дневнике давать рациональный ответ на свои автоматические мысли. Если вы не знаете, как это делать, воспользуйтесь разделом «Изменение мыслей». Новая мысль приведет к изменению эмоции, зафиксируйте её в окошке новая эмоция.", "Мой дневник мыслей")) {
                settings.set(true, forKey: "diary_accept")
                settings.synchronize()
            }
        }
        
        let realm = try! Realm()
        let calendar = Calendar.current
        for record in realm.objects(Record.self).sorted(\.date) {
            let clearDate = calendar.dateComponents([.day], from: minDate, to: record.date)
            var oldValues = sections[clearDate] ?? []
            oldValues.append(record)
            sections[clearDate] = oldValues
        }
        
        datesCollectionView.registerNib(cell: RecordDayCollectionViewCell.self)
        
        tableView.registerNib(cell: DiaryRecordTableViewCell.self)
        tableView.contentInset = .only(bottom: 88)
        
        selectedDate = DateComponents(day: collectionView(datesCollectionView, numberOfItemsInSection: 0) - 1)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name("record_delete"), object: nil, queue: .main) { [weak self] notification in
            guard let self = self else { return }
            guard let recordID = notification.object as? String else { return }
            
            var key: DateComponents?
            var index: Int?
            for (k, var value) in self.sections {
                guard let i = value.firstIndex(where: { $0.isInvalidated }) else { continue }
                
                value.remove(at: i)
                
                self.sections[k] = value
                
                key = k
                index = i
            }
            
            guard let k = key, let i = index else { return }
            
            if k == self.selectedDate {
                self.tableView.beginUpdates()
                self.tableView.deleteRows(at: [IndexPath(row: i, section: 0)], with: .automatic)
                self.tableView.endUpdates()
            }
        }
    }
    
    var isFixed = false
    var maxDate: Date?
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        guard !isFixed else { return }
        
        isFixed = true
        
        scrollToEnd()
    }
    
    func scrollToEnd() {
        datesCollectionView.setContentOffset(CGPoint(x: datesCollectionView.contentSize.width - datesCollectionView.frame.width, y: 0), animated: false)
    }
    
    func update(with date: Date) {
        self.date = date
        let calendar = Calendar.current
        selectedDate = calendar.dateComponents([.day], from: minDate, to: date)
        
        guard let index = selectedDate?.day else { return }
        
        if index > (Calendar.current.dateComponents([.day], from: minDate, to: maxDate!).day ?? 0) {
            datesCollectionView.reloadData()
            scrollToEnd()
        } else {
            datesCollectionView.scrollToItem(at: IndexPath(row: index, section: 0), at: .centeredHorizontally, animated: true)
        }
    }
    
    func update(adding record: Record) {
        let calendar = Calendar.current
        let clearDate = calendar.dateComponents([.day], from: minDate, to: record.date)
        
        var oldValues = sections[clearDate] ?? []
        oldValues.append(record)
        sections[clearDate] = oldValues
        
        datesCollectionView.reloadData()
        tableView.reloadData()
        
        emptyLabel.isHidden = true
        arrowImageView.isHidden = true
        
        view.setNeedsLayout()
        view.layoutIfNeeded()
    }
    
    func update(editing record: Record) {
        if let indexPath = selectedRecordIndexPath {
            tableView.reloadRows(at: [indexPath], with: .automatic)
        }
    }
    
}

private extension RecordsListViewController {
    
    @IBAction func setDate() {
        onSetDate?()
    }
    
    @IBAction func addRecord() {
        guard let index = datesCollectionView.centerCellIndexPath else { return }
        
        date = Calendar.current.date(byAdding: .day, value: index.row, to: minDate) ?? date
        
        selectedDate = DateComponents(day: index.row)
        
        onAdd?(date)
   }
    
}

extension RecordsListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let selectedDate = selectedDate else { return 0 }
        
        return sections[selectedDate]?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeue(DiaryRecordTableViewCell.self)!
        
        guard let selectedDate = selectedDate else { return cell }
        
        let record = sections[selectedDate]?[indexPath.row]
        
        cell.situationLabel.text = record?.situatuon
        cell.timeLabel.text = record?.date.string(format: "H:mm")
        cell.thoughtsLabel.text = record?.thought
        
        return cell
    }
    
}

extension RecordsListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let selectedDate = selectedDate,
            let record = sections[selectedDate]?[indexPath.row] else { return }
        
        selectedRecordIndexPath = indexPath
        
        DispatchQueue.main.async {
            self.onEdit?(record)
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        guard let selectedDate = selectedDate, var section = sections[selectedDate] else { return }
        
        if editingStyle == .delete {
            let record = section.remove(at: indexPath.row)
            sections[selectedDate] = section
            
            let id = record.recordID
            
            let realm = try! Realm()
            try! realm.write {
                realm.delete(record)
            }
            tableView.beginUpdates()
            tableView.deleteRows(at: [indexPath], with: .automatic)
            tableView.endUpdates()
            
            NotificationCenter.default.post(name: NSNotification.Name("record_delete"), object: id)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        true
    }
    
}

extension RecordsListViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        maxDate = Date()
        return (Calendar.current.dateComponents([.day], from: minDate, to: maxDate!).day ?? 0) + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeue(RecordDayCollectionViewCell.self, indexPath: indexPath)!
        
        guard let date = Calendar.current.date(byAdding: .day, value: indexPath.row, to: minDate) else { return cell }
        
        var weekday = Calendar.current.component(.weekday, from: date) - 2
        if weekday == -1 {
            weekday = 6
        }
        cell.border = colors[weekday]
        cell.dayLabel.text = date.string(format: "dd")
        cell.monthLabel.text = date.string(format: "LLLL").capitalized
        cell.yearLabel.text = date.string(format: "yyyy")
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedDate = DateComponents(day: indexPath.row)
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        guard let index = selectedDate?.day else { return }
        
        guard let date = Calendar.current.date(byAdding: .day, value: index, to: minDate) else { return }
        
        onAdd?(date)
    }
    
}

extension RecordsListViewController: UICollectionViewDelegateFlowLayout {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        guard !decelerate else { return }
        
        updateTableViewContent()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        updateTableViewContent()
    }
    
    func updateTableViewContent() {
        guard let index = datesCollectionView.centerCellIndexPath else { return }
        
        date = Calendar.current.date(byAdding: .day, value: index.row, to: minDate) ?? date
        
        selectedDate = DateComponents(day: index.row)
    }
    
}

extension UICollectionView {
    var centerPoint: CGPoint {
        CGPoint(x: bounds.midX, y: bounds.midY)
    }
    
    var centerCellIndexPath: IndexPath? {
        indexPathForItem(at: centerPoint)
    }
}
