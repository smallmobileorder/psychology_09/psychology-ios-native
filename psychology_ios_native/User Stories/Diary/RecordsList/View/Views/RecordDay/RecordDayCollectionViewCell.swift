//
//  RecordDayCollectionViewCell.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 30/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class RecordDayCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private weak var circleView: UIView!
    
    @IBOutlet weak var dayLabel: UILabel!
    @IBOutlet weak var monthLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    
    var border: UIColor? {
        didSet {
            setNeedsLayout()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        circleView.setNeedsLayout()
        circleView.layoutIfNeeded()
        circleView.cornerRadius = circleView.frame.width / 2.0
        circleView.layer.borderColor = border?.cgColor
        circleView.layer.borderWidth = 4
        
        addShadow(color: .shadow, opacity: 0.5, radius: 3.0, offset: .zero)
    }

}
