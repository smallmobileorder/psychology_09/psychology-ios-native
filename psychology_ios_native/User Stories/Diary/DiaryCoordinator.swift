//
//  DiaryCoordinator.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 28/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation
import RealmSwift

final class DiaryCoordinator: BaseCoordinator, DiaryCoordinatorOutput {
    
    var finishFlow: Callback?
    var onMindChanging: ObjectCallback<Record>?
    var onMindStart: ObjectCallback<Record>?
    
    private let factory: DiaryFactoryProtocol
    private let coordinatorFactory: CoordinatorFactoryProtocol
    private let parentRouter: Routable
    
    init(with factory: DiaryFactoryProtocol, coordinatorFactory: CoordinatorFactoryProtocol, router: Routable) {
        self.factory = factory
        self.coordinatorFactory = coordinatorFactory
        self.parentRouter = router
    }
    
    private weak var view: RecordsListViewProtocol?
    
    private var rootRouter: Routable?
    private var flowRouter: Routable?
    
}

// MARK:- Coordinatable

extension DiaryCoordinator: Coordinatable {
    
    func start() {
        performFlow()
    }
    
    func addRecord() {
        addRecord(date: Date())
    }
    
    func editRecord(record: Record) {
        show(record)
    }
    
}

// MARK:- Private methods

private extension DiaryCoordinator {
    
    func performFlow() {
        let view = factory.makeRecordsListView()
        view.onAdd = { [weak self] date in
            self?.addRecord(date: date)
        }
        view.onEdit = { [weak self] record in
            self?.show(record)
        }
        view.onSetDate = { [weak self, weak view] in
            self?.getDate(completion: { date in
                guard let date = date else { return }
                view?.update(with: date)
            })
        }
        
        rootRouter = factory.makeRouter(root: view, hideBar: false)
        
        parentRouter.push(rootRouter, animated: false)
        
        self.view = view
    }
    
    func getDate(completion: @escaping (Date?) -> ()) {
        let calendarCoordinator = coordinatorFactory.makeCalendarCoordinator(router: parentRouter)
        calendarCoordinator.finishFlow = { [weak self, weak calendarCoordinator] date in
            self?.removeDependency(calendarCoordinator)
            
            completion(date)
        }
        addDependency(calendarCoordinator)
        calendarCoordinator.start()
    }
    
    func addRecord(date: Date) {
        let calendar = Calendar.current
        let currentDate = calendar.dateComponents([.hour, .minute, .second], from: Date())
        guard let date = calendar.date(
            bySettingHour: currentDate.hour!,
            minute: currentDate.minute!,
            second: currentDate.second!,
            of: date
        )
        else { return }
        
        let view = factory.makeDiaryAddRecordView()
        view.date = date
        view.onAdd = { [weak self] data in
            let realm = try! Realm()
            let record = Record()
            record.date = date
            record.situatuon = data.situation
            record.thought = data.thoughts
            record.emotions.append(objectsIn: data.emotions.map { dic -> EmotionRealm in
                let emo = EmotionRealm()
                emo.type = dic.key.rawValue
                emo.value = dic.value
                return emo
            })
            record.reaction = data.reaction

            try! realm.write {
                realm.add(record)
            }

            self?.rootRouter?.popToRootModule(animated: true)

            self?.view?.update(adding: record)
        }
        view.onClose = { [weak self] in
            self?.rootRouter?.popToRootModule(animated: true)
        }
        view.onStartMind = { [weak self, weak view] data in
            let realm = try! Realm()
            let record = Record()
            record.date = date
            record.situatuon = data.situation
            record.thought = data.thoughts
            record.emotions.append(objectsIn: data.emotions.map { dic -> EmotionRealm in
                let emo = EmotionRealm()
                emo.type = dic.key.rawValue
                emo.value = dic.value
                return emo
            })
            record.reaction = data.reaction

            try! realm.write {
                realm.add(record)
            }
            
            self?.rootRouter?.popToRootModule(animated: true)
            
            self?.view?.update(adding: record)

            self?.onMindStart?(record)
        }

        view.valhalla = true

        rootRouter?.push(view)
    }
    
    func show(_ record: Record) {
        let view = factory.makeDiaryAddRecordView()
        view.record = record
        view.onEdit = { [weak self] data in
            let realm = try! Realm()
            
            try! realm.write {
                record.situatuon = data.situation
                record.thought = data.thoughts
                record.emotions.removeAll()
                record.emotions.append(objectsIn: data.emotions.map { dic -> EmotionRealm in
                    let emo = EmotionRealm()
                    emo.type = dic.key.rawValue
                    emo.value = dic.value
                    return emo})
                record.reaction = data.reaction
            }
            
            self?.view?.update(editing: record)
        }
        view.onClose = { [weak self] in
            self?.rootRouter?.popToRootModule(animated: true)
        }
        view.onGoMind = { [weak self] in
            self?.rootRouter?.popToRootModule(animated: true)
            self?.onMindStart?(record)
        }
        view.onStartMind = { [weak self, weak view] data in
            let realm = try! Realm()
            
            try! realm.write {
                record.situatuon = data.situation
                record.thought = data.thoughts
                record.emotions.removeAll()
                record.emotions.append(objectsIn: data.emotions.map { dic -> EmotionRealm in
                    let emo = EmotionRealm()
                    emo.type = dic.key.rawValue
                    emo.value = dic.value
                    return emo})
                record.reaction = data.reaction
            }
            
            self?.view?.update(editing: record)
            
            self?.rootRouter?.popToRootModule(animated: true)
            
            self?.onMindStart?(record)
        }
        
        view.valhalla = false
        
        rootRouter?.push(view)
    }
    
}
