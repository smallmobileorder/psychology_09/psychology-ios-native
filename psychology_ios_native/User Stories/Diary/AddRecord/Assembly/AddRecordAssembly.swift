//
//  AddRecordAssembly.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 28/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol AddRecordAssemblable: AddRecordViewProtocol, AddRecordPresenterOutput {}

final class AddRecordAssembly {
    static func assembly(with output: AddRecordPresenterOutput) {
        let presenter  = AddRecordPresenter()
    
        presenter.output     = output
        output.presenter     = presenter
    }
}
