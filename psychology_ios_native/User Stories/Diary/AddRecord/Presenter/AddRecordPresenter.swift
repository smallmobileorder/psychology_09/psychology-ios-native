//
//  AddRecordPresenter.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 28/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol AddRecordPresenterInput: class {

}

protocol AddRecordPresenterOutput: class {
    var presenter: AddRecordPresenterInput? { get set }
}

final class AddRecordPresenter {

    weak var output: AddRecordPresenterOutput?
    
}

// MARK:- AddRecordPresenterInput
extension AddRecordPresenter: AddRecordPresenterInput {

}
