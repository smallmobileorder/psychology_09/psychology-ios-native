//
//  AddRecordViewController.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 28/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit
import RealmSwift

struct MyData {
    let situation: String
    let thoughts: String
    let emotions: [EmotionType: Int]
    let reaction: String
}

enum EmotionType: Int, CaseIterable {
    case joy
    case sadness
    case annoyance
    case anxiety
    case disgust
    case interest
    case guilt
    case resentment
    
    func imageName(enabled: Bool) -> String {
        let name: String
        switch self {
        case .joy: name = "joy"
        case .sadness: name = "sadness"
        case .annoyance: name = "annoyance"
        case .anxiety: name = "anxiety"
        case .disgust: name = "disgust"
        case .interest: name = "interest"
        case .guilt: name = "guilt"
        case .resentment: name = "resentment"
        }
        
        return "\(name.capitalized)\(enabled ? "Active" : "NonActive")"
    }
    
    var name: String {
        switch self {
        case .joy: return "Радость"
        case .sadness: return "Печаль"
        case .annoyance: return "Раздражение"
        case .anxiety: return "Тревога"
        case .disgust: return "Отвращение"
        case .interest: return "Интерес"
        case .guilt: return "Вина"
        case .resentment: return "Обида"
        }
    }
    
}

class AddRecordViewController: UIViewController, AddRecordAssemblable {
    
    var onAdd: ObjectCallback<MyData>?
    var onEdit: ObjectCallback<MyData>?
    var onClose: Callback?
    
    var onGoMind: Callback?
    var onStartMind: ObjectCallback<MyData>?
    var onHomeWork: Callback?
    var onDiary: Callback?
    
    var date: Date?
    var record: Record?
    var forceShowStart: Bool = false
    
    var valhalla: Bool = false
    var canEdit: Bool = true
    
//    var situation: String? = ""
//    var thoughts: String? = ""
//    var reaction: String? = ""
    
    var enabledEmotions: [EmotionType: Int] = [:]
    
    
    @IBOutlet weak var dateLabel: UILabel!
    
    
    
    @IBOutlet private weak var startMindButton: UIButton!
    @IBOutlet private weak var startHomeWorkButton: UIButton!
    @IBOutlet private weak var startDiaryButton: UIButton!
    @IBOutlet private weak var situationSectionLabel: UILabel!
    @IBOutlet private weak var thoughtsSectionLabel: UILabel!
    @IBOutlet private weak var emotionsSectionLabel: UILabel!
    @IBOutlet private weak var reactionSectionLabel: UILabel!
    
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet var selectViews: [UIImageView]!
    @IBOutlet var emotionNameLabels: [UILabel]!
    @IBOutlet var percentageLabels: [UILabel]!
    
    @IBOutlet private weak var saveButton: UIButton!
    @IBOutlet private weak var showMindChangesButton: UIButton!
    @IBOutlet private weak var scrollView: AutoScrollView!
    @IBOutlet private weak var sliderSectionView: UIStackView!
    @IBOutlet private weak var emotionNameLabel: UILabel!
    @IBOutlet private weak var slider: UISlider!
    @IBOutlet private weak var emotionPercentageLabel: UILabel!
    
    @IBOutlet private weak var sliderContainer: UIView!
    
    
    @IBOutlet private weak var sliderContainerHeightConstraint: NSLayoutConstraint!
    
    
    
    @IBOutlet private weak var situationTextView: CustomTextView!
    @IBOutlet private weak var situationLabel: UILabel!
    @IBOutlet private weak var thoughtsTextView: CustomTextView!
    @IBOutlet private weak var thoughtsLabel: UILabel!
    @IBOutlet private weak var reactionTextView: CustomTextView!
    @IBOutlet private weak var reactionLabel: UILabel!
    
    @IBOutlet private weak var allEmotionsSection: UIStackView!
    @IBOutlet private weak var myEmotionsSection: UIStackView!
    
    @IBOutlet var labelHorizontalConstraints: [NSLayoutConstraint]!
    @IBOutlet var labelVerticalConstraints: [NSLayoutConstraint]!
    
    private var currentPosition: Int?

	var presenter: AddRecordPresenterInput?
    
	override func viewDidLoad() {
        super.viewDidLoad()
        
        if let record = record {
            for emotion in record.emotions {
                enabledEmotions[EmotionType(rawValue: emotion.type)!] = emotion.value
            }
        }
        
        showMindChangesButton.isHidden = onGoMind == nil
        startMindButton.isHidden = onStartMind == nil
        startHomeWorkButton.isHidden = onHomeWork == nil
        startDiaryButton.isHidden = onDiary == nil
        
        dateLabel.text = (date ?? record?.date)?.string(format: "d MMMM, yyyy")
        
//        situation = record?.situatuon
//        thoughts = record?.thought
//        reaction = record?.reaction
        
        labelVerticalConstraints.forEach { $0.constant = valhalla ? 11 : 0 }
        labelHorizontalConstraints.forEach { $0.constant = valhalla ? 16 : 0 }
        
        set(editing: valhalla)
        
        setup()
    }
    
    @objc func close() {
        showAlertСontroller(
            title: "Вы действительно хотите выйти из заполнения ситуации?",
            actions: [
                (title: "Отмена", handler: {}),
                (title: "Да", handler: { [weak self] in self?.onClose?() })
            ]
        )
    }
    
    @IBAction private func startMind() {
        record?.emotions
        let data = MyData(
            situation: (situationTextView.text ?? "").isEmpty ? record?.situatuon ?? "" : situationTextView.text ?? "",
            thoughts: (thoughtsTextView.text ?? "").isEmpty ? record?.thought ?? "" : thoughtsTextView.text ?? "",
            emotions: enabledEmotions.isEmpty ? (record?.emotions ?? List<EmotionRealm>()).reduce(into: [:], { dict, rea in
                dict[EmotionType(rawValue: rea.type)!] = rea.value
            }) : enabledEmotions,
            reaction: (reactionTextView.text ?? "").isEmpty ? record?.reaction ?? "" : reactionTextView.text ?? ""
        )
        onStartMind?(data)
    }
    
    @IBAction private func showMind() {
        onGoMind?()
    }
    
    @IBAction private func startHomeWork() {
        onHomeWork?()
    }
    
    @IBAction private func startDiary() {
        onDiary?()
    }
    
}

extension AddRecordViewController {
    
    func set(editing: Bool) {
        valhalla = editing
        
        if editing {
            addNavigationBackButton(selector: #selector(close))
        } else {
            navigationItem.leftBarButtonItems = []
        }
        
        saveButton.isHidden = !canEdit
        saveButton.setTitle(editing ? "Сохранить": "Редактировать", for: .normal)
        showMindChangesButton.isHidden = editing || onGoMind == nil
        startMindButton.isHidden = !editing && !forceShowStart || onStartMind == nil
        
        situationSectionLabel.text = editing ? "Что произошло? Опишите максимально объективно." : "Ситуация:"
        thoughtsSectionLabel.text = editing ? "О чем вы думали?" : "Мысли:"
        emotionsSectionLabel.text = editing ? "Выберите одну или несколько эмоций. Оцените степень выраженности в %." : "Эмоции:"
        reactionSectionLabel.text = editing ? "Что вы сделали или что вам захотелось сделать? Как отреагировало ваше тело?" : "Поведение и телесные ощущения:"
        
        situationTextView.isHidden = !editing
        thoughtsTextView.isHidden = !editing
        reactionTextView.isHidden = !editing
        
        situationLabel.isHidden = editing
        thoughtsLabel.isHidden = editing
        reactionLabel.isHidden = editing
        
        labelVerticalConstraints.forEach { $0.constant = editing ? 11 : 0 }
        labelHorizontalConstraints.forEach { $0.constant = editing ? 16 : 0 }
        
        let enabled = enabledEmotions.keys.sorted(by: { $0.rawValue < $1.rawValue })
        
        myEmotionsSection.subviews.dropLast().enumerated().forEach { $1.isHidden = $0 >= enabled.count }
        
        for (index, emotionType) in zip(8 ... 10, enabled) {
            buttons[index].setImage(UIImage(named: emotionType.imageName(enabled: true)), for: .normal)
            let percent = enabledEmotions[emotionType]!
            emotionNameLabels[index - 8].text = emotionType.name
            percentageLabels[index].isHidden = false
            percentageLabels[index].text = percent.description.appending("%")
        }
        
        for index in (8 + enabled.count) ..< 11 {
            buttons[index].setImage(nil, for: .normal)
            emotionNameLabels[index - 8].text = ""
            percentageLabels[index].text = ""
        }
        
        allEmotionsSection.isHidden = !editing
        myEmotionsSection.superview?.isHidden = editing
        
        sliderContainer.isHidden = !editing
        
        sliderContainerHeightConstraint.constant = editing ? 74 : 0
        
//
//        let def = UIEdgeInsets(top: 11, left: 16, bottom: 11, right: 16)
//        let hidden = UIEdgeInsets(top: 11, left: 0, bottom: 11, right: -40)
//        situationTextView.layoutMargins = editing ? def : hidden
//        thoughtsTextView.layoutMargins = editing ? def : hidden
//        reactionTextView.layoutMargins = editing ? def : hidden
        
//        situationLabel.isHidden = true
//        thoughtsLabel.isHidden = true
//        reactionLabel.isHidden = true
        
        if let record = record, editing {
            situationTextView.text = record.situatuon
            thoughtsTextView.text = record.thought
            reactionTextView.text = record.reaction
        }
        if let record = record {
            situationLabel.text = record.situatuon
            thoughtsLabel.text = record.thought
            reactionLabel.text = record.reaction
        }
        
        view.setNeedsLayout()
        view.layoutIfNeeded()
    }
    
}

private extension AddRecordViewController {
    
    @IBAction func performAction(_ sender: Any) {
        view.endEditing(true)
        
        let data = MyData(
            situation: situationTextView.text ?? "",
            thoughts: thoughtsTextView.text ?? "",
            emotions: enabledEmotions,
            reaction: reactionTextView.text ?? ""
        )
        
        if let onAdd = onAdd {
            onAdd(data)
            return
        }
        
        if onEdit != nil && valhalla {
            onEdit?(data)
        }
        
        if !valhalla {
            situationTextView.text = record?.situatuon
            thoughtsTextView.text = record?.thought
            reactionTextView.text = record?.reaction
        }
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: .transitionCrossDissolve, animations: {
            self.set(editing: !self.valhalla)
            self.view.layoutIfNeeded()
        }) { _ in
            self.scrollView.setContentOffset(.zero, animated: true)
        }
    }
    
    @IBAction func updateCurrentValue(_ sender: Any) {
        guard let currentPosition = currentPosition else { return }
        
        let value = Int(slider.value)
        let percentFormattedValue = value.description.appending("%")
        emotionPercentageLabel.text = percentFormattedValue
        percentageLabels[currentPosition].text = percentFormattedValue
        
        let emotion = EmotionType.allCases[currentPosition]
        
        enabledEmotions[emotion] = value
    }
    
    @objc func setEmotion(_ sender: UIButton) {
        defer { updateSaveButton() }
        
        guard let position = buttons.firstIndex(of: sender) else { return }
        
        if let currentPosition = currentPosition {
            percentageLabels[currentPosition].isHidden = false
            selectViews[currentPosition].isHidden = true
        }
        
        let selectedEmotion = EmotionType.allCases[position]
        
        let percentageLabel = percentageLabels[position]
        let selectView = selectViews[position]
        
        if let _ = enabledEmotions.removeValue(forKey: selectedEmotion) {
            sender.setImage(UIImage(named: selectedEmotion.imageName(enabled: false)), for: .normal)
            percentageLabel.isHidden = true
            selectView.isHidden = true
            sliderSectionView.isHidden = true
            
            currentPosition = nil
            
            return
        } else if enabledEmotions.count > 2 {
            return
        }
        
        sender.setImage(UIImage(named: selectedEmotion.imageName(enabled: true)), for: .normal)
        percentageLabel.isHidden = true
        selectView.isHidden = false
        
        sliderSectionView.isHidden = false
        slider.value = 1
        emotionPercentageLabel.text = "1%"
        percentageLabel.text = "1%"
        emotionNameLabel.text = selectedEmotion.name
        
        enabledEmotions[selectedEmotion] = 1
        
        currentPosition = position
    }
    
    @objc func dismissEmotionPicker(tap: UITapGestureRecognizer) {
        guard !slider.bounds.contains(tap.location(in: slider)) else { return }
        
        sliderSectionView.isHidden = true
        
        if let currentPosition = currentPosition {
            percentageLabels[currentPosition].isHidden = false
            selectViews[currentPosition].isHidden = true
        }
    }
    
}

private extension AddRecordViewController {
    
    func setup() {
        for ((button, label), emotionType) in zip(zip(buttons, percentageLabels), EmotionType.allCases) {
            let value = enabledEmotions[emotionType]
            button.setImage(UIImage(named: emotionType.imageName(enabled: value != nil)), for: .normal)
            label.isHidden = value == nil
            label.text = value?.description.appending("%")
        }
        
        for index in 0 ... 7 {
            buttons[index].addTarget(self, action: #selector(setEmotion(_:)), for: .touchUpInside)
        }
        
        sliderSectionView.isHidden = true
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissEmotionPicker))
        gestureRecognizer.delegate = self
        scrollView.addGestureRecognizer(gestureRecognizer)
        scrollView.bounces = false
        
        updateSaveButton()
    }
    
    
    
    func updateSaveButton() {
        saveButton.isEnabled =
            !enabledEmotions.isEmpty &&
            !situationTextView.text.isEmpty &&
            !thoughtsTextView.text.isEmpty &&
            !reactionTextView.text.isEmpty || !valhalla
        startMindButton.isEnabled = saveButton.isEnabled
        showMindChangesButton.isEnabled = saveButton.isEnabled
    }
    
}

extension AddRecordViewController: UIScrollViewDelegate {
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
//        UIView.animate(withDuration: 0.3, animations: {
//            self.set(editing: !self.isEditing)
//            self.view.setNeedsLayout()
//        })
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        
    }
    
}

extension AddRecordViewController: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        true
    }
    
}

extension AddRecordViewController: UITextViewDelegate {
    
    func textViewDidChange(_ textView: UITextView) {
        updateSaveButton()
    }
    
}
