//
//  AddRecordViewProtocol.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 28/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol AddRecordViewProtocol: BaseViewProtocol {
    var date: Date? { get set }
    var record: Record? { get set }
    var valhalla: Bool { get set }
    var canEdit: Bool { get set }
    
    var onGoMind: Callback? { get set }
    var onStartMind: ObjectCallback<MyData>? { get set }
    var onHomeWork: Callback? { get set }
    var onDiary: Callback? { get set }
    var forceShowStart: Bool { get set }
    
    var onAdd: ObjectCallback<MyData>? { get set }
    var onEdit: ObjectCallback<MyData>? { get set }
    var onClose: Callback? { get set }
}

