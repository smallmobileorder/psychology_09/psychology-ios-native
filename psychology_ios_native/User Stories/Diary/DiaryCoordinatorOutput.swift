//
//  DiaryCoordinatorOutput.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 28/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

protocol DiaryCoordinatorOutput: class {
    var finishFlow: Callback? { get set }
    var onMindChanging: ObjectCallback<Record>? { get set }
    var onMindStart: ObjectCallback<Record>? { get set }
}
