//
//  AppCoordinator.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 16/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

enum LaunchInstructor {
    
    case main
    
    static func setup() -> LaunchInstructor {
        return .main
    }
}

final class AppCoordinator: BaseCoordinator {
    
    private let factory: CoordinatorFactoryProtocol
    private let router: Routable
    
    private var instructor: LaunchInstructor {
        return LaunchInstructor.setup()
    }
    
    init(router: Routable, factory: CoordinatorFactory) {
        self.router = router
        self.factory = factory
    }
}

// MARK:- Coordinatable

extension AppCoordinator: Coordinatable {
    
    func start() {
        performStart(content: nil)
    }
    
    func start(content: PushNotificatonContent) {
        performStart(content: content)
    }
    
}

// MARK:- Private methods

private extension AppCoordinator {
    
    func performStart(content: PushNotificatonContent?) {
        switch instructor {
        case .main:
            performMainFlow()
        }
    }
    
    func performMainFlow() {
        let coordinator = factory.makeHomeCoordinator(router: router, coordinatorFactory: factory)
        coordinator.finishFlow = { [unowned self, unowned coordinator] in
            self.removeDependency(coordinator)
            self.start()
        }
        addDependency(coordinator)
        coordinator.start()
    }
}
