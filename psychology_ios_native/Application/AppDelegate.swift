//
//  AppDelegate.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit
import CoreData
import MaterialComponents
import RealmSwift
import Firebase
import SwiftyStoreKit
import KeychainAccess

extension Notification.Name {
    static let purchased = Notification.Name("purchased")
}

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    let keychain = Keychain(service: "com.example.github-token")
    
    var isPurchased: Bool {
        get {
            (keychain["ispur"] ?? "false") == "true"
        }
        set {
            keychain["ispur"] = newValue ? "true" : "false"
        }
    }
    
    private var appCoordinator: AppCoordinator?

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        Crashlytics.crashlytics().setCrashlyticsCollectionEnabled(true)
        
        let config = Realm.Configuration(
          schemaVersion: 4,
          migrationBlock: { migration, oldSchemaVersion in
            if oldSchemaVersion == 0 {
                migration.enumerateObjects(ofType: InfoAboutRealm.className()) { (_, newPerson) in
                    newPerson?["canSend"] = false
                }
            } else if oldSchemaVersion == 1 {
                migration.enumerateObjects(ofType: InfoAboutRealm.className()) { (_, newPerson) in
                    newPerson?["email"] = ""
                }
            } else if oldSchemaVersion == 2 {
                migration.enumerateObjects(ofType: Record.className()) { (_, record) in
                    record?["recordID"] = UUID().uuidString
                }
            } else if oldSchemaVersion == 3 {
               migration.enumerateObjects(ofType: CatastrhHomeWork.className()) { (_, dz) in
                    dz?["worstHarmPercent"] = 1
                    dz?["bestHarmPercent"] = 1
               }
           }
          })
        Realm.Configuration.defaultConfiguration = config
        
        FirebaseApp.configure()
        
        if UserDefaults.standard.bool(forKey: "onboard") {
            start()
        } else {
            window = UIWindow(frame: UIScreen.main.bounds)
            let on = FirstViewController()
            on.completion = { [weak self] in
                self?.start()
                UserDefaults.standard.set(true, forKey: "onboard")
            }
            window?.rootViewController = on
            window?.makeKeyAndVisible()
        }
        
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            for purchase in purchases {
                switch purchase.transaction.transactionState {
                case .purchased, .restored:
                    if purchase.needsFinishTransaction {
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                    (UIApplication.shared.delegate as? AppDelegate)?.isPurchased = true
                case .failed, .purchasing, .deferred:
                    (UIApplication.shared.delegate as? AppDelegate)?.isPurchased = false
                }
            }
        }
        
        SwiftyStoreKit.restorePurchases(atomically: true) { results in
            if results.restoreFailedPurchases.count > 0 {
                print("Restore Failed: \(results.restoreFailedPurchases)")
            }
            else if results.restoredPurchases.count > 0 {
                (UIApplication.shared.delegate as? AppDelegate)?.isPurchased = true
            }
            else {
                (UIApplication.shared.delegate as? AppDelegate)?.isPurchased = false
            }
        }
        
        return true
    }
    
    func start() {
        window?.removeFromSuperview()
        window = UIWindow(frame: UIScreen.main.bounds)
        let router = ModulesFactory().makeRouter(root: nil)
        window?.rootViewController = router.toPresent
        window?.makeKeyAndVisible()
        
        setupAppearance()
        
        appCoordinator = AppCoordinator(router: router, factory: CoordinatorFactory())
        appCoordinator?.start()
    }
    
}

private extension AppDelegate {
    
    func setupAppearance() {
//        if #available(iOS 13.0, *) {
//        } else {
//           let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
//           statusBar?.backgroundColor = .active
//        }
        
        UITextField.appearance().tintColor = .active
        UITextField.appearance().textColor = .darkSystemGray5
        UITextField.appearance().font = .systemFont(ofSize: 17)
        
//        UIButton.appearance().tintColor = .active
        
        UIImageView.appearance().tintColor = .active
        
        UINavigationBar.appearance().tintColor = .active
        UITabBar.appearance().tintColor = .active
        UITabBar.appearance().unselectedItemTintColor = .lightSystemGray
        
        CustomTextView.appearance().layoutMargins = UIEdgeInsets(top: 11, left: 16, bottom: 11, right: 16)
        CustomTextView.appearance().font = .systemFont(ofSize: 17)
        CustomTextView.appearance().textColor = .darkSystemGray5
        CustomTextView.appearance().tintColor = .active
        CustomTextView.appearance().cornerRadius = 8
        
        CustomTextField.appearance().backgroundColor = .white
        
        BrandButton.appearance().cornerRadius = 8
        
        CustomSlider.appearance().tintColor = .active
        
        MDCFloatingButton.appearance().backgroundColor = .active
        MDCFloatingButton.appearance().tintColor = .back
    }
    
}
