//
//  UserInfoService.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 24/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation
import RealmSwift

class UserInfoService {
    var info: InfoAboutRealm? {
        let realm = try! Realm()
        return realm.objects(InfoAboutRealm.self).first
    }
}
