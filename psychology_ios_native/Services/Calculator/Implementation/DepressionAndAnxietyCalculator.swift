//
//  DepressionAndAnxietyCalculator.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 16/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

struct DepressionAndAnxietyCalculator: TestCalculator {
    
    let test: Test
    
    private static let anxietyIndexies: Set<Int> = [
        1, 3, 5, 7, 9, 11, 13
    ]
    
    private static let anxietyAdvices = [
        "У вас нет достоверно выраженных симптомов тревоги.",
        "У вас присутствуют достоверно выраженные симптомы тревоги, но их выраженность не достигает состояния болезни. Рекомендуем обратиться к специалисту для профилактики заболевания и продолжить работу в данном приложении.",
        "У вас присутствуют выраженные симптомы тревоги, их выраженность достигает состояния болезни. Рекомендуем обратиться к психотерапевту на консультацию и продолжить работу в данном приложении."
    ]
    
    private static let depressionIndexies: Set<Int> = [
        2, 4, 6, 8, 10, 12, 14
    ]

    private static let depressionAdvices = [
        "У вас нет достоверно выраженных симптомов депрессии.",
        "У вас присутствуют достоверно выраженные симптомы депрессии, но их выраженность не достигает состояния болезни. Рекомендуем обратиться к специалисту для профилактики заболевания и продолжить работу в данном приложении.",
        "У вас присутствуют выраженные симптомы депрессии, их выраженность достигает состояния болезни. Рекомендуем обратиться к психотерапевту на консультацию и продолжить работу в данном приложении."
    ]
    
    func calculate(summary: TestAnswerSummary) -> TestResult {
        let answers = summary.answers
        
        let anxiety = calculate(
            indexies: DepressionAndAnxietyCalculator.anxietyIndexies,
            answers: answers
        )
        
        let depression = calculate(
            indexies: DepressionAndAnxietyCalculator.depressionIndexies,
            answers: answers
        )
        
        return TestResult(test: test, results: [
            makeResultDescription(for: "тревоги", advices: DepressionAndAnxietyCalculator.anxietyAdvices, sum: anxiety),
            makeResultDescription(for: "депрессии", advices: DepressionAndAnxietyCalculator.depressionAdvices, sum: depression),
        ], res: Res(title: "ГШТиД", pairs: [Pair(key: "ГШТиД шкала тревоги", value: anxiety), Pair(key: "ГШТиД шкала депрессии", value: depression)]))
    }
    
}

private extension DepressionAndAnxietyCalculator {
    
    func calculate(indexies: Set<Int>, answers: [TestAnswer]) -> Int {
        var sum = 0
        
        for sumIndex in indexies {
            sum += answers[sumIndex - 1].weight
        }
        
        return sum
    }
    
    func makeResultDescription(for category: String, advices: [String], sum: Int) -> ResultDescription {
        let title = "Вы набрали \(sum.markFormatted) по шкале \(category)"
        
        let advice: String
        
        switch sum {
        case ...7: advice = advices[0]
        case 8 ... 10: advice = advices[1]
        case 11...: advice = advices[2]
        default: fatalError()
        }
        
        return ResultDescription(title: title, advice: advice)
    }
    
}
