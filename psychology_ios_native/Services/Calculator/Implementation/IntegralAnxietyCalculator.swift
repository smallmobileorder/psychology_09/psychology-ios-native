//
//  IntegralAnxietyCalculator.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 16/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

struct IntegralAnxietyCalculator: TestCalculator {
    
    let test: Test
    let info: InfoAboutRealm
    
    private static let edAdvices: [String] = [
        "Тревога не доставляет вам эмоционального дискомфорта.",
        "Тревога доставляет вам умеренный эмоциональный дискомфорт.",
        "Тревога влияет на ваше настроение, снижая его. В результате возникает неудовлетворенность жизненной ситуацией, эмоциональной напряженностью, периодическое беспокойство."
    ]
    
    private static let astAdvices: [String] = [
        "Тревога не сопровождается усталостью и быстрой утомляемостью.",
        "Тревога периодически приводит к возникновению усталости, но вы достаточно хорошо восстанавливаете свои силы.",
        "Тревога влияет на ваше самочувствие. В результате возникает усталость, расстройства сна, вялость и пассивность, быстрая утомляемость."
    ]
    
    private static let fobAdvices: [String] = [
        "У вас низкий уровень тревоги и страхов.",
        "Уровень вашей тревоги и страхов в пределах нормы.",
        "Ваша тревога сопровождается ощущениями непонятной угрозы, неуверенности в себе, собственной бесполезности. Вы не всегда можете сформулировать источник своих тревог, как будто страх это хроническое состояние, периодически возрастающее в зависимости от внутреннего состояния или обострения внешней ситуации."
    ]
    
    private static let opAdvices: [String] = [
        "Представления о будущем не вызывают у вас повышения тревоги.",
        "Вы периодически испытываете тревогу о будущем, но умеете достаточно реалистично оценить степень угрозы.",
        "Ваши страхи направлены не только на текущее положение дел, но и на перспективу. Вы озабочены будущим, что еще больше усиливает тревогу и эмоциональную чувствительность."
    ]
    
    private static let szAdvices: [String] = [
        "Взаимодействие с окружающими людьми не вызывает у вас тревогу.",
        "Тревоги, связанные с социальными контактами находятся у вас на уровне нормы.",
        "Основные проявления вашей тревожности находятся в сфере социальных контактов. Окружающие люди могут доставлять вам напряжение и усиливать неуверенность в себе."
    ]
    
    func calculate(summary: TestAnswerSummary) -> TestResult {
        let weights = summary.answers.map { $0.weight }
        
        let situationWeights = Array(weights.dropLast(15))
        let situationStan = calculateStan(situationWeights)
        
        let personalWeights = Array(weights.dropFirst(15))
        let personalStan = calculateStan(personalWeights)
        
        var results: [ResultDescription] = []
        
        let sitAnswers = Array(summary.answers.dropLast(15))
        let persAnswers = Array(summary.answers.dropFirst(15))
        
        let sitMarks = ["ЭД", "АСТ", "ФОБ", "ОП", "СЗ"]
        .map { self.calculateMark(for: $0, additional: self.calculateAddition(for: $0, answers: sitAnswers)) }
        let persMarks = ["ЭД", "АСТ", "ФОБ", "ОП", "СЗ"]
        .map { self.calculateMark(for: $0, additional: self.calculateAddition(for: $0, answers: persAnswers)) }
        
        results.append(
            ResultDescription(
                title: "Результаты в баллах",
                advice:
"""
В настоящее время:
Эмоциональный дискомфорт: \(sitMarks[0])
Астенический компонент тревоги: \(sitMarks[1])
Фобический компонент тревоги: \(sitMarks[2])
Тревожная оценка переспективы: \(sitMarks[3])
Социальная защищенность: \(sitMarks[4])
Общий балл тревоги: \(situationStan)

На протяжении последнего года:
Эмоциональный дискомфорт: \(persMarks[0])
Астенический компонент тревоги: \(persMarks[1])
Фобический компонент тревоги: \(persMarks[2])
Тревожная оценка переспективы: \(persMarks[3])
Социальная защищенность: \(persMarks[4])
Общий балл тревоги: \(personalStan)
"""
            )
        )
        
        results.append(makeResultDescription(marks: persMarks, isSit: false, stan: personalStan))
        results.append(makeResultDescription(marks: sitMarks, isSit: true, stan: situationStan))
        
        var pairs: [Pair] = []
        
        pairs.append(Pair(key: "ИТТ ситуативный ЭД", value: sitMarks[0]))
        pairs.append(Pair(key: "ИТТ ситуативный АСТ", value: sitMarks[1]))
        pairs.append(Pair(key: "ИТТ ситуативный ФОБ", value: sitMarks[2]))
        pairs.append(Pair(key: "ИТТ ситуативный ОП", value: sitMarks[3]))
        pairs.append(Pair(key: "ИТТ ситуативный СЗ", value: sitMarks[4]))
        pairs.append(Pair(key: "ИТТ ситуативный общий балл", value: situationStan))
        pairs.append(Pair(key: "ИТТ личностный ЭД", value: persMarks[0]))
        pairs.append(Pair(key: "ИТТ личностный АСТ", value: persMarks[1]))
        pairs.append(Pair(key: "ИТТ личностный ФОБ", value: persMarks[2]))
        pairs.append(Pair(key: "ИТТ личностный ОП", value: persMarks[3]))
        pairs.append(Pair(key: "ИТТ личностный СЗ", value: persMarks[4]))
        pairs.append(Pair(key: "ИТТ личностный общий балл", value: personalStan))
        
        return TestResult(test: test, results: results, res: Res(title: "ИТТ", pairs: pairs))
    }
    
}

private extension IntegralAnxietyCalculator {
    
    func calculateStan(_ weights: [Int]) -> Int {
        let sum = weights.reduce(0, +)
        switch (info.sex, info.age) {
        case (.woman, ...17):
            switch sum {
            case ...6:    return 1
            case 7...8:   return 2
            case 9...10:  return 3
            case 11...12: return 4
            case 13...16: return 5
            case 17...21: return 6
            case 22...25: return 7
            case 26...30: return 8
            default:      return 9
            }
        
        default:
            switch sum {
            case ...6:    return 1
            case 7...8:   return 2
            case 9:       return 3
            case 10...11: return 4
            case 12...14: return 5
            case 16...18: return 6
            case 19...22: return 7
            case 23...26: return 8
            default:      return 9
            }
        }
    }
    
    func calculateAddition(for category: String, answers: [TestAnswer]) -> Int {
        switch category {
        case "ЭД":
            return [
                answers[0],
                answers[1],
                answers[3],
                answers[5]
            ].enumerated()
            .map { index, answer in
                let weights = [
                    [0, 25, 49, 74],
                    [0, 24, 49, 73],
                    [0, 27, 53, 80],
                    [0, 24, 49, 73]
                ]
                return weights[index][answer.weight]
            }.reduce(0, +)
            
        case "АСТ":
            return [
                answers[7],
                answers[12],
                answers[13],
            ].enumerated()
            .map { index, answer in
                let weights = [
                    [0, 30, 61, 91],
                    [0, 41, 81, 122],
                    [0, 29, 58, 87]
                ]
                return weights[index][answer.weight]
            }.reduce(0, +)
            
        case "ФОБ":
            return [
                answers[6],
                answers[8],
                answers[11],
            ].enumerated()
            .map { index, answer in
                let weights = [
                    [0, 37, 74, 111],
                    [0, 28, 56, 85],
                    [0, 29, 58, 87]
                ]
                return weights[index][answer.weight]
            }.reduce(0, +)
            
        case "ОП":
            return [
                answers[2],
                answers[4],
                answers[14],
            ].enumerated()
            .map { index, answer in
                let weights = [
                    [0, 37, 74, 110],
                    [0, 32, 65, 98],
                    [0, 31, 61, 92]
                ]
                return weights[index][answer.weight]
            }.reduce(0, +)
            
        case "СЗ":
            return [
                answers[9],
                answers[10]
            ].enumerated()
            .map { index, answer in
                let weights = [
                    [0, 57, 114, 171],
                    [0, 43, 86, 129]
                ]
                return weights[index][answer.weight]
            }.reduce(0, +)
            
        default: fatalError()
        }
    }
    
    func calculateMark(for category: String, additional: Int) -> Int {
        let mark: Int
        switch (info.sex, info.age) {
        case (.woman, ...17):
            switch category {
            case "ЭД":
                switch additional {
                case ...42: mark = 1
                case 43 ... 58: mark = 2
                case 59 ... 75: mark = 3
                case 76 ... 92: mark = 4
                case 93 ... 117: mark = 5
                case 118 ... 150: mark = 6
                case 151 ... 183: mark = 7
                case 184 ... 217: mark = 8
                default: mark = 9
                }
                
            case "АСТ":
                switch additional {
                case ...31: mark = 1
                case 32 ... 44: mark = 2
                case 45 ... 57: mark = 3
                case 58 ... 70: mark = 4
                case 71 ... 95: mark = 5
                case 96 ... 132: mark = 6
                case 133 ... 169: mark = 7
                case 170 ... 206: mark = 8
                default: mark = 9
                }
                
            case "ФОБ":
                switch additional {
                case ...16: mark = 1
                case 17 ... 23: mark = 2
                case 24 ... 29: mark = 3
                case 30 ... 36: mark = 4
                case 37 ... 61: mark = 5
                case 62 ... 104: mark = 6
                case 105 ... 148: mark = 7
                case 149 ... 191: mark = 8
                default: mark = 9
                }
                
            case "ОП":
                switch additional {
                case ...53: mark = 1
                case 54 ... 75: mark = 2
                case 76 ... 97: mark = 3
                case 98 ... 118: mark = 4
                case 119 ... 143: mark = 5
                case 144 ... 172: mark = 6
                case 173 ... 200: mark = 7
                case 201 ... 228: mark = 8
                default: mark = 9
                }
                
            case "СЗ":
                switch additional {
                case ...60: mark = 1
                case 61 ... 85: mark = 2
                case 86 ... 109: mark = 3
                case 110 ... 134: mark = 4
                case 135 ... 159: mark = 5
                case 160 ... 184: mark = 6
                case 185 ... 210: mark = 7
                case 211 ... 235: mark = 8
                default: mark = 9
                }
                
            default: fatalError()
            }
        default:
            switch category {
            case "ЭД":
                switch additional {
                case ...34: mark = 1
                case 35 ... 48: mark = 2
                case 49 ... 62: mark = 3
                case 63 ... 76: mark = 4
                case 77 ... 100: mark = 5
                case 101 ... 137: mark = 6
                case 138 ... 173: mark = 7
                case 174 ... 209: mark = 8
                default: mark = 9
                }
                
            case "АСТ":
                switch additional {
                case ...26: mark = 1
                case 27 ... 36: mark = 2
                case 37 ... 47: mark = 3
                case 48 ... 57: mark = 4
                case 58 ... 82: mark = 5
                case 83 ... 122: mark = 6
                case 123 ... 161: mark = 7
                case 162 ... 201: mark = 8
                default: mark = 9
                }
                
            case "ФОБ":
                switch additional {
                case ...13: mark = 1
                case 14 ... 19: mark = 2
                case 20 ... 24: mark = 3
                case 25 ... 29: mark = 4
                case 30 ... 54: mark = 5
                case 55 ... 99: mark = 6
                case 100 ... 144: mark = 7
                case 145 ... 188: mark = 8
                default: mark = 9
                }
                
            case "ОП":
                switch additional {
                case ...44: mark = 1
                case 45 ... 62: mark = 2
                case 63 ... 80: mark = 3
                case 81 ... 97: mark = 4
                case 98 ... 122: mark = 5
                case 123 ... 155: mark = 6
                case 156 ... 187: mark = 7
                case 188 ... 219: mark = 8
                default: mark = 9
                }
                
            case "СЗ":
                switch additional {
                case ...50: mark = 1
                case 51 ... 70: mark = 2
                case 71 ... 90: mark = 3
                case 91 ... 110: mark = 4
                case 111 ... 135: mark = 5
                case 136 ... 165: mark = 6
                case 166 ... 195: mark = 7
                case 196 ... 225: mark = 8
                default: mark = 9
                }
                
            default: fatalError()
            }
        }
        
        return mark
    }
    
    func makeResultDescription(marks: [Int], isSit sit: Bool, stan: Int) -> ResultDescription {
        let indices = marks.map { mark -> Int in
            switch mark {
            case ...3: return 0
            case 4 ... 6: return 1
            case 7...: return 2
            default: fatalError()
            }
        }
        
        let anxiety: String
        switch stan {
        case ...3: anxiety = "Уровень тревоги в целом низкий."
        case 4 ... 6: anxiety = "Уровень тревоги в целом средний."
        case 7 ... 9: anxiety = "Уровень тревоги в целом высокий."
        default: fatalError()
        }
        
        var advices = [
            IntegralAnxietyCalculator.edAdvices,
            IntegralAnxietyCalculator.astAdvices,
            IntegralAnxietyCalculator.fobAdvices,
            IntegralAnxietyCalculator.opAdvices,
            IntegralAnxietyCalculator.szAdvices
        ].enumerated()
        .map { index, array in return array[indices[index]] }
        advices.append(anxiety)
        
        return ResultDescription(
            title: sit ? "Ситуативная тревога" : "Личностная тревога",
            advice: advices.joined(separator: " ")
        )
    }
    
}
