//
//  EllisCalculator.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

struct EllisCalulcator: TestCalculator {
    
    let test: Test
    
    private static let disastrousSummarizedIndexies: Set<Int> = [
        6, 11, 16, 21, 31, 36, 41
    ]
    
    private static let disastrousReversedIndexies: Set<Int> = [
        1, 26, 46
    ]
    
    private static let disastrousAdvices = [
        "Вы склоны сильно преувеливать степень негативных последствий ситуаций. Неблагоприятные события кажутся вам ужасными и невыносимыми. Зачастую это приводит к повышению тревоги. Во время работы с собственными мыслями рекомендуем вам обратить внимание на проработку ошибки мышления «катастрофизация».",
        "Вы склоны в некоторой степени преувеливать степень негативных последствий ситуаций. Порой неблагоприятные события кажутся вам ужасными и невыносимыми. Это может повышать уровень тревоги. Во время работы с собственными мыслями рекомендуем вам обратить внимание на проработку ошибки мышления «катастрофизация».",
        "Вы не склоны преувеливать степень негативных последствий ситуаций. Неблагоприятные события кажутся вам вполне переносимыми."
    ]
    
    private static let myOwingSummarizedIndexies: Set<Int> = [
        2, 7, 12, 27, 32, 37, 47
    ]
    
    private static let myOwingReversedIndexies: Set<Int> = [
        17, 22, 42
    ]
    
    private static let myOwingAdvices = [
        "Вы очень часто предъявляете чрезмерно высокие требования к себе. Это может приводить к высокому чувству вины. Во время работы с собственными мыслями, рекомендуем вам обратить внимание на проработку ошибки мышления «долженствование».",
        "Вы порой предъявляете чрезмерно высокие требования к себе. Это может приводить к повышению чувства вины. Во время работы с собственными мыслями, рекомендуем вам обратить внимание на проработку ошибки мышления «долженствование».",
        "Вы предъявляете достаточно адекватные требования к себе."
    ]
    
    private static let otherOwingSummarizedIndexies: Set<Int> = [
        3, 8, 18, 23, 33, 43, 48
    ]
    
    private static let otherOwingReversedIndexies: Set<Int> = [
        13, 28, 38
    ]
    
    private static let otherOwingAdvices = [
        "Вы очень часто предъявляете чрезмерно высокие требования к другим. Часто это приводит к повышению чувства обиды и злости. Во время работы с собственными мыслями рекомендуем вам обратить внимание на проработку ошибки мышления «долженствование».",
        "Вы порой предъявляете чрезмерно высокие требования к другим. Это может приводить к частым обидам и раздражению. Во время работы с собственными мыслями рекомендуем вам обратить внимание на проработку ошибки мышления «долженствование».",
        "Вы предъявляете достаточно адекватные требования к другим."
    ]
    
    private static let tolerancySummarizedIndexies: Set<Int> = [
        9, 14, 19, 24, 29, 39, 44
    ]
    
    private static let tolerancyReversedIndexies: Set<Int> = [
        4, 34, 49
    ]
    
    private static let tolerancyAdvices = [
        "У вас очень низкая степень стрессоустойчивости. Высокая вероятность развития стресса.",
        "У вас низкая степень стрессоустойчивости. Средняя вероятность развития стресса.",
        "Вы достаточно стрессоустойчивы. Низкая вероятность развития стресса."
    ]
    
    private static let selfEsteemSummarizedIndexies: Set<Int> = [
        5, 10, 15, 30, 35, 40, 45, 50
    ]
    
    private static let selfEsteemReversedIndexies: Set<Int> = [
        20, 25
    ]
    
    private static let selfEsteemAdvices = [
        "Вы часто оцениваете не отдельные черты или поступки людей, а личность в целом. Это касается не только окружающих, но и вас самих. Зачастую это приводит к выраженному снижению самооценки и частому раздражению. Во время работы с собственными мыслями рекомендуем вам обратить внимание на проработку ошибки мышления «навешивание ярлыков».",
        "Вы склонны оценивать не отдельные черты или поступки людей, а личность в целом. Это касается не только окружающих, но и вас самих. Эта привычка может приводить к выраженному снижению самооценки. Во время работы с собственными мыслями рекомендуем вам обратить внимание на проработку ошибки мышления «навешивание ярлыков».",
        "Вы умеете не переносить оценку поведения или отдельных черт человека на личность в целом."
    ]
    
    func calculate(summary: TestAnswerSummary) -> TestResult {
        let answers = summary.answers
        
        let disastrous = calculate(
            summarized: EllisCalulcator.disastrousSummarizedIndexies,
            reversed: EllisCalulcator.disastrousReversedIndexies,
            answers: answers
        )
        
        let myOwing = calculate(
            summarized: EllisCalulcator.myOwingSummarizedIndexies,
            reversed: EllisCalulcator.myOwingReversedIndexies,
            answers: answers
        )
        
        let otherOwing = calculate(
            summarized: EllisCalulcator.otherOwingSummarizedIndexies,
            reversed: EllisCalulcator.otherOwingReversedIndexies,
            answers: answers
        )
        
        let tolerancy = calculate(
            summarized: EllisCalulcator.tolerancySummarizedIndexies,
            reversed: EllisCalulcator.tolerancyReversedIndexies,
            answers: answers
        )
        
        let selfEsteem = calculate(
            summarized: EllisCalulcator.selfEsteemSummarizedIndexies,
            reversed: EllisCalulcator.selfEsteemReversedIndexies,
            answers: answers
        )
        
        let gen = disastrous + myOwing + otherOwing + tolerancy + selfEsteem
        
        let pairs = zip(["Тест эллиса катастрофизация", "Тест эллиса должен к себе", "Тест эллиса должен к другим", "Тест эллиса низкая фрустрац толер", "Тест эллиса самооценка", "Тест эллиса общий балл"], [disastrous, myOwing,  otherOwing, tolerancy, selfEsteem, gen]).map { Pair(key: $0.0, value: $0.1) }
        
        return TestResult(test: test, results: [
            makeResultDescription(for: "катастрофизации", advices: EllisCalulcator.disastrousAdvices, sum: disastrous),
            makeResultDescription(for: "долженствования в отношении себя", advices: EllisCalulcator.myOwingAdvices, sum: myOwing),
            makeResultDescription(for: "долженствования в отношении других", advices: EllisCalulcator.otherOwingAdvices, sum: otherOwing),
            makeResultDescription(for: "низкая фрустрационная толерантность", advices: EllisCalulcator.tolerancyAdvices, sum: tolerancy),
            makeResultDescription(for: "самооценка", advices: EllisCalulcator.selfEsteemAdvices, sum: selfEsteem),
        ], res: Res(title: "Эллис", pairs: pairs))
    }
    
}

private extension EllisCalulcator {
    
    func calculate(summarized: Set<Int>, reversed: Set<Int>, answers: [TestAnswer]) -> Int {
        var sum = 0
        
        for sumIndex in summarized {
            sum += answers[sumIndex - 1].weight
        }
        
        for sumIndex in reversed {
            sum += 7 - answers[sumIndex - 1].weight
        }
        
        return sum
    }
    
    func makeResultDescription(for category: String, advices: [String], sum: Int) -> ResultDescription {
        let title = "Вы набрали \(sum.markFormatted) по шкале \(category)"
        
        let advice: String
        
        switch sum {
        case ...30: advice = advices[0]
        case 31 ... 45: advice = advices[1]
        case 46...: advice = advices[2]
        default: fatalError()
        }
        
        return ResultDescription(title: title, advice: advice)
    }
    
}
