//
//  LifestyleCalculator.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 16/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

struct LifestyleCalculator: TestCalculator {
    
    let test: Test
    
    private static let negationIndices = [
        1, 16, 22, 28, 34, 42, 51, 61, 68, 77, 82, 90, 94
    ]
    private static let negationAdvice = "Ваша психика стремится защитить вас, отрицая обстоятельства, вызывающие тревогу. Действие этого механизма проявляется в отрицании тех аспектов внешней реальности, которые, будучи очевидными для окружающих, тем не менее, не принимаются, не признаются вами. Иными словами, информация, которая тревожит и может привести к конфликту, не воспринимается. Имеется в виду конфликт, возникающий при проявлении мотивов, противоречащих основным установкам личности или информация, которая угрожает самосохранению, самоуважению, или социальному престижу."
    
    private static let suppresionIndices = [
        6, 11, 19, 25, 35, 43, 49, 59, 66, 75, 85, 89,
    ]
    private static let suppresionAdvice = "Ваша психика защищает вас, вытесняя из памяти все неприемлемые импульсы: желания, мысли, чувства, вызывающие тревогу. Подавленные импульсы становятся бессознательными, не находят разрешения в поведении, тем не менее, сохраняют свои эмоциональные переживания и телесные симптомы. Например, вы можете порой  ощущать немотивированную тревогу."
    
    private static let regressionIndices = [
        2, 14, 18, 26, 33, 48, 50, 58, 69, 78, 86, 88, 93, 95,
    ]
    private static let regressionAdvice = "Ваша психика защищает вас от стрессов заменой решений субъективно более сложных задач на относительно более простые и доступные в сложившихся ситуациях. Использование более простых и привычных поведенческих стереотипов существенно обедняет общий (потенциально возможный) арсенал преобладания конфликтных ситуаций. Возникает ощущение, что вы не можете с этим справиться, не знаете, как это сделать."
    
    private static let compensationIndices = [
        3, 10, 24, 29, 37, 45, 52, 64, 65, 74,
    ]
    private static let compensationAdvice = "Ваша психика помогает вам найти подходящую замену реального или воображаемого недостатка, нестерпимого дефекта другим качеством. Чаще всего это происходит с помощью фантазирования или присвоения себе свойств, достоинств, ценностей, поведенческих характеристик другой личности. При этом заимствованные ценности, установки или мысли принимаются без анализа и переструктурирования, и поэтому, не становятся частью самой личности.\nДругим проявлением компенсаторных защитных механизмов может быть ситуация преодоления тревожащих обстоятельств сверхудовлетворением своих потребностей в других сферах, например, физически слабый или робкий человек, неспособный ответить на угрозу расправы, находит удовлетворение за счет унижения обидчика с помощью изощренного ума или хитрости. Возможно, вы мечтатель, ищущий идеалы в различных сферах жизнедеятельности."
    
    private static let projectionIndices = [
        7, 9, 23, 27, 38, 41, 55, 63, 71, 73, 84, 92, 96,
    ]
    private static let projectionAdvice = "Ваша психика защищает вас от тревоги посредством приписывания окружающим своих неосознаваемых и неприемлемых чувств и мыслей. Например, агрессивность нередко приписывается окружающим, чтобы оправдать свою собственную агрессивность или недоброжелательность, которая проявляется как бы в защитных целях."
    
    private static let substutionIndices = [
        8, 15, 20, 31, 40, 47, 54, 60, 67, 76, 83, 91, 97
    ]
    private static let substutionAdvice = "Ваша психика позволяет вам справляться с подавленными эмоциями, перенося их на других людей. Например, злость на одного человека может быть выражена другому, представляющему меньшую опасность  или более доступному.\nВы можете совершать неожиданные, подчас бессмысленные действия, которые разрешают внутреннее напряжение."
    
    private static let intellectualizationIndices = [
        4, 13, 17, 30, 36, 44, 56, 62, 70, 80, 81, 87,
    ]
    private static let intellectualizationAdvice = "Ваша психика защищает вас от тревоги чрезмерным умственным способом преодоления конфликтов. Вы пресекаете переживания, вызванные неприятной или субъективно неприемлемой ситуацией при помощи логических установок и манипуляций, даже при наличии убедительных доказательств в пользу противоположного.\nВы способны создавать логические (псевдоразумные), но благовидные обоснования своего или чужого поведения, действий или переживаний, вызванных причинами, которые не можете признать из-за угрозы потери самоуважения. При этом способе защиты нередко наблюдаются очевидные попытки снизить ценность недоступного опыта."
    
    private static let reactIndices = [
        5, 12, 21, 32, 39, 46, 53, 57, 72, 79
    ]
    private static let reactAdvice = "Ваша психика предотвращает выражение неприятных или неприемлемых для нее мыслей, чувств или поступков путем преувеличенного развития противоположных стремлений. Иными словами, происходит как бы трансформация внутренних импульсов в субъективно понимаемую их противоположность. Одно из самых известных проявлений этого защитного механизма – бить, чтобы выразить свою любовь."
    
    func calculate(summary: TestAnswerSummary) -> TestResult {
        let answers = summary.answers
        let results = [
            calculate(for: "Отрицание", advice: LifestyleCalculator.negationAdvice, sum: sum(for: "Отрицание", answers: answers, indices: LifestyleCalculator.negationIndices), threshold: 5),
            calculate(for: "Подавление", advice: LifestyleCalculator.suppresionAdvice, sum: sum(for: "Подавление", answers: answers, indices: LifestyleCalculator.suppresionIndices), threshold: 4),
            calculate(for: "Регрессия", advice: LifestyleCalculator.regressionAdvice, sum: sum(for: "Регрессия", answers: answers, indices: LifestyleCalculator.regressionIndices), threshold: 4),
            calculate(for: "Компенсация", advice: LifestyleCalculator.compensationAdvice, sum: sum(for: "Компенсация", answers: answers, indices: LifestyleCalculator.compensationIndices), threshold: 3),
            calculate(for: "Проекция", advice: LifestyleCalculator.projectionAdvice, sum: sum(for: "Проекция", answers: answers, indices: LifestyleCalculator.projectionIndices), threshold: 9),
            calculate(for: "Замещение", advice: LifestyleCalculator.substutionAdvice, sum: sum(for: "Замещение", answers: answers, indices: LifestyleCalculator.substutionIndices), threshold: 4),
            calculate(for: "Интеллектуализация", advice: LifestyleCalculator.intellectualizationAdvice, sum: sum(for: "Интеллектуализация", answers: answers, indices: LifestyleCalculator.intellectualizationIndices), threshold: 6),
            calculate(for: "Реактивные образования", advice: LifestyleCalculator.reactAdvice, sum: sum(for: "Реактивные образования", answers: answers, indices: LifestyleCalculator.reactIndices), threshold: 3)
        ]
        
        let pairs = [
            Pair(key: "Отрицание", value: sum(for: "Отрицание", answers: answers, indices: LifestyleCalculator.negationIndices).percentile),
            Pair(key: "Подавление", value: sum(for: "Подавление", answers: answers, indices: LifestyleCalculator.suppresionIndices).percentile),
            Pair(key: "Регрессия", value: sum(for: "Регрессия", answers: answers, indices: LifestyleCalculator.regressionIndices).percentile),
            Pair(key: "Компенсация", value: sum(for: "Компенсация", answers: answers, indices: LifestyleCalculator.compensationIndices).percentile),
            Pair(key: "Проекция", value: sum(for: "Проекция", answers: answers, indices: LifestyleCalculator.projectionIndices).percentile),
            Pair(key: "Замещение", value: sum(for: "Замещение", answers: answers, indices: LifestyleCalculator.substutionIndices).percentile),
            Pair(key: "Интеллектуализация", value: sum(for: "Интеллектуализация", answers: answers, indices: LifestyleCalculator.intellectualizationIndices).percentile),
            Pair(key: "Реактивные образования", value: sum(for: "Реактивные образования", answers: answers, indices: LifestyleCalculator.reactIndices).percentile)
        ]
        
        return TestResult(test: test, results: results, res: Res(title: "ИЖC", pairs: pairs))
    }
    
}

private extension LifestyleCalculator {
    
    func calculate(for category: String, advice: String, sum: (raw: Int, percentile: Int), threshold: Int) -> ResultDescription {
        ResultDescription(title: "Шкала \(category.lowercased()) \(sum.percentile)", advice: sum.raw >= threshold ? advice : "Выраженность этого защитного механизма соответствует норме.")
    }
    
    func sum(for category: String, answers: [TestAnswer], indices: [Int]) -> (raw: Int, percentile: Int) {
        var sum = 0
        for index in indices {
            sum += answers[index - 1].weight
        }
        let percentiles: [Int]
        switch category {
        case "Отрицание": percentiles = [3, 13, 27, 39, 50, 61, 79, 84, 90, 97, 98, 99]
        case "Подавление": percentiles = [2, 8, 25, 42, 63, 76, 87, 92, 97, 98, 99]
        case "Регрессия": percentiles = [2, 6, 19, 35, 53, 70, 80, 85, 88, 95, 97, 99]
        case "Компенсация": percentiles = [5, 20, 37, 63, 78, 88, 95, 97, 99]
        case "Проекция": percentiles = [1, 5, 6, 7, 12, 20, 27, 36, 46, 64, 72, 90, 96, 99]
        case "Замещение": percentiles = [6, 23, 37, 48, 65, 77, 86, 93, 97, 98, 99]
        case "Интеллектуализация": percentiles = [0, 3, 6, 17, 28, 42, 59, 76, 87, 92, 97, 99]
        case "Реактивные образования": percentiles = [7, 19, 39, 61, 76, 91, 97, 98, 99]
        default: fatalError()
        }
        if percentiles.count <= sum  {
            return (raw: sum, 100)
        } else {
            return (raw: sum, percentiles[sum])
        }
    }
    
}
