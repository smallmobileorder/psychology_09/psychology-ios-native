//
//  TestCalculator.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

struct Res {
    let title: String
    let pairs: [Pair]
}

protocol TestCalculator {
    func calculate(summary: TestAnswerSummary) -> TestResult
}
