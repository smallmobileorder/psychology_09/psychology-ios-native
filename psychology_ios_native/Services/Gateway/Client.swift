//
//  Client.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

protocol ClientProtocol {
    func request<Response>(_ endpoint: Endpoint<Response>, completion: @escaping ObjectCallback<Result<Response, Error>>)
}

final class Client: ClientProtocol {
    
    private let queue = DispatchQueue(label: "com.psychology.psychology-ios-native.service-queue")

    func request<Response>(_ endpoint: Endpoint<Response>, completion: @escaping ObjectCallback<Result<Response, Error>>) {
        queue.async {
            var data: Data?
            switch endpoint.path {
            case "tests":
                data = Bundle.main.json(named: "tests")
                
            default:
                return
            }
            if let data = data {
                completion(endpoint.decode(data))
            } else {
                completion(.failure(EndpointError.cantRetrieveData))
            }
        }
    }
    
}
