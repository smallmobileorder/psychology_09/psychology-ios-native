//
//  Usage.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation


// MARK: Defining Endpoints

enum API {}

extension API {
    
    static var tests: Endpoint<[Test]> {
        return Endpoint(
            path: "tests"
        )
    }
    
}
