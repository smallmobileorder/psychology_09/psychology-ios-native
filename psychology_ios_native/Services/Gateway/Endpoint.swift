//
//  Endpoint.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

// MARK: Defines

typealias Parameters = [String: Any]
typealias Path = String

enum Method {
    case get, post, put, patch, delete
}

enum EndpointError: String, Error {
    case failedToDecodeVoid
    case cantRetrieveData
    
    var localizedDescription: String {
        return rawValue.components(separatedBy: CharacterSet.uppercaseLetters).map { $0.capitalizingFirstLetter() }.joined(separator: " ")
    }
}

// MARK: Endpoint

final class Endpoint<Response> {
    let method: Method
    let path: Path
    let parameters: Parameters?
    let decode: (Data) -> Result<Response, Error>

    init(method: Method = .get,
         path: Path,
         parameters: Parameters? = nil,
         decode: @escaping (Data) -> Result<Response, Error>) {
        self.method = method
        self.path = path
        self.parameters = parameters
        self.decode = decode
    }
}


// MARK: Convenience

extension Endpoint where Response: Decodable {
    convenience init(method: Method = .get,
                     path: Path,
                     parameters: Parameters? = nil,
                     decoder: JSONDecoder  = .init()) {
        self.init(method: method, path: path, parameters: parameters) {
            do {
                return .success(try decoder.decode(Response.self, from: $0))
            } catch let error {
                return .failure(error)
            }
        }
    }
}

extension Endpoint where Response == Void {
    convenience init(method: Method = .get,
                     path: Path,
                     parameters: Parameters? = nil) {
        self.init(
            method: method,
            path: path,
            parameters: parameters,
            decode: { _ in ( .failure(EndpointError.failedToDecodeVoid) ) }
        )
    }
}
