//
//  CatastrhHomeWork.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 20.05.2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import RealmSwift
import Foundation

class CatastrhHomeWork: Object {
    @objc dynamic var goodResult: String?
    @objc dynamic var peopleThoughts: String?
    @objc dynamic var reasonableResult: String?
    @objc dynamic var worstHarm: String?
    @objc dynamic var worstHarmPercent = 1
    @objc dynamic var bestHarm: String?
    @objc dynamic var bestHarmPercent = 1
    @objc dynamic var createDate = Date(timeIntervalSince1970: 1)
}

//class СatastrophicDz: Object {
//    @objc dynamic var goodResult: String?
//    @objc dynamic var peopleThoughts: String?
//    @objc dynamic var reasonableResult: String?
//    @objc dynamic var worstHarm: String?
//    @objc dynamic var bestHarm: String?
//    @objc dynamic var realHarm: String?
//}
