//
//  ExpandedHolder.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 16/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

class ExpandedHolder<T> {
    let model: T
    
    init(with model: T) {
        self.model = model
    }
    
    var onChange: Callback?
    
    private(set) var previousState: Bool?
    private(set) var expanded = false
    
    func collapse(callingCallback: Bool = true) {
        previousState = expanded
        expanded = false
        if callingCallback { onChange?() }
    }
    
    func expand(callingCallback: Bool = true) {
        previousState = expanded
        expanded = true
        if callingCallback { onChange?() }
    }
    
}
