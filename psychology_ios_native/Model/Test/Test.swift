//
//  Test.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 07/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

struct Test: Codable, Equatable {
    let title: String?
    let description: String?
    let intro: String?
    let questions: [TestQuestion]
}

struct TestQuestion: Codable, Equatable {
    let question: String
    let answers: [TestAnswer]?
}

struct TestAnswer: Codable, Equatable {
    let answer: String
    let weight: Int
}

struct TestAnswerSummary {
    let answers: [TestAnswer]
}
