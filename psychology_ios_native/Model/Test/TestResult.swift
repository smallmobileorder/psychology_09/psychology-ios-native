//
//  TestResult.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

struct TestResult {
    let test: Test
    let results: [ResultDescription]
    let res: Res
}

struct ResultDescription {
    let title: String
    let advice: String
}
