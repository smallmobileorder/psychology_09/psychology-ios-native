//
//  ExerciseCase.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 28/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

struct ExerciseCase: Codable, Hashable {
    
	let text: String?
	let questions: [Question]?

	enum CodingKeys: String, CodingKey {
		case text
		case questions
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		text = try values.decodeIfPresent(String.self, forKey: .text)
		questions = try values.decodeIfPresent([Question].self, forKey: .questions)
	}

}
