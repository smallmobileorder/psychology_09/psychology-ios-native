//
//  Exercise.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 28/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

struct Exercise: Codable, Hashable {
    
	let title: ExperimentTitle?
	let description: ExperimentDescription?
	let cases: [ExerciseCase]
	let conclusion: ExperimentConclusion

	enum CodingKeys: String, CodingKey {
		case title
		case description
		case cases
		case conclusion
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		title = try values.decodeIfPresent(ExperimentTitle.self, forKey: .title)
		description = try values.decodeIfPresent(ExperimentDescription.self, forKey: .description)
		cases = try values.decode([ExerciseCase].self, forKey: .cases)
		conclusion = try values.decode(ExperimentConclusion.self, forKey: .conclusion)
	}

}
