//
//  Question.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 28/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

struct Question: Codable, Hashable {
    
	let title: String?
	let answer: String?

	enum CodingKeys: String, CodingKey {
		case title
		case answer
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		title = try values.decodeIfPresent(String.self, forKey: .title)
		answer = try values.decodeIfPresent(String.self, forKey: .answer)
	}

}
