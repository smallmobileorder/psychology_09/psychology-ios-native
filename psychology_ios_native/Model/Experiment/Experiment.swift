//
//  Experiment.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 26/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

struct Experiment: Codable, Hashable {
	
    let title: ExperimentTitle?
	let description: ExperimentDescription?
	let situation: ExperimentSituation
	let conclusion: ExperimentConclusion

	enum CodingKeys: String, CodingKey {
		case title
		case description
		case situation
		case conclusion
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		title = try values.decodeIfPresent(ExperimentTitle.self, forKey: .title)
		description = try values.decodeIfPresent(ExperimentDescription.self, forKey: .description)
		situation = try values.decode(ExperimentSituation.self, forKey: .situation)
		conclusion = try values.decode(ExperimentConclusion.self, forKey: .conclusion)
	}

}
