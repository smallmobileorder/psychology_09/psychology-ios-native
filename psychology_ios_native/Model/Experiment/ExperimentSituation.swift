//
//  Situation.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 26/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

struct ExperimentSituation: Codable, Hashable {
    
	let text: String?
	let image: String?
    let questionPrefix: String?
	let questions: [String]?

	enum CodingKeys: String, CodingKey {
		case text
		case image
        case questionPrefix = "prefix"
		case questions
	}

	init(from decoder: Decoder) throws {
		let values = try decoder.container(keyedBy: CodingKeys.self)
		text = try values.decodeIfPresent(String.self, forKey: .text)
		image = try values.decodeIfPresent(String.self, forKey: .image)
        questionPrefix = try values.decodeIfPresent(String.self, forKey: .questionPrefix)
		questions = try values.decodeIfPresent([String].self, forKey: .questions)
	}

}
