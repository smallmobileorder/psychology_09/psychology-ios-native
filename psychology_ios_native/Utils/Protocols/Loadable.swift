//
//  Loadable.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

protocol Loadable {
    func startLoading()
    func stopLoading()
}
