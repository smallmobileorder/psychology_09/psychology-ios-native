//
//  Extensions+UITableViewCell.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

extension UITableViewCell {
    static var identifier: String { return String(describing: self) }
    
    class var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
}

extension UITableView {
    func register<Cell: UITableViewCell>(
        cell: Cell.Type,
        identifier reuseIdentifier: String = Cell.identifier) {
        register(cell, forCellReuseIdentifier: reuseIdentifier)
    }
    
    func registerNib<Cell: UITableViewCell>(
        cell: Cell.Type,
        identifier reuseIdentifier: String = Cell.identifier) {
        register(cell.nib, forCellReuseIdentifier: reuseIdentifier)
    }
    
    func dequeue<Cell>(_ reusableCell: Cell.Type) -> Cell? where Cell: UITableViewCell {
        return dequeueReusableCell(withIdentifier: reusableCell.identifier) as? Cell
    }
    
    func register<HeaderFooter: UITableViewHeaderFooterView>(
        headerFooter: HeaderFooter.Type,
        identifier reuseIdentifier: String = String(describing: HeaderFooter.self)) {
        register(headerFooter, forHeaderFooterViewReuseIdentifier: reuseIdentifier)
    }
    
    func dequeue<HeaderFooter: UITableViewHeaderFooterView>(
        headerFooter: HeaderFooter.Type,
        identifier reuseIdentifier: String = String(describing: HeaderFooter.self)) -> HeaderFooter? {
        return dequeueReusableHeaderFooterView(withIdentifier: reuseIdentifier) as? HeaderFooter
    }
    
}
