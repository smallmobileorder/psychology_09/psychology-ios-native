//
//  Extensions+UIImage.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

extension UIImage {
    
    var width: CGFloat { return CGFloat(cgImage?.width ?? 0) }
    var height: CGFloat { return CGFloat(cgImage?.height ?? 0) }
    var aspectRatio: CGFloat {
        guard width != 0 else { return 0 }
        
        return height / width
    }
    
}
