//
//  PresenterOutputProtocol+UIViewController.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 26/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

extension PresenterOutputProtocol where Self: UIViewController {
    
    func startLoading() {
        DispatchQueue.main.async { self.start() }
    }
    
    func stopLoading() {
        DispatchQueue.main.async { self.stop() }
    }
    
    func show(message: AlertMessage, onCompletion: @escaping () -> ()) {
        DispatchQueue.main.async { self.showAlert(message, onCompletion: onCompletion) }
    }
    
}

private extension PresenterOutputProtocol where Self: UIViewController {
    
    func start() {
        view.startLoading()
    }
    
    func stop() {
        view.stopLoading()
    }
    
}
