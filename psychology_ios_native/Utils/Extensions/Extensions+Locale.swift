//
//  Extensions+Locale.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

extension Locale {
    
    static let us: Locale = {
        return Locale(identifier: "en_US")
    }()
    
}
