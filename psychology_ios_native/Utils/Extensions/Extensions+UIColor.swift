//
//  Extensions+UIColor.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var active: UIColor {
        get {
            return UIColor(hexString: UserDefaults.standard.string(forKey: "theme_color") ?? "BD17F8")
        }
        set {
            UserDefaults.standard.setValue(newValue.hexStringValue(), forKeyPath: "theme_color")
        }
    }
    
    static var darkPurple: UIColor { return UIColor(rgb: 0x9B02D0) }
    static var shadow: UIColor { return UIColor.black.withAlphaComponent(0.25) }
    
    static var sexMan: UIColor { return UIColor(rgb: 0x59CFE3) }
    static var sexWoman: UIColor { return UIColor(rgb: 0xBD17F8) }
    
    static var darkSystemGray: UIColor { return UIColor(rgb: 0x8E8E93) }
    static var darkSystemGray2: UIColor { return UIColor(rgb: 0x3A3A3C) }
    static var lightSystemGray4: UIColor { return UIColor(rgb: 0xD1D1D6) }
    static var darkSystemGray4: UIColor { return UIColor(rgb: 0x3A3A3C) }
    static var darkSystemGray5: UIColor { return UIColor(rgb: 0x2C2C2E) }
    static var lightSystemGray: UIColor { return UIColor(rgb: 0x8E8E93) }
    static var systemGray5: UIColor { return UIColor(rgb: 0x2C2C2E) }
    static var back: UIColor { return UIColor(rgb: 0xFCFCFC) }
    
    var asImage: UIImage? {
        defer { UIGraphicsEndImageContext() }
        
        let rect = CGRect(origin: .zero, size: CGSize(width: 1, height: 1))
        
        UIGraphicsBeginImageContextWithOptions(rect.size, true, 1)
        setFill()
        UIRectFill(rect)
        
        return UIGraphicsGetImageFromCurrentImageContext()
    }
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
            assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    func image(_ size: CGSize = CGSize(width: 1, height: 1), cornerRadius: CGFloat = 8) -> UIImage {
        return UIGraphicsImageRenderer(size: size).image { context in
            let rect = CGRect(origin: .zero, size: size)
            let clipPath = UIBezierPath(roundedRect: rect, cornerRadius: cornerRadius).cgPath
            context.cgContext.addPath(clipPath)
            context.cgContext.setFillColor(cgColor)
            context.cgContext.fillPath()
        }
    }
    
}
