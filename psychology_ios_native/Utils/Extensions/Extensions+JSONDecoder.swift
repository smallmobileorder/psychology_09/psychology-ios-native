//
//  Extensions+JSONDecoder.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

extension JSONDecoder.DateDecodingStrategy {
    
    static func formatted(_ format: String) -> JSONDecoder.DateDecodingStrategy {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        return .formatted(dateFormatter)
    }
    
}
