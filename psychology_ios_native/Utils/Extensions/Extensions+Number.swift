//
//  Extension+NSNumber.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

protocol Number {}

extension Number {
    
    var millionFormatted: String? {
        return Formatter.millionFormatter.string(for: self)
    }
    
    var stringAsMoney: String? {
        return Formatter.moneyFormatter.string(for: self)
    }
    
    var percentFormatted: String? {
        return Formatter.percentFormatter.string(for: self)
    }
    
    var areaFormatted: String? {
        return formatted(appending: "м²")
    }
    
    func formatted(appending suffix: String = "") -> String? {
        guard let string = Formatter.limitedFractionDigitsFormatter.string(for: self) else { return nil }
        
        return [string, suffix].joined(separator: " ")
    }
    
}

extension Int: Number {
    
    var markFormatted: String {
        switch self % 10 {
        case 0, 5 ... 9: return "\(self) баллов"
        case 1: return "\(self) балл"
        default: return "\(self) \((self > 10 && self < 20) ? "баллов" : "балла")"
        }
    }
    
}

extension Double: Number {}
extension Decimal: Number {}
