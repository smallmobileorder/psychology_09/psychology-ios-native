//
//  Extensions+Bundle.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 26/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

extension Bundle {
    
    func json(named name: String) -> Data {
        let jsonPath = path(forResource: name, ofType: "json")!
        return try! Data(contentsOf: URL(fileURLWithPath: jsonPath))
    }
    
}
