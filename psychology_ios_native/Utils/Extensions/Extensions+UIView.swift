//
//  Extensions+UIView.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

enum RoundType {
    case top
    case right
    case left
    case none
    case bottom
    case all
}

extension Loadable where Self: UIView {
    
    func startLoading() {
        let activityView = ActivityView()
        activityView.frame = bounds
        activityView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(activityView)
        activityView.start()
        isUserInteractionEnabled = false
    }
    
    func stopLoading() {
        guard let activityView = subviews.first(where: { $0 is ActivityView }) as? ActivityView else { return }
        activityView.stop()
        activityView.removeFromSuperview()
        isUserInteractionEnabled = true
    }
    
}

extension CALayer {
    
    func addShadow(color: UIColor, opacity: Float, radius: CGFloat, offset: CGSize) {
        masksToBounds = false
        shadowColor = color.cgColor
        shadowOpacity = opacity
        shadowRadius = radius
        shadowOffset = offset
        shadowPath = UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius).cgPath
    }
    
}

extension UIView: Loadable { }

extension UIView {
    
    var asImage: UIImage? {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(bounds: bounds)
            return renderer.image { rendererContext in
                layer.render(in: rendererContext.cgContext)
            }
        } else {
            UIGraphicsBeginImageContext(frame.size)
            defer { UIGraphicsEndImageContext() }
            
            guard let context = UIGraphicsGetCurrentContext() else { return nil }
            
            layer.render(in: context)
            
            guard let cgImage = UIGraphicsGetImageFromCurrentImageContext()?.cgImage else { return nil }
            
            return UIImage(cgImage: cgImage)
        }
    }
    
    //view matches the container's size
    func setEqualSize(on view: UIView) {
        leadingAnchor(to: view.leadingAnchor)
        trailingAnchor(to: view.trailingAnchor)
        topAnchor(to: view.topAnchor)
        bottomAnchor(to: view.bottomAnchor)
    }
    
    func round(with type: RoundType, radius: CGFloat = 5.0) {
        var corners: UIRectCorner
        
        switch type {
        case .top:
            corners = [.topLeft, .topRight]
        case .right:
            corners = [.bottomRight, .topRight]
        case .left:
            corners = [.bottomLeft, .topLeft]
        case .none:
            corners = []
        case .bottom:
            corners = [.bottomLeft, .bottomRight]
        case .all:
            corners = [.allCorners]
        }
        
        round(with: corners, radius: radius)
    }
    
    func round(with corners: UIRectCorner, radius: CGFloat = 5.0) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    
    func addShadow(color: UIColor, opacity: Float, radius: CGFloat, offset: CGSize) {
        clipsToBounds = true
        layer.addShadow(color: color, opacity: opacity, radius: radius, offset: offset)
        //layer.shouldRasterize = true
        //layer.rasterizationScale = UIScreen.main.scale
    }
    
    func removeShadow() {
        layer.shadowPath = nil
    }
    
    func setDismissKeyboardOnTap() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        tapGestureRecognizer.cancelsTouchesInView = false
        addGestureRecognizer(tapGestureRecognizer)
    }
    
    @objc func dismissKeyboard() {
        endEditing(true)
    }
    
    func loadNib() {
        let myType = type(of: self)
        let nib = UINib(nibName: String(describing: myType), bundle: Bundle(for: myType))
        let view = nib.instantiate(withOwner: self).first as! UIView
        view.frame = bounds
        view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        addSubview(view)
    }
    
}

extension UIView {
    
    @discardableResult
    func activateAnchors() -> Self {
        translatesAutoresizingMaskIntoConstraints = false
        return self
    }
    
    @discardableResult
    func topAnchor(to anchor: NSLayoutYAxisAnchor, constant: CGFloat = 0) -> Self {
        topAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        return self
    }
    @discardableResult
    func leadingAnchor(to anchor: NSLayoutXAxisAnchor, constant: CGFloat = 0) -> Self {
        leadingAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        return self
    }
    @discardableResult
    func bottomAnchor(to anchor: NSLayoutYAxisAnchor, constant: CGFloat = 0) -> Self {
        bottomAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        return self
    }
    @discardableResult
    func trailingAnchor(to anchor: NSLayoutXAxisAnchor, constant: CGFloat = 0) -> Self {
        trailingAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        return self
    }
    @discardableResult
    func widthAnchor(constant: CGFloat) -> Self {
        widthAnchor.constraint(equalToConstant: constant).isActive = true
        return self
    }
    @discardableResult
    func heightAnchor(constant: CGFloat) -> Self {
        heightAnchor.constraint(equalToConstant: constant).isActive = true
        return self
    }
    @discardableResult
    func dimensionAnchors(width widthConstant: CGFloat, height heightConstant: CGFloat) -> Self {
        widthAnchor.constraint(equalToConstant: widthConstant).isActive = true
        heightAnchor.constraint(equalToConstant: heightConstant).isActive = true
        return self
    }
    @discardableResult
    func dimensionAnchors(size: CGSize) -> Self {
        widthAnchor.constraint(equalToConstant: size.width).isActive = true
        heightAnchor.constraint(equalToConstant: size.height).isActive = true
        return self
    }
    @discardableResult
    func centerYAnchor(to anchor: NSLayoutYAxisAnchor, constant: CGFloat = 0)  -> Self {
        centerYAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        return self
    }
    @discardableResult
    func centerXAnchor(to anchor: NSLayoutXAxisAnchor, constant: CGFloat = 0)  -> Self {
        centerXAnchor.constraint(equalTo: anchor, constant: constant).isActive = true
        return self
    }
    
    var rootView: UIView? {
        var parentView = superview
        while let view = parentView?.superview {
            parentView = view
        }
        return parentView
    }
    
    func minSize(thatFits size: CGSize) -> CGSize {
        let oldSize = size
        defer { frame.size = oldSize }
        frame.size = size
        return systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
    }
    
}

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor ?? UIColor.lightGray.cgColor)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
}
