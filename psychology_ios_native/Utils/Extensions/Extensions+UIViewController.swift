//
//  UIViewControllerExtension.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit
import Foundation

extension NSObject {
    static var nameOfClass: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
}

extension UIViewController {
    class func instanceController(storyboard: Storyboards) -> Self {
        return storyboard.instance.instantiateViewController(withIdentifier: nameOfClass) as! Self
    }
    class func fromNib() -> Self {
        return Self.init(nibName: nameOfClass, bundle: nil)
    }
}

extension UIViewController {
    
    func dismissAndResignFirstResponder(animated: Bool, completion: (() -> ())? = nil) {
        view.dismissKeyboard()
        dismiss(animated: animated, completion: completion)
    }
    
    func addChild(_ vc: UIViewController?, container: UIView) {
        if let childVC = vc, let childView = childVC.view {
            childVC.willMove(toParent: self)
            childView.translatesAutoresizingMaskIntoConstraints = false
            container.addSubview(childView)
            childView.frame = container.bounds
            childView.setEqualSize(on: container)
            addChild(childVC)
            childVC.didMove(toParent: self)
        }
    }
    
    func addNavigationBackButton(
        title: String? = "Назад",
        image: UIImage? = #imageLiteral(resourceName: "Arrow"),
        tintColor: UIColor = .active,
        selector: Selector,
        font: UIFont? = .systemFont(ofSize: 17)) {
        let button = UIButton(type: .system)
        let originalImage = image
        button.setImage(originalImage, for: .normal)
        button.setTitle(title, for: .normal)
        button.contentEdgeInsets = .only(left: -8, right: 5)
        button.titleEdgeInsets = .only(left: 5, right: -5)
        if let f = font {
            button.titleLabel?.font = f
        }
        button.frame = CGRect(x: 0, y: 0, width: 24, height: 24)
        button.sizeToFit()
        button.tintColor = tintColor
        button.addTarget(self, action: selector, for: .touchUpInside)
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
    }
    
    func showAlertСontroller(title: String? = nil, message: String? = nil, actions: [(title: String, handler: Callback?)] = []) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if let title = title {
            alert.setValue(NSAttributedString(string: title, attributes:
                [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 17, weight: .semibold),
                 NSAttributedString.Key.foregroundColor : UIColor.black]), forKey: "attributedTitle")
        }
        if let message = message {
            alert.setValue(NSAttributedString(string: message, attributes:
                [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 13, weight: .regular),
                 NSAttributedString.Key.foregroundColor : UIColor.black]), forKey: "attributedMessage")
        }
        alert.view.tintColor = .active
        alert.view.layer.cornerRadius = 16
        //alert.view.clipsToBounds = true
        
        if actions.isEmpty {
            alert.addAction(UIAlertAction(title: "OK", style: .default))
        } else {
            for action in actions {
                alert.addAction(UIAlertAction(title: action.title, style: .default, handler: { _ in action.handler?() }))
            }
        }
        
        present(alert, animated: true)
    }
    
    func showAlert(_ alert: AlertMessage, onCompletion: @escaping () -> ()) {
        let vc = UIViewController()
        
        let alertView = AlertView()
        alertView.message = alert
        alertView.alpha = 0
        
        vc.view.addSubview(alertView)
        alertView.frame = vc.view.frame
        alertView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        let rootWindow = UIApplication.shared.keyWindow
        
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = vc
        alertWindow.makeKeyAndVisible()
        
        alertView.onClose = { [weak rootWindow] in
            alertWindow.rootViewController = nil
            rootWindow?.makeKeyAndVisible()
            onCompletion()
        }
        
        UIView.animate(withDuration: 0.1) {
            alertView.alpha = 1
        }
    }
    
}
