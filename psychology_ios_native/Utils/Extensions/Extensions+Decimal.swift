//
//  Extensions+Decimal.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

extension Decimal {
    init?(string: String?) {
        guard let string = string else { return nil }
        
        self.init(string: string)
    }
}
