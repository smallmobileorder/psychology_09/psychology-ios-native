//
//  Extensions+NSMutableAttributedString.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 28/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

extension NSMutableAttributedString {
    
    func setFontFace(font: UIFont, color: UIColor? = nil) {
        beginEditing()
        enumerateAttribute(.font, in: NSRange(location: 0, length: length)) { value, range, stop in
            guard
                let oldFont = value as? UIFont,
                let newFontDescriptor = oldFont.fontDescriptor
                    .withFamily(font.familyName)
                    .withSymbolicTraits(oldFont.fontDescriptor.symbolicTraits) else { return }
            
            let newFont = UIFont(descriptor: newFontDescriptor, size: font.pointSize)
            removeAttribute(.font, range: range)
            addAttribute(.font, value: newFont, range: range)
            if let color = color {
                removeAttribute(.foregroundColor, range: range)
                addAttribute(.foregroundColor, value: color, range: range)
            }
        }
        endEditing()
    }
    
}
