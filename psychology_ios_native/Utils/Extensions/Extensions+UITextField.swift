//
//  Extensions+UITextField.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

extension UITextField {
    
    func setFormattedNumber(_ number: String, addCode: Bool = false) {
        let cleanPhoneNumber = number.components(separatedBy: CharacterSet.decimalDigits.inverted).joined()
        
        var mask = ""
        if addCode {
            mask = "+X (XXX) XXX-XX-XX"
        } else {
            mask = "(XXX) XXX-XX-XX"
        }
        
        var result = ""
        var index = cleanPhoneNumber.startIndex
        for ch in mask {
            if index == cleanPhoneNumber.endIndex {
                break
            }
            if ch == "X" {
                result.append(cleanPhoneNumber[index])
                index = cleanPhoneNumber.index(after: index)
            } else {
                result.append(ch)
            }
        }
        
        text = result
    }
    
}
