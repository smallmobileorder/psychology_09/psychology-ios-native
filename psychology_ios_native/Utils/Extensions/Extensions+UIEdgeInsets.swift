//
//  Extensions+UIEdgeInsets.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

extension UIEdgeInsets {
    
    static func only(
        top: CGFloat = 0,
        left: CGFloat = 0,
        bottom: CGFloat = 0,
        right: CGFloat = 0) -> UIEdgeInsets {
        return UIEdgeInsets(top: top, left: left, bottom: bottom, right: right)
    }
    
    static func all(_ value: CGFloat) -> UIEdgeInsets {
        return UIEdgeInsets(top: value, left: value, bottom: value, right: value)
    }
    
    static func symmetric(horizontal: CGFloat = 0, vertical: CGFloat = 0) -> UIEdgeInsets {
        return UIEdgeInsets(top: vertical, left: horizontal, bottom: vertical, right: horizontal)
    }
    
}
