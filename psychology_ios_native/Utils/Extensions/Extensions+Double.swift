//
//  Extensions+Double.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

extension Double {
    var asInt: Int? {
        if self >= Double(Int.min) && self < Double(Int.max) { return Int(self) }
        else { return nil }
    }
}
