//
//  Extensions+Formatter.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

extension Formatter {
    
    static let millionFormatter: Formatter = {
        let formatter = makeMoneyFormatter()
        formatter.currencySymbol = "млн. ₽"
        formatter.currencyDecimalSeparator = "."
        return formatter
    }()
    
    static let moneyFormatter: Formatter = {
        return makeMoneyFormatter()
    }()
    
    static let percentFormatter: Formatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.locale = .us
        formatter.maximumFractionDigits = 0
        return formatter
    }()
    
    static let limitedFractionDigitsFormatter: Formatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.decimalSeparator = "."
        formatter.minimumFractionDigits = 2
        formatter.maximumFractionDigits = 2
        return formatter
    }()
    
}

private extension Formatter {
    
    static func makeMoneyFormatter() -> NumberFormatter {
        let formatter = NumberFormatter()
        formatter.perMillSymbol = " "
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        formatter.currencySymbol = "₽"
        formatter.numberStyle = .currency
        return formatter
    }
    
}
