//
//  Extensions+Sequence.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

extension Sequence {
    
    func min<T: Comparable>(_ keyPath: KeyPath<Element, T>) -> Element? {
        return self.min(by: { $0[keyPath: keyPath] < $1[keyPath: keyPath] })
    }
    
    func max<T: Comparable>(_ keyPath: KeyPath<Element, T>) -> Element? {
        return self.max(by: { $0[keyPath: keyPath] < $1[keyPath: keyPath] })
    }
    
    func sorted<T: Comparable>(_ keyPath: KeyPath<Element, T>) -> [Self.Element] {
        return sorted(by: { $0[keyPath: keyPath] > $1[keyPath: keyPath] })
    }
    
    func map<T>(_ keyPath: KeyPath<Element, T>) -> [T] {
        return map { $0[keyPath: keyPath] }
    }
    
}
