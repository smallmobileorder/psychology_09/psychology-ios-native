//
//  Typealiases.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 16/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation
import UIKit

typealias Callback = () -> ()
typealias ObjectCallback<T> = (T) -> ()
typealias OptionalObjectCallback<T> = (T?) -> ()

typealias RouterCompletions = [UIViewController : Callback]
