//
//  Routable.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

protocol Routable: Presentable {
    var rootModule: Presentable? { get }
    var modules: [Presentable]? { get }
    func present(_ module: Presentable?, animated: Bool)
    func push(_ module: Presentable?, animated: Bool, completion: Callback?)
    func pushReplacing(_ module: Presentable?, animated: Bool, completion: Callback?)
    func popModule(animated: Bool)
    func dismissModule(animated: Bool, completion: Callback?)
    func setRootModule(_ module: Presentable?, hideBar: Bool)
    func popToRootModule(animated: Bool)
    func popToModule(_ module: Presentable?, animated: Bool)
}

extension Routable {
    
    func present(_ module: Presentable?) {
        present(module, animated: true)
    }
    
    func push(_ module: Presentable?) {
        push(module, animated: true)
    }
    
    func push(_ module: Presentable?, animated: Bool) {
        push(module, animated: animated, completion: {})
    }
    
    func pushReplacing(_ module: Presentable?) {
        pushReplacing(module, animated: true)
    }
    
    func pushReplacing(_ module: Presentable?, animated: Bool) {
        pushReplacing(module, animated: animated, completion: {})
    }
    
    func popModule() {
        popModule(animated: true)
    }
    
    func dismissModule() {
        dismissModule(animated: true)
    }
    
    func dismissModule(animated: Bool) {
        dismissModule(animated: animated, completion: nil)
    }
    
    func setRootModule(_ module: Presentable?) {
        setRootModule(module, hideBar: false)
    }
    
}
