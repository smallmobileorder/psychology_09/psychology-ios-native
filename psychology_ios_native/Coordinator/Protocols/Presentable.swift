//
//  Presentable.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol Presentable: class {
    var toPresent: UIViewController? { get }
}

extension UIViewController: Presentable {
    
    var toPresent: UIViewController? {
        return self
    }
    
}
