//
//  Coordinatable.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

enum PushNotificatonContent {
    case some
}

protocol Coordinatable: class {
    var views: [BaseEventViewProtocol?] { get }
    
    func start()
    func start(content: PushNotificatonContent)
    func consume(content: PushNotificatonContent)
}

extension Coordinatable {
    var views: [BaseEventViewProtocol?] { return [] }
    
    func start(content: PushNotificatonContent) { }
    func consume(content: PushNotificatonContent) { }
}

extension Coordinatable where Self: BaseCoordinator {
    
    func send(content: PushNotificatonContent) {
        consume(content: content)
        
        for child in childCoordinators {
            child.consume(content: content)
        }
    }
}
