//
//  DiaryFactoryProtocol.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 28/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

protocol DiaryFactoryProtocol: ModuleFactoryProtocol {
    func makeRecordsListView() -> RecordsListViewProtocol
    func makeDiaryAddRecordView() -> AddRecordViewProtocol
}
