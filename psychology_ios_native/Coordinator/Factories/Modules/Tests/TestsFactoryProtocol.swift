//
//  TestsFactoryProtocol.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 07/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

protocol TestsFactoryProtocol: ModuleFactoryProtocol {
    func makeTestsWelcomeView() -> TestsWelcomeViewProtocol
    func makeTestsList(filtered: Bool) -> TestsListViewProtocol
    func makeCompletedTestsView() -> CompletedTestsViewProtocol
    func makeTestAboutView() -> TestAboutViewProtocol
    func makeTestDescriptionView() -> TestDescriptionViewProtocol
    func makeTestQuestionView() -> TestQuestionViewProtocol
    func makeTestConclusionView() -> TestConclusionViewProtocol
}
