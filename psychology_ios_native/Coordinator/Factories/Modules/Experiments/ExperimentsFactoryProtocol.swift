//
//  ExperimentsFactoryProtocol.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 26/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

protocol ExperimentsFactoryProtocol: ModuleFactoryProtocol {
    func makeExperimentsListView() -> ExperimentsListViewProtocol
    func makeExperimentDescriptionView() -> ExperimentDescriptionViewProtocol
    func makeExperimentView() -> ExperimentViewProtocol
    func makeExperimentConclusionView() -> ExperimentConclusionViewProtocol
    func makeExerciseDescriptionView() -> ExperimentDescriptionViewProtocol
    func makeExerciseQuestionsView() -> ExerciseQuestionsViewProtocol
}
