//
//  HomeFactoryProtocol.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 26/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

protocol HomeFactoryProtocol: ModuleFactoryProtocol {
    func makeHomeView() -> HomeViewProtocol
}
