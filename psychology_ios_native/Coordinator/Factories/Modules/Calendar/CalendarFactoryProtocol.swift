//
//  CalendarFactoryProtocol.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 30/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

protocol CalendarFactoryProtocol: ModuleFactoryProtocol {
    func makeCalendarView() -> CalendarViewProtocol
}
