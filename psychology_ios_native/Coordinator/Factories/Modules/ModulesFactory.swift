//
//  ModulesFactory.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

final class ModulesFactory {}

extension ModulesFactory: HomeFactoryProtocol {
    
    func makeHomeView() -> HomeViewProtocol {
        let view = HomeViewController()
        HomeAssembly.assembly(with: view)
        return view
    }
    
}

extension ModulesFactory: ExperimentsFactoryProtocol {
    
    func makeExperimentsListView() -> ExperimentsListViewProtocol {
        let view: ExperimentsListViewController = ExperimentsListViewController.instanceController(storyboard: .experiments)
        ExperimentsListAssembly.assembly(with: view)
        return view
    }
    
    func makeExperimentDescriptionView() -> ExperimentDescriptionViewProtocol {
        let view = Storyboards.experiments.instance.instantiateViewController(withIdentifier: "ExperimentDescriptionViewController") as! ExperimentDescriptionViewController
        ExperimentDescriptionAssembly.assembly(with: view)
        return view
    }
    
    func makeExperimentView() -> ExperimentViewProtocol {
        let view: ExperimentViewController = ExperimentViewController.instanceController(storyboard: .experiments)
        ExperimentAssembly.assembly(with: view)
        return view
    }
    
    func makeExperimentConclusionView() -> ExperimentConclusionViewProtocol {
        let view: ExperimentConclusionViewController = ExperimentConclusionViewController.instanceController(storyboard: .experiments)
        ExperimentConclusionAssembly.assembly(with: view)
        return view
    }
    
    func makeExerciseDescriptionView() -> ExperimentDescriptionViewProtocol {
        let view = Storyboards.experiments.instance.instantiateViewController(withIdentifier: "ExerciseDescriptionViewController") as! ExperimentDescriptionViewController
        ExperimentDescriptionAssembly.assembly(with: view)
        return view
    }
    
    func makeExerciseQuestionsView() -> ExerciseQuestionsViewProtocol {
        let view: ExerciseQuestionsViewController = ExerciseQuestionsViewController.instanceController(storyboard: .experiments)
        ExerciseQuestionsAssembly.assembly(with: view)
        return view
    }
    
}

extension ModulesFactory: DiaryFactoryProtocol {
    
    func makeRecordsListView() -> RecordsListViewProtocol {
        let view: RecordsListViewController = RecordsListViewController.instanceController(storyboard: .diary)
        RecordsListAssembly.assembly(with: view)
        return view
    }
    
    func makeDiaryAddRecordView() -> AddRecordViewProtocol {
        let view: AddRecordViewController = AddRecordViewController.instanceController(storyboard: .diary)
        AddRecordAssembly.assembly(with: view)
        return view
    }
    
}

extension ModulesFactory: CalendarFactoryProtocol {
    
    func makeCalendarView() -> CalendarViewProtocol {
        let view: CalendarViewController = CalendarViewController.instanceController(storyboard: .calendar)
        CalendarAssembly.assembly(with: view)
        return view
    }
    
}

extension ModulesFactory: TestsFactoryProtocol {
    
    func makeTestsWelcomeView() -> TestsWelcomeViewProtocol {
        let view: TestsWelcomeViewController = TestsWelcomeViewController.instanceController(storyboard: .tests)
        TestsWelcomeAssembly.assembly(with: view)
        return view
    }
    
    func makeTestsList(filtered: Bool) -> TestsListViewProtocol {
        let view: TestsListViewController = TestsListViewController.instanceController(storyboard: .tests)
        TestsListAssembly.assembly(with: view, filtered: filtered)
        return view
    }
    
    func makeCompletedTestsView() -> CompletedTestsViewProtocol {
        let view: CompletedTestsViewController = CompletedTestsViewController.instanceController(storyboard: .tests)
        CompletedTestsAssembly.assembly(with: view)
        return view
    }
    
    func makeTestAboutView() -> TestAboutViewProtocol {
        let view: TestAboutViewController = TestAboutViewController.instanceController(storyboard: .tests)
        TestAboutAssembly.assembly(with: view)
        return view
    }
    
    func makeTestDescriptionView() -> TestDescriptionViewProtocol {
        let view: TestDescriptionViewController = TestDescriptionViewController.instanceController(storyboard: .tests)
        TestDescriptionAssembly.assembly(with: view)
        return view
    }
    
    func makeTestQuestionView() -> TestQuestionViewProtocol {
        let view: TestQuestionViewController = TestQuestionViewController.instanceController(storyboard: .tests)
        TestQuestionAssembly.assembly(with: view)
        return view
    }
    
    func makeTestConclusionView() -> TestConclusionViewProtocol {
        let view: TestConclusionViewController = TestConclusionViewController.instanceController(storyboard: .tests)
        TestConclusionAssembly.assembly(with: view)
        return view
    }
    
}

// MARK:- ModuleFactoryProtocol

extension ModulesFactory: ModuleFactoryProtocol {
    
    func makeProgressHeaderView() -> ExerciseViewProtocol {
        let view: ExerciseViewController = ExerciseViewController.instanceController(storyboard: .experiments)
        return view
    }
    
    func makeSectionHeaderView() -> SectionHeaderViewProtocol {
        let view = SectionHeaderViewController.fromNib()
        return view
    }
    
    func makeRouter(root: Presentable?, hideBar: Bool = false) -> Routable {
        let navigationController = UINavigationController(navigationBarClass: NavigationBar.self, toolbarClass: nil)
        navigationController.navigationBar.shadowImage = UIImage()
        navigationController.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController.view.backgroundColor = .clear
//        navigationController.navigationBar.isTranslucent = true
        let _ = navigationController.view
        let router = Router(rootController: navigationController)
        router.setRootModule(root, hideBar: hideBar)
        return router
    }
    
}
