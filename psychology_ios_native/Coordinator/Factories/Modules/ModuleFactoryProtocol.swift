//
//  ModuleFactoryProtocol.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

protocol ModuleFactoryProtocol {
    func makeProgressHeaderView() -> ExerciseViewProtocol
    func makeSectionHeaderView() -> SectionHeaderViewProtocol
    func makeRouter(root: Presentable?, hideBar: Bool) -> Routable
}
