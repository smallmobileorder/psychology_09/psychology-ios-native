//
//  CoordinatorFactoryProtocol.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

protocol CoordinatorFactoryProtocol {
    func makeHomeCoordinator(router: Routable, coordinatorFactory: CoordinatorFactoryProtocol) -> Coordinatable & HomeCoordinatorOutput
    func makeExperimentsCoordinator(router: Routable, coordinatorFactory: CoordinatorFactoryProtocol) -> Coordinatable & ExperimentsCoordinatorOutput
    func makeDiaryCoordinator(router: Routable, coordinatorFactory: CoordinatorFactoryProtocol) -> Coordinatable & DiaryCoordinatorOutput
    func makeTestsCoordinator(router: Routable, coordinatorFactory: CoordinatorFactoryProtocol) -> Coordinatable & TestsCoordinatorOutput
    func makeCalendarCoordinator(router: Routable) -> Coordinatable & CalendarCoordinatorOutput
}
