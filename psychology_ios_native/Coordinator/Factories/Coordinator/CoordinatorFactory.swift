//
//  CoordinatorFactory.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

final class CoordinatorFactory {
    private let modulesFactory = ModulesFactory()
}

// MARK:- CoordinatorFactoryProtocol

extension CoordinatorFactory: CoordinatorFactoryProtocol {
    
    func makeHomeCoordinator(router: Routable, coordinatorFactory: CoordinatorFactoryProtocol) -> Coordinatable & HomeCoordinatorOutput {
        return HomeCoordinator(with: modulesFactory, coordinatorFactory: coordinatorFactory, router: router)
    }
    
    func makeExperimentsCoordinator(router: Routable, coordinatorFactory: CoordinatorFactoryProtocol) -> Coordinatable & ExperimentsCoordinatorOutput {
        return ExperimentsCoordinator(with: modulesFactory, coordinatorFactory: coordinatorFactory, router: router)
    }
    
    func makeDiaryCoordinator(router: Routable, coordinatorFactory: CoordinatorFactoryProtocol) -> Coordinatable & DiaryCoordinatorOutput {
        return DiaryCoordinator(with: modulesFactory, coordinatorFactory: coordinatorFactory, router: router)
    }
    
    func makeTestsCoordinator(router: Routable, coordinatorFactory: CoordinatorFactoryProtocol) -> Coordinatable & TestsCoordinatorOutput {
        return TestsCoordinator(with: modulesFactory, coordinatorFactory: coordinatorFactory, router: router)
    }
    
    func makeCalendarCoordinator(router: Routable) -> Coordinatable & CalendarCoordinatorOutput {
        return CalendarCoordinator(with: modulesFactory, router: router)
    }
    
}
