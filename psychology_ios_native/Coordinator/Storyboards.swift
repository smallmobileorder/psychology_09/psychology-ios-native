//
//  Storyboards.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

public enum Storyboards: String {
    case experiments
    case diary
    case calendar
    case tests
    case settings
    case mind
    case consult
    var instance: UIStoryboard {
        return UIStoryboard(name: rawValue.capitalized, bundle: nil)
    }
}
