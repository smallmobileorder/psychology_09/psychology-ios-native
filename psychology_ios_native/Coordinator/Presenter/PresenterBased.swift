//
//  PresenterBased.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

enum AlertMessage {
    case success(String)
    case error(String)
    case message(String, String, String)
    case noInternetConnection
    case serverIsUnavailable
}

extension AlertMessage {
    static var errorDefault: AlertMessage { return .error("Ошибка") }
}

protocol PresenterOutputProtocol: class, Loadable {
    func show(message: AlertMessage, onCompletion: @escaping () -> ())
}

extension PresenterOutputProtocol {
    func show(message: AlertMessage) {
        show(message: message, onCompletion: { })
    }
}
