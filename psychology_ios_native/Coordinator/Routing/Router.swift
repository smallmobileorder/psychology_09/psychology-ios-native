//
//  Router.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

final class Router: NSObject {
    
    // MARK:- Private variables
    private weak var rootController: UINavigationController?
    
    private var completions: RouterCompletions = [:]
    
    init(rootController: UINavigationController) {
        self.rootController = rootController
    }
    
    var toPresent: UIViewController? {
        return rootController
    }
    
}

// MARK:- Routable

extension Router: Routable {
    
    var rootModule: Presentable? {
        return rootController?.viewControllers.first
    }
    
    var modules: [Presentable]? {
        return rootController?.viewControllers
    }
    
    func present(_ module: Presentable?, animated: Bool) {
        guard let controller = module?.toPresent else { return }
        
        controller.modalPresentationStyle = .fullScreen
        
        rootController?.present(controller, animated: animated, completion: nil)
    }
    
    func push(_ module: Presentable?, animated: Bool, completion: Callback?) {
        guard let controller = module?.toPresent else { return }
        guard !(controller is UINavigationController) else {
            assertionFailure("⚠️Deprecated push UINavigationController.")
            return
        }
        if let completion = completion { completions[controller] = completion }
        rootController?.pushViewController(controller, animated: animated)
    }
    
    func pushReplacing(_ module: Presentable?, animated: Bool, completion: Callback?) {
        guard module?.toPresent != nil else { return }
        
        push(module, animated: animated, completion: completion)
        
        var controllers = rootController?.viewControllers ?? []
        guard controllers.count >= 2 else { return }
        
        controllers.remove(at: controllers.count - 2)
        rootController?.viewControllers = controllers
    }
    
    func popModule(animated: Bool)  {
        if let controller = rootController?.popViewController(animated: animated) {
            runCompletion(for: controller)
        }
    }
    
    func dismissModule(animated: Bool, completion: Callback?) {
        rootController?.dismiss(animated: animated, completion: completion)
    }
    
    func setRootModule(_ module: Presentable?, hideBar: Bool) {
        guard let controller = module?.toPresent else { return }
        
        popToRootModule(animated: false)
        dismissModule()
        rootController?.setViewControllers([controller], animated: false)
        rootController?.isNavigationBarHidden = hideBar
    }
    
    func popToRootModule(animated: Bool) {
        if let controllers = rootController?.popToRootViewController(animated: animated) {
            controllers.forEach { runCompletion(for: $0) }
        }
    }
    
    func popToModule(_ module: Presentable?, animated: Bool) {
        guard let vc = module?.toPresent else { return }
        
        if let controllers = rootController?.popToViewController(vc, animated: animated) {
            controllers.forEach { runCompletion(for: $0) }
        }
    }
}

// MARK:- Private methods

private extension Router {
    func runCompletion(for controller: UIViewController) {
        guard let completion = completions[controller] else { return }
        
        completion()
        completions.removeValue(forKey: controller)
    }
}
