//
//  ShadowCard.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 26/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

@IBDesignable
class ShadowCard: UIView {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        addShadow(color: .shadow, opacity: 0.5, radius: 3.0, offset: .zero)
    }

}
