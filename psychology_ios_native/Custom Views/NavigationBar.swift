//
//  NavigationBar.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 27/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class NavigationBar: UINavigationBar {

    override var frame: CGRect {
        get { return super.frame }
        set { super.frame = CGRect(origin: CGPoint(x: 0, y: 20), size: newValue.size) }
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        guard nestedInteractiveViews(in: self, contain: point) else { return false }
        
        return super.point(inside: point, with: event)
    }
    
    private func nestedInteractiveViews(in view: UIView, contain point: CGPoint) -> Bool {
        if view.isPotentiallyInteractive, view.bounds.contains(convert(point, to: view)) { return true }
        
        return view.subviews.contains(where: { nestedInteractiveViews(in: $0, contain: point) })
    }

}

fileprivate extension UIView {
    var isPotentiallyInteractive: Bool {
        guard isUserInteractionEnabled else { return false }
        return (isControl || doesContainGestureRecognizer)
    }
    
    var isControl: Bool {
        return self is UIControl
    }
    
    var doesContainGestureRecognizer: Bool {
        return !(gestureRecognizers?.isEmpty ?? true)
    }
}
