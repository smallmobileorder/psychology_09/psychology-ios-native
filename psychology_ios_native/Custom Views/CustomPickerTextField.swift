//
//  CustomPickerTextField.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 21/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

@IBDesignable
class CustomPickerTextField: UITextField {
    
    var pickerDataSource: UIPickerViewDataSource? {
        get {
            return (inputView as? UIPickerView)?.dataSource
        }
        set {
            (inputView as? UIPickerView)?.dataSource = newValue
        }
    }
    
    var pickerDelegate: UIPickerViewDelegate? {
        get {
            return (inputView as? UIPickerView)?.delegate
        }
        set {
            (inputView as? UIPickerView)?.delegate = newValue
        }
    }
    
    var pickerView: UIPickerView? {
        return inputView as? UIPickerView
    }
    
    private weak var containerLayer: CALayer?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setup()
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        
        containerLayer?.frame = layer.bounds
        containerLayer?.cornerRadius = cornerRadius
        
        addShadow(color: .shadow, opacity: 0.5, radius: 3.0, offset: .zero)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 8, y: bounds.origin.y, width: bounds.width - 16, height: bounds.height)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return editingRect(forBounds: bounds)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return editingRect(forBounds: bounds)
    }
    
    override func caretRect(for position: UITextPosition) -> CGRect {
        return .zero
    }
    
    override func rightViewRect(forBounds bounds: CGRect) -> CGRect {
        return super.rightViewRect(forBounds: bounds).offsetBy(dx: -15, dy: 0)
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }

}

private extension CustomPickerTextField {
    
    func setup() {
        borderStyle = .none
        
        let containerLayer = CALayer()
        containerLayer.backgroundColor = UIColor.white.cgColor
        layer.addSublayer(containerLayer)
        self.containerLayer = containerLayer
        
        let imageView = UIImageView(image: #imageLiteral(resourceName: "expand"))
        imageView.tintColor = UIColor(red: 0.24, green: 0.24, blue: 0.26, alpha: 0.3)
        rightView = imageView
        rightViewMode = .always
        
        let pickerView = UIPickerView()
        pickerView.delegate = pickerDelegate
        inputView = pickerView
    }
    
}
