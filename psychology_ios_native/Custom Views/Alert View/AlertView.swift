//
//  CustomAlertView.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class AlertView: ViewNibInitialized {
    
    var message: AlertMessage? {
        didSet {
            showMessage()
        }
    }
    var onClose: Callback?
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var okButton: UIButton!
    
    override func initializedFromNib() {
        setup()
    }

}

private extension AlertView {
    
    @IBAction func close() {
        onClose?()
    }
    
}

private extension AlertView {
    
    func setup() {
//        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(close))
//        gestureRecognizer.cancelsTouchesInView = false
//        addGestureRecognizer(gestureRecognizer)
    }
    
    func showMessage() {
        guard let message = message else { return }
        
        switch message {
            case .message(let title, let message, let ok): showCenterMessage(title: title, message, ok: ok)
        default: break
        }
    }
    
    func showCenterMessage(title: String, _ message: String, ok: String) {
        titleLabel.text = title
        messageLabel.text = message
        okButton.setTitle(ok, for: .normal)
    }
    
}
