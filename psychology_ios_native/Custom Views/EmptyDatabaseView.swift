//
//  EmptyDatabaseView.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 21/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class EmptyDatabaseView: UIView {
    
    var text: String? {
        get { return label.text }
        set { label.text = newValue }
    }
    
    private weak var label: UILabel!

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }

}

private extension EmptyDatabaseView {
    
    func setup() {
        let label = UILabel()
        label.textAlignment = .center
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 20, weight: .semibold)
        label.textColor = .darkSystemGray
        label.translatesAutoresizingMaskIntoConstraints = false
        addSubview(label)
        self.label = label
        
        label
            .leadingAnchor(to: leadingAnchor, constant: 16)
            .trailingAnchor(to: trailingAnchor, constant: -16)
            .centerXAnchor(to: centerXAnchor)
            .centerYAnchor(to: centerYAnchor)
    }
    
}
