//
//  ProgressView.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 28/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

@IBDesignable
class ProgressView: UIView {
    
    @IBInspectable var numberOfSections = 0 {
        didSet {
            numberOfSections = max(0, numberOfSections)
            setupSections()
        }
    }
    
    @IBInspectable var currentSection = 0 {
        didSet {
            currentSection = min(numberOfSections - 1, max(0, currentSection))
            UIView.animate(withDuration: 0.3) {
                self.updateColors()
            }
        }
    }
    
    @IBInspectable var spacing: CGFloat = 1 {
        didSet {
            stackView.spacing = spacing
        }
    }
    
    @IBInspectable var defaultSectionColor: UIColor? = UIColor(red: 60, green: 60, blue: 67).withAlphaComponent(0.3) {
        didSet {
            updateColors()
        }
    }
    
    private weak var stackView: UIStackView!
    
    override func tintColorDidChange() {
        super.tintColorDidChange()
        
        updateColors()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: UIView.noIntrinsicMetric, height: 3)
    }

}

private extension ProgressView {
    
    func setup() {
        backgroundColor = .clear
        
        let stackView = UIStackView()
        stackView.axis = .horizontal
        stackView.spacing = spacing
        stackView.alignment = .fill
        stackView.distribution = .fillEqually
        stackView.frame = bounds
        stackView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(stackView)
        self.stackView = stackView
        
        setupSections()
    }
    
    func setupSections() {
        stackView.subviews.forEach { $0.removeFromSuperview() }
        
        for index in 0 ..< numberOfSections {
            let view = UIView()
            view.backgroundColor = index <= currentSection ? .active : defaultSectionColor
            stackView.addArrangedSubview(view)
        }
        
        guard numberOfSections != 0 else { return }
        
        let widthPerSection = frame.width / CGFloat(numberOfSections)
        let clampedWidth = (widthPerSection / 0.5).rounded(.down) * 0.5
        let extraWidth = widthPerSection - clampedWidth
        spacing = max(0.5, CGFloat(numberOfSections) * extraWidth / CGFloat(numberOfSections + 1))
    }
    
    func updateColors() {
        for (index, section) in stackView.subviews.enumerated() {
            section.backgroundColor = index <= currentSection ? .active : defaultSectionColor
        }
    }
    
}
