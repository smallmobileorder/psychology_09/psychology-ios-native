//
//  SliderView.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 22.02.2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

class SliderView: UIView {
    
    var text = "" {
        didSet {
            tit.text = text
            tit.isHidden = text.isEmpty
        }
    }
    
    private var tit = UILabel()
    private let percent = UILabel()
    var slider = CustomSlider()

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setup()
    }
    
    private func setup() {
        tit.numberOfLines = 0
        tit.text = text
        tit.font = .systemFont(ofSize: 16, weight: .semibold)
        tit.textColor = .darkSystemGray4
        
        percent.numberOfLines = 0
        percent.text = "1%"
        percent.font = .systemFont(ofSize: 16, weight: .semibold)
        percent.textColor = .darkSystemGray4
        percent.textAlignment = .right
        
        slider.minimumValue = 1
        slider.maximumValue = 100
        slider.value = 1
        slider.addTarget(self, action: #selector(changeValue), for: .valueChanged)
        
        let stack = UIStackView(arrangedSubviews: [tit, percent, slider])
        stack.axis = .vertical
        stack.spacing = 12
        stack.translatesAutoresizingMaskIntoConstraints = false
        addSubview(stack)
        NSLayoutConstraint.activate([
            stack.leadingAnchor.constraint(equalTo: leadingAnchor),
            stack.trailingAnchor.constraint(equalTo: trailingAnchor),
            stack.topAnchor.constraint(equalTo: topAnchor),
            stack.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
    
    @objc private func changeValue(_ sender: UISlider) {
        percent.text = "\(Int(sender.value))%"
    }

}
