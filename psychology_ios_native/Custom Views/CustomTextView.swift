//
//  CustomTextView.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 27/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

@IBDesignable
class CustomTextView: UITextView {
    
    @IBInspectable var placeholder: String = "" {
        didSet {
            placeholderLabel.text = placeholder
        }
    }
    @IBInspectable var placeholderColor: UIColor = .lightSystemGray4 {
        didSet {
            placeholderLabel.textColor = placeholderColor
        }
    }
    
    private var isPlaceholdered: Bool = false {
        didSet {
            placeholderLabel.isHidden = !isPlaceholdered
        }
    }
    
    override var text: String! {
        didSet {
            isPlaceholdered = text?.isEmpty ?? true
            clearButton.isHidden = text?.isEmpty ?? true
        }
    }
    
    override var layoutMargins: UIEdgeInsets {
        didSet {
            var margins = layoutMargins
            margins.left -= textContainer.lineFragmentPadding
            textContainerInset = margins
        }
    }
    
    private weak var placeholderLabel: UILabel!
    private weak var clearButton: UIButton!
    
    override init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        isPlaceholdered = text.isEmpty
    }
    
    override func willMove(toWindow newWindow: UIWindow?) {
        if newWindow == nil {
            NotificationCenter.default.removeObserver(self)
        } else {
            subscribe()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        textContainer.exclusionPaths = [
            UIBezierPath(rect: CGRect(
                x: frame.width - 44,
                y: 0,
                width: 44,
                height: 16)
            )
        ]
        
        addInnerShadow()
    }
    
    private weak var layerInnerShadow: CALayer?
    
    private func addInnerShadow() {
        let innerShadow = layerInnerShadow ?? {
            let innerShadow = CALayer()
            layer.addSublayer(innerShadow)
            layerInnerShadow = innerShadow
            return innerShadow
        }()
        innerShadow.frame = bounds
        let path = UIBezierPath(rect: innerShadow.bounds.inset(by: .only(top: -0.8, left: -0.8, bottom: -0.4, right: -0.8)))
        let cutout = UIBezierPath(rect: innerShadow.bounds).reversing()
        path.append(cutout)
        innerShadow.shadowPath = path.cgPath
        innerShadow.masksToBounds = true
        innerShadow.shadowColor = UIColor(white: 0, alpha: 1).cgColor
        innerShadow.shadowOffset = CGSize.zero
        innerShadow.shadowOpacity = 0.6
        innerShadow.shadowRadius = 3
    }
    
}

private extension CustomTextView {
    
    func setup() {
        addPlaceholderLabel()
        addClearButton()
        clearButton.isHidden = text?.isEmpty ?? true
        
        subscribe()
        
        textContainerInset = layoutMargins
    }
    
    func addPlaceholderLabel() {
        let placeholderLabel = UILabel()
        placeholderLabel.text = placeholder
        placeholderLabel.textColor = placeholderColor
        placeholderLabel.translatesAutoresizingMaskIntoConstraints = false
        addSubview(placeholderLabel)
        self.placeholderLabel = placeholderLabel
        NSLayoutConstraint.activate([
            placeholderLabel.leadingAnchor.constraint(equalTo: layoutMarginsGuide.leadingAnchor),
            placeholderLabel.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor),
            placeholderLabel.centerYAnchor.constraint(equalTo: layoutMarginsGuide.centerYAnchor)
        ])
    }
    
    func addClearButton() {
        let button = UIButton(type: .custom)
        button.addTarget(self, action: #selector(clear), for: .touchUpInside)
        button.setImage(#imageLiteral(resourceName: "Close"), for: .normal)
        button.tintColor = .lightSystemGray
        button.translatesAutoresizingMaskIntoConstraints = false
        button.imageEdgeInsets = .only(top: 10, left: 6, bottom: 10, right: 6)
        addSubview(button)
        clearButton = button
        NSLayoutConstraint.activate([
            button.trailingAnchor.constraint(equalTo: layoutMarginsGuide.trailingAnchor, constant: layoutMargins.right),
            button.topAnchor.constraint(equalTo: topAnchor),
            button.heightAnchor.constraint(equalToConstant: 40),
            button.widthAnchor.constraint(equalToConstant: 32)
        ])
    }
    
    func subscribe() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(textEditing),
                                               name: UITextView.textDidBeginEditingNotification,
                                               object: self)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(textEditing),
                                               name: UITextView.textDidChangeNotification,
                                               object: self)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(textDidEndEditing),
                                               name: UITextView.textDidEndEditingNotification,
                                               object: self)
    }

    @objc func textEditing() {
        clearButton.isHidden = text.isEmpty
        isPlaceholdered = false
    }

    @objc func textDidEndEditing() {
        isPlaceholdered = text.isEmpty
    }
    
    @objc func clear() {
        text = ""
        clearButton.isHidden = true
        delegate?.textViewDidChange?(self)
    }

}
