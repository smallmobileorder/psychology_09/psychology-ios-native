//
//  EmoView.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 22.02.2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

class EmoView: ViewNibInitialized {
    
    var onUpdateState: Callback?
    private(set) var enabledEmotions: [EmotionType: Int] = [:]
    
    private var currentPosition: Int?

    @IBOutlet private var sliderSectionView: UIView!
    @IBOutlet private var buttons: [UIButton]!
    @IBOutlet private var selectViews: [UIImageView]!
    @IBOutlet private var percentageLabels: [UILabel]!
    @IBOutlet private var emotionNameLabel: UILabel!
    @IBOutlet var slider: UISlider!
    @IBOutlet private var emotionPercentageLabel: UILabel!
    @IBOutlet private var sliderContainer: UIView!
    
    override func initializedFromNib() {
        super.initializedFromNib()
        
        setup()
    }
    
    func dismissEmotionPicker() {
        sliderSectionView.isHidden = true
        
        if let currentPosition = currentPosition {
            percentageLabels[currentPosition].isHidden = false
            selectViews[currentPosition].isHidden = true
        }
    }

}

private extension EmoView {
    
    func setup() {
        for ((button, label), emotionType) in zip(zip(buttons, percentageLabels), EmotionType.allCases) {
            let value = enabledEmotions[emotionType]
            button.setImage(UIImage(named: emotionType.imageName(enabled: value != nil)), for: .normal)
            label.isHidden = value == nil
            label.text = value?.description.appending("%")
        }
        
        for index in 0 ... 7 {
            buttons[index].addTarget(self, action: #selector(setEmotion(_:)), for: .touchUpInside)
        }
        
        sliderSectionView.isHidden = true
        
        onUpdateState?()
    }
    
    @IBAction func updateCurrentValue(_ sender: Any) {
        guard let currentPosition = currentPosition else { return }
        
        let value = Int(slider.value)
        let percentFormattedValue = value.description.appending("%")
        emotionPercentageLabel.text = percentFormattedValue
        percentageLabels[currentPosition].text = percentFormattedValue
        
        let emotion = EmotionType.allCases[currentPosition]
        
        enabledEmotions[emotion] = value
    }
    
    @objc func setEmotion(_ sender: UIButton) {
        defer { onUpdateState?() }
        
        guard let position = buttons.firstIndex(of: sender) else { return }
        
        if let currentPosition = currentPosition {
            percentageLabels[currentPosition].isHidden = false
            selectViews[currentPosition].isHidden = true
        }
        
        let selectedEmotion = EmotionType.allCases[position]
        
        let percentageLabel = percentageLabels[position]
        let selectView = selectViews[position]
        
        if let _ = enabledEmotions.removeValue(forKey: selectedEmotion) {
            sender.setImage(UIImage(named: selectedEmotion.imageName(enabled: false)), for: .normal)
            percentageLabel.isHidden = true
            selectView.isHidden = true
            sliderSectionView.isHidden = true
            
            currentPosition = nil
            
            return
        } else if enabledEmotions.count > 0 {
            return
        }
        
        sender.setImage(UIImage(named: selectedEmotion.imageName(enabled: true)), for: .normal)
        percentageLabel.isHidden = true
        selectView.isHidden = false
        
        sliderSectionView.isHidden = false
        slider.value = 1
        emotionPercentageLabel.text = "1%"
        percentageLabel.text = "1%"
        emotionNameLabel.text = selectedEmotion.name
        
        enabledEmotions[selectedEmotion] = 1
        
        currentPosition = position
    }
    
}
