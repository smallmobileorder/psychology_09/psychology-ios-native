//
//  RadioButton.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 20.02.2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

@IBDesignable
class RadioButton: UIButton {
    @IBInspectable var shouldManuallyToggle: Bool = true
    
    @IBInspectable var selectedImage: UIImage? = UIImage(named: "icRadioSelected") {
        didSet {
            setImage(selectedImage, for: .selected)
        }
    }
    
    @IBInspectable var deselectedImage: UIImage? = UIImage(named: "icRadioDeselected") {
        didSet {
            setImage(deselectedImage, for: .normal)
        }
    }
    
    // MARK: Object lifecycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setup()
    }
    
    private func setup() {
        setImage(selectedImage, for: .selected)
        setImage(deselectedImage, for: .normal)
        
        tintColor = .clear
        addTarget(self, action: #selector(toggle), for: .touchUpInside)
    }
    
    @objc func toggle() {
        if shouldManuallyToggle {
            isSelected.toggle()
        }
    }
}
