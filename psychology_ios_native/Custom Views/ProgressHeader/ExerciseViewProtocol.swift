//
//  ExerciseViewProtocol.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 28/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

protocol ExerciseViewProtocol: BaseViewProtocol {
    var headerTitle: String? { get set }
    var casesCount: Int { get set }
    var currentCaseIndex: Int { get set }
}

