//
//  ExerciseViewController.swift
//  psychology_ios_native
//
//  Created Eldar Goloviznin on 28/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class ExerciseViewController: UIViewController, ExerciseViewProtocol {

    var headerTitle: String?
    var casesCount: Int = 0 { didSet { progressView?.numberOfSections = casesCount } }
    var currentCaseIndex: Int = 0 { didSet { progressView?.currentSection = currentCaseIndex } }
    
    @IBOutlet private weak var progressView: ProgressView?
    @IBOutlet private weak var headerLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .clear
        
        progressView?.numberOfSections = casesCount
        progressView?.currentSection = currentCaseIndex
        headerLabel.text = headerTitle
    }
    
}
