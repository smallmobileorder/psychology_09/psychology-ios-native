//
//  ViewNibInitialized.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class ViewNibInitialized: UIView {

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    func initializedFromNib() { }

}

private extension ViewNibInitialized {
    
    func setup() {
        loadNib()
        initializedFromNib()
    }
    
}
