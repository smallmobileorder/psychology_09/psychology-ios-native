//
//  RoundedView.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

@IBDesignable
class RoundedView: UIView {
    
    @IBInspectable var radius: CGFloat = 0.0 { didSet { round() } }
    @IBInspectable var topLeft: Bool = false { didSet { round() } }
    @IBInspectable var topRight: Bool = false { didSet { round() } }
    @IBInspectable var bottomLeft: Bool = false { didSet { round() } }
    @IBInspectable var bottomRight: Bool = false { didSet { round() } }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        round()
    }
    
}

private extension RoundedView {
    
    func round() {
        var corners = UIRectCorner()
        if topLeft { corners = corners.union(.topLeft) }
        if topRight { corners = corners.union(.topRight) }
        if bottomLeft { corners = corners.union(.bottomLeft) }
        if bottomRight { corners = corners.union(.bottomRight) }
        round(with: corners, radius: radius)
    }
    
}
