//
//  AdvancedTableView.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class AdvancedTableView: UITableView {
    
    var emptyView: UIView? {
        didSet {
            oldValue?.removeFromSuperview()
            
            guard let emptyView = emptyView,
                let emptyContainer = emptyViewContainer else { return }
            
            emptyView.translatesAutoresizingMaskIntoConstraints = false
            emptyContainer.addSubview(emptyView)
            NSLayoutConstraint.activate([
                emptyView.leadingAnchor.constraint(equalTo: emptyContainer.leadingAnchor, constant: 16.0),
                emptyView.trailingAnchor.constraint(equalTo: emptyContainer.trailingAnchor, constant: -16.0),
                emptyView.centerYAnchor.constraint(equalTo: emptyContainer.centerYAnchor)
            ])
        }
    }
    
    private var isEmpty: Bool = false {
        didSet { emptyViewContainer?.isHidden = !isEmpty }
    }
    
    private weak var emptyViewContainer: UIView?
    
    override var backgroundColor: UIColor? {
        didSet { emptyViewContainer?.backgroundColor = backgroundColor }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let emptyContainer = emptyViewContainer { bringSubviewToFront(emptyContainer) }
        
        handleContentChange()
    }
    
    override init(frame: CGRect, style: UITableView.Style) {
        super.init(frame: frame, style: style)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    func sizeToFitTableHeaderView() {
        guard let header = tableHeaderView else { return }
        
        let headerWidth = bounds.width
        
        let headerHeight = header.minSize(thatFits: CGSize(width: headerWidth, height: UIView.noIntrinsicMetric)).height
        guard headerHeight != header.frame.height else { return }
        
        header.frame.size = CGSize(width: headerWidth, height: headerHeight)
        tableHeaderView = header
        layoutIfNeeded()
    }
    
    func sizeToFitTableFooterView() {
        guard let footer = tableFooterView else { return }
        
        let footerWidth = bounds.width
        
        let footerHeight = footer.minSize(thatFits: CGSize(width: footerWidth, height: UIView.noIntrinsicMetric)).height
        guard footerHeight != footer.frame.height else { return }
        
        footer.frame.size = CGSize(width: footerWidth, height: footerHeight)
        tableFooterView = footer
        layoutIfNeeded()
    }
    
    
    
}

private extension AdvancedTableView {
    
    func setup() {
        let container = UIView()
        container.isHidden = true
        container.isUserInteractionEnabled = false
        container.backgroundColor = .clear
        container.frame = bounds
        container.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        addSubview(container)
        emptyViewContainer = container
        
        addKeyboardListener()
    }
    
    func handleContentChange() {
        isEmpty = false
        
        guard let dataSource = dataSource else { return }
        
        let sectionsCount = dataSource.numberOfSections?(in: self) ?? 1
        
        guard sectionsCount > 0 else {
            isEmpty = true
            return
        }
        
        isEmpty = !(0 ..< sectionsCount).contains(where: { dataSource.tableView(self, numberOfRowsInSection: $0) > 0 })
    }
    
    func addKeyboardListener() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidChange),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil
        )
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidChange),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil
        )
    }
    
    @objc func keyboardDidChange(notification: Notification) {
        guard let userInfo = notification.userInfo, let rootView = rootView else { return }
        
        let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = rootView.convert(keyboardScreenEndFrame, from: rootView.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            contentInset = UIEdgeInsets.zero
        } else {
            var inset = UIEdgeInsets.only(bottom: keyboardViewEndFrame.height)
            if #available(iOS 11.0, *) {
                inset.bottom -= rootView.safeAreaInsets.bottom
            }
            contentInset = inset
        }
        
        scrollIndicatorInsets = contentInset
    }
    
}
