//
//  SecureTextField.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class SecureTextField: UITextField {

    override func canPaste(_ itemProviders: [NSItemProvider]) -> Bool {
        return true
    }
    
}
