//
//  CustomTextField.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 20/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class CustomTextField: UITextField {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        addInnerShadow()
    }

    private weak var layerInnerShadow: CALayer?
    
    private func addInnerShadow() {
        let innerShadow = layerInnerShadow ?? {
            let innerShadow = CALayer()
            layer.addSublayer(innerShadow)
            layerInnerShadow = innerShadow
            return innerShadow
        }()
        innerShadow.frame = bounds
        let path = UIBezierPath(rect: innerShadow.bounds.inset(by: .only(top: -0.8, left: -0.8, bottom: -0.4, right: -0.8)))
        let cutout = UIBezierPath(rect: innerShadow.bounds).reversing()
        path.append(cutout)
        innerShadow.shadowPath = path.cgPath
        innerShadow.masksToBounds = true
        innerShadow.shadowColor = UIColor(white: 0, alpha: 1).cgColor
        innerShadow.shadowOffset = CGSize.zero
        innerShadow.shadowOpacity = 0.6
        innerShadow.shadowRadius = 3
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 8, y: bounds.origin.y, width: bounds.width - 16, height: bounds.height)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return editingRect(forBounds: bounds)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return editingRect(forBounds: bounds)
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return false
    }

}

private extension CustomTextField {
    
    func setup() {
        borderStyle = .none
        cornerRadius = 8
    }
    
}
