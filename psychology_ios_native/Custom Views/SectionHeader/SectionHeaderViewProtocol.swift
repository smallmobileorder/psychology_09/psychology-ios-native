//
//  SectionHeaderViewProtocol.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 16/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import Foundation

protocol SectionHeaderViewProtocol: BaseViewProtocol {
    var headerTitle: String? { get set }
}
