//
//  SectionHeaderViewController.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 16/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class SectionHeaderViewController: UIViewController, SectionHeaderViewProtocol {
    
    var headerTitle: String?
    
    @IBOutlet private weak var headerLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()

        headerLabel.text = headerTitle
    }

}
