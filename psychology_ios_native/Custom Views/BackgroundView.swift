//
//  BackgroundView.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 16/03/2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

extension Notification.Name {
    static let backgroundChange = Notification.Name("backgroundChangeCustom")
}

class BackgroundView: UIView {
    
    var image: UIImage? {
        get {
            imageView.image
        }
        set {
            imageView.image = newValue
        }
    }
    
    private var imageView = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setup()
    }
    
    private func setup() {
        clipsToBounds = false
        imageView.translatesAutoresizingMaskIntoConstraints = false
        insertSubview(imageView, at: 0)
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: -6),
            imageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: -6),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: 6),
            imageView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 6)
        ])
        NotificationCenter.default.addObserver(self, selector: #selector(updateImage), name: .backgroundChange, object: nil)
        updateImage()
    }
    
    @objc private func updateImage() {
        let backgroundName = UserDefaults.standard.string(forKey: "background_image")
        if let backgroundName = backgroundName {
            image = UIImage(named: backgroundName)
        } else {
            image = UIImage(named: "")
        }
    }

}
