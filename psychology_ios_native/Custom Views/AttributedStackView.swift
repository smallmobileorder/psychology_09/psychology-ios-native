//
//  AttributedStackView.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

struct AttributedInfo {
    let attribute: String
    let info: String
}

extension AttributedInfo {
    
    init?(attribute: String, info: String?) {
        guard let info = info else { return nil }
        
        self.attribute = attribute
        self.info = info
    }
    
}

class AttributedStackView: UIStackView {
    
    var attributeFont: UIFont = .systemFont(ofSize: 16.0) { didSet { update() } }
    var attributeColor: UIColor = UIColor.black.withAlphaComponent(0.87) { didSet { update() } }
    
    var infoFont: UIFont = .systemFont(ofSize: 12.0) { didSet { update() } }
    var infoColor: UIColor = UIColor.black.withAlphaComponent(0.60) { didSet { update() } }
    
    var attributedInfoMargin: UIEdgeInsets = UIEdgeInsets(top: 12.0, left: 16.0, bottom: 12.0, right: 16.0) { didSet { update() } }
    var separatorMargin: UIEdgeInsets = .only(left: 16.0) { didSet { update() } }
    
    func add(attributes: AttributedInfo?...) {
        for attribute in attributes {
            add(attribute: attribute)
        }
    }
    
    func add(attribute: AttributedInfo?) {
        guard let attribute = attribute else { return }
        
        let attributeLabel = UILabel()
        setupAttributeLabel(attributeLabel)
        attributeLabel.text = attribute.attribute
        
        let infoLabel = UILabel()
        setupInfoLabel(infoLabel)
        infoLabel.text = attribute.info
        
        let labelStackView = UIStackView(arrangedSubviews: [attributeLabel, infoLabel])
        setupLabelStackView(labelStackView)
        
        let separatorView = UIView()
        separatorView.backgroundColor = UIColor(red: 227.0 / 255.0,
                                                green: 227.0 / 255.0,
                                                blue: 227.0 / 255.0,
                                                alpha: 1.0)
        separatorView.heightAnchor(constant: 1.0)
        
        let separatorStackView = UIStackView(arrangedSubviews: [separatorView])
        setupSeparatorStackView(separatorStackView)
        
        let rowStackView = UIStackView(arrangedSubviews: [labelStackView, separatorStackView])
        rowStackView.axis = .vertical
        
        addArrangedSubview(rowStackView)
    }

}

private extension AttributedStackView {
    
    func update() {
        for case let rowStackView as UIStackView in subviews {
            if let labelStackView = rowStackView.arrangedSubviews.first as? UIStackView {
                setupLabelStackView(labelStackView)
                setupAttributeLabel(labelStackView.arrangedSubviews.first as? UILabel)
                setupInfoLabel(labelStackView.arrangedSubviews.last as? UILabel)
            }
            setupSeparatorStackView(rowStackView.arrangedSubviews.last as? UIStackView)
        }
    }
    
    func setupAttributeLabel(_ label: UILabel?) {
        label?.font = attributeFont
        label?.textColor = attributeColor
    }
    
    func setupInfoLabel(_ label: UILabel?) {
        label?.font = infoFont
        label?.textColor = infoColor
    }
    
    func setupLabelStackView(_ stackView: UIStackView?) {
        stackView?.isLayoutMarginsRelativeArrangement = true
        stackView?.axis = .horizontal
        stackView?.distribution = .equalSpacing
        stackView?.layoutMargins = attributedInfoMargin
    }
    
    func setupSeparatorStackView(_ stackView: UIStackView?) {
        stackView?.isLayoutMarginsRelativeArrangement = true
        stackView?.layoutMargins = separatorMargin
    }
    
}
