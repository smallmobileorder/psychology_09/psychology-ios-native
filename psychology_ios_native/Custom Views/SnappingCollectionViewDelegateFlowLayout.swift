//
//  SnappingCollectionViewDelegateFlowLayout.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class SnappingCollectionViewDelegateFlowLayout: NSObject, UICollectionViewDelegateFlowLayout {
    
    private let spacing: CGFloat
    
    init(spacing: CGFloat) {
        self.spacing = spacing
    }
    
    weak var collectionView: UICollectionView? {
        willSet {
            collectionView?.delegate = nil
        }
        didSet {
            collectionView?.delegate = self
        }
    }
    
    weak var delegate: UICollectionViewDelegateFlowLayout?
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.collectionView?(collectionView, didSelectItemAt: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return delegate?.collectionView?(collectionView, layout: collectionViewLayout, insetForSectionAt: section) ?? .only(left: spacing, right: spacing)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return spacing
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return delegate?.collectionView?(collectionView, layout: collectionViewLayout, sizeForItemAt: indexPath) ?? .zero
    }
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        guard let collectionView = scrollView as? UICollectionView else {
            return
        }
        
        let layout = collectionView.collectionViewLayout
        let bounds = collectionView.bounds
        
        switch velocity.x {
        case let x where x >= 0.3:
            let attributes = layout.layoutAttributesForElements(in: bounds.offsetBy(dx: bounds.width, dy: 0))
            if let rightAttribute = attributes?.min(by: { $0.frame.origin.x < $1.frame.origin.x }) {
                targetContentOffset.pointee.x = rightAttribute.frame.origin.x
            } else {
                fallthrough
            }
            
        case let x where abs(x) < 0.3:
            let currentCenter = bounds.midX
            let attributes = layout.layoutAttributesForElements(in: bounds)
            if let nearestToCurrentCenter = attributes?.min(by: { abs($0.center.x - currentCenter) < abs($1.center.x - currentCenter) }) {
                targetContentOffset.pointee.x = nearestToCurrentCenter.frame.origin.x
            }
            
        default:
            let attributes = layout.layoutAttributesForElements(in: bounds.offsetBy(dx: -bounds.width, dy: 0))
            if let leftAttribute = attributes?.max(by: { $0.frame.origin.x < $1.frame.origin.x }) {
                targetContentOffset.pointee.x = leftAttribute.frame.origin.x
            }
        }
        
        targetContentOffset.pointee.x -= spacing * 1.5
    }
    
}
