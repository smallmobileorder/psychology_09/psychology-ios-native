//
//  ActivityView.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class ActivityView: UIView {
    
    private var activityIndicator: UIActivityIndicatorView!
    
    private(set) var isLoading: Bool = false {
        didSet {
            isHidden = !isLoading
            if isLoading { activityIndicator.startAnimating() } else { activityIndicator.stopAnimating() }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    func start() {
        isLoading = true
    }
    
    func stop() {
        isLoading = false
    }
    
    override var intrinsicContentSize: CGSize {
        return CGSize(width: 24, height: 24)
    }

}

private extension ActivityView {
    
    func setup() {
        isHidden = true
        
        activityIndicator = UIActivityIndicatorView()
        activityIndicator.activateAnchors()
        addSubview(activityIndicator)
        activityIndicator.setEqualSize(on: self)
    }
    
}
