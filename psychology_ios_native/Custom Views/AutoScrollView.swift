//
//  CustomScrollView.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 15/09/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class AutoScrollView: UIScrollView {
    

    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }

}

private extension AutoScrollView {
    
    func setup() {
        setDismissKeyboardOnTap()
        addKeyboardListener()
    }
    
    func addKeyboardListener() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidChange),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil
        )
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardDidChange),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil
        )
    }
    
    @objc func keyboardDidChange(notification: Notification) {
        guard let userInfo = notification.userInfo, let rootView = rootView else { return }
        
        let keyboardScreenEndFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let keyboardViewEndFrame = rootView.convert(keyboardScreenEndFrame, from: rootView.window)
        
        if notification.name == UIResponder.keyboardWillHideNotification {
            contentInset = .zero
        } else {
            var inset = UIEdgeInsets.only(bottom: keyboardViewEndFrame.height)
            if #available(iOS 11.0, *) {
                inset.bottom -= rootView.safeAreaInsets.bottom
            }
            contentInset = inset
        }
        
        scrollIndicatorInsets = contentInset
    }
    
}
