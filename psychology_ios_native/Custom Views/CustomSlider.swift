//
//  CustomSlider.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 01.06.2020.
//  Copyright © 2020 Psychology. All rights reserved.
//

import UIKit

class CustomSlider: UISlider {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        setup()
    }
    
    private func setup() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tap(gesture:)))
        addGestureRecognizer(tapGesture)
    }
    
    @objc private func tap(gesture: UITapGestureRecognizer) {
        update(with: gesture.location(in: self))
    }
    
    private func update(with touch: CGPoint) {
        let positionOfSlider: CGPoint = frame.origin
        let widthOfSlider: CGFloat = frame.size.width
        let newValue = ((touch.x - positionOfSlider.x) * CGFloat(maximumValue) / widthOfSlider)

        setValue(Float(newValue), animated: true)
        sendActions(for: .valueChanged)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        defer { super.touchesBegan(touches, with: event) }
        guard let pointTapped = touches.first?.location(in: self) else { return }

        update(with: pointTapped)
    }
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        true
    }
}
