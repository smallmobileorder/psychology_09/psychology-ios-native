//
//  CustomButton.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 07/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class CustomButton: UIButton {
    private let animationKeyPath = "transform.scale"
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    private func setup() {
        titleLabel?.font = .systemFont(ofSize: 17, weight: .semibold)
        
        addTarget(self, action: #selector(animateTouchDown), for: .touchDown)
        addTarget(self, action: #selector(animateTouchUp), for: .touchUpInside)
        addTarget(self, action: #selector(animateTouchUp), for: .touchUpOutside)
        addTarget(self, action: #selector(animateTouchUp), for: .touchCancel)
    }
    
    @objc private func animateTouchDown() {
        let animation = CABasicAnimation(keyPath: animationKeyPath)
        animation.fromValue = layer.presentation()?.value(forKeyPath: animationKeyPath)
        animation.toValue = 0.98
        animation.duration = 0.1
        layer.add(animation, forKey: animationKeyPath)
        layer.setValue(0.98, forKeyPath: animationKeyPath)
    }
    
    @objc private func animateTouchUp() {
        let animation = CABasicAnimation(keyPath: animationKeyPath)
        animation.fromValue = layer.presentation()?.value(forKeyPath: animationKeyPath)
        animation.toValue = 1.0
        animation.duration = 0.1
        layer.add(animation, forKey: animationKeyPath)
        layer.setValue(1.0, forKeyPath: animationKeyPath)
    }
}
