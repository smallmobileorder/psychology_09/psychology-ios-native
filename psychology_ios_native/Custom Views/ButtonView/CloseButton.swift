//
//  CloseButton.swift
//  psychology_ios_native
//
//  Created by Alex Korneev on 17.09.2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

class CloseButton: UIButton {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setup()
    }
    
}

private extension CloseButton {
    
    func setup(){
        self.tintColor = UIColor.init(rgb: 0xDADADA)
        self.setImage(#imageLiteral(resourceName: "Close"), for: .normal)
    }
    
}
