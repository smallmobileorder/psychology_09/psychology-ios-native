//
//  BrandButton.swift
//  psychology_ios_native
//
//  Created by Alex Korneev on 16.09.2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

@IBDesignable
class BrandButton: CustomButton {
    var normalColor: UIColor = .active {
        didSet {
            setNormalBackgroundImage()
        }
    }
    var disabledColor: UIColor = .lightSystemGray {
        didSet {
            setDisabledBackgroundImage()
        }
    }
    var selectedColor: UIColor = .darkPurple {
        didSet {
            setSelectedBackgroundImage()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    private func setup() {
        setNormalBackgroundImage()
        setDisabledBackgroundImage()
        setSelectedBackgroundImage()
        
        setTitleColor(.back, for: .normal)
    }
    
    private func setNormalBackgroundImage() {
        adjustsImageWhenHighlighted = normalColor != selectedColor
        setBackgroundImage(normalColor.image(frame.size, cornerRadius: cornerRadius), for: .normal)
    }
    
    private func setDisabledBackgroundImage() {
        setBackgroundImage(disabledColor.image(frame.size, cornerRadius: cornerRadius), for: .disabled)
    }
    
    private func setSelectedBackgroundImage() {
        adjustsImageWhenHighlighted = normalColor != selectedColor
        setBackgroundImage(selectedColor.image(frame.size, cornerRadius: cornerRadius), for: [.selected, .highlighted, .focused])
    }
}
