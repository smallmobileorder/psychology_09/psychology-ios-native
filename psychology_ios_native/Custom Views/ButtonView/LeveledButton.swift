//
//  LeveledButton.swift
//  psychology_ios_native
//
//  Created by Eldar Goloviznin on 20/10/2019.
//  Copyright © 2019 Psychology. All rights reserved.
//

import UIKit

@IBDesignable
class LeveledButton: CustomButton {

    override var isSelected: Bool {
        didSet {
            setNeedsLayout()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if isSelected {
            removeShadow()
            addInnerShadow()
            layer.borderWidth = 1
            layer.borderColor = tintColor.cgColor
        } else {
            layer.borderWidth = 0
            layerInnerShadow?.removeFromSuperlayer()
            addShadow(color: .shadow, opacity: 0.5, radius: 3.0, offset: .zero)
        }
    }
    
    private weak var layerInnerShadow: CALayer?
    
    private func addInnerShadow() {
        let innerShadow = layerInnerShadow ?? {
            let innerShadow = CALayer()
            layer.addSublayer(innerShadow)
            layerInnerShadow = innerShadow
            return innerShadow
        }()
        innerShadow.frame = bounds
        let path = UIBezierPath(rect: innerShadow.bounds.inset(by: .only(top: -0.8, left: -0.8, bottom: -0.4, right: -0.8)))
        let cutout = UIBezierPath(rect: innerShadow.bounds).reversing()
        path.append(cutout)
        clipsToBounds = true
        innerShadow.shadowPath = path.cgPath
        innerShadow.masksToBounds = true
        innerShadow.shadowColor = UIColor(white: 0, alpha: 1).cgColor
        innerShadow.shadowOffset = CGSize.zero
        innerShadow.shadowOpacity = 0.6
        innerShadow.shadowRadius = 3
    }

}

private extension LeveledButton {
    
    func setup() {
        titleLabel?.font = .systemFont(ofSize: 17)
    }
    
}
